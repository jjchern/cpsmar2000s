* NOTE: You need to set the Stata working directory to the path
* where the data file is located.

set more off

clear
quietly infix                     ///
  str     rectype      1-1        ///
  int     year         2-5        ///
  long    serial       6-10       ///
  byte    mish         11-11      ///
  byte    numprec      12-13      ///
  float   hwtsupp      14-23      ///
  float   hhwt04       24-31      ///
  double  hrhhid       32-46      ///
  long    hrhhid2      47-52      ///
  byte    huhhnum      53-54      ///
  byte    gq           55-55      ///
  byte    hhintype     56-56      ///
  byte    repwt        57-57      ///
  byte    region       58-59      ///
  byte    statefip     60-61      ///
  byte    statecensus  62-63      ///
  byte    asecflag     64-64      ///
  byte    metro        65-65      ///
  int     metarea      66-69      ///
  long    county       70-74      ///
  byte    ownershp     75-76      ///
  double  hhincome     77-84      ///
  long    housret      85-89      ///
  long    proptax      90-94      ///
  byte    pubhous      95-95      ///
  byte    rentsub      96-96      ///
  byte    heatsub      97-97      ///
  int     heatval      98-101     ///
  byte    foodstmp     102-102    ///
  byte    stampno      103-103    ///
  byte    stampmo      104-105    ///
  long    stampval     106-110    ///
  byte    atelunch     111-112    ///
  byte    lunchsub     113-113    ///
  byte    frelunch     114-115    ///
  byte    unitsstr     116-117    ///
  byte    phone        118-118    ///
  byte    nfams        119-120    ///
  byte    ncouples     121-121    ///
  byte    nmothers     122-122    ///
  byte    nfathers     123-123    ///
  float   repwt1       124-132    ///
  float   repwt2       133-141    ///
  float   repwt3       142-150    ///
  float   repwt4       151-159    ///
  float   repwt5       160-168    ///
  float   repwt6       169-177    ///
  float   repwt7       178-186    ///
  float   repwt8       187-195    ///
  float   repwt9       196-204    ///
  float   repwt10      205-213    ///
  float   repwt11      214-222    ///
  float   repwt12      223-231    ///
  float   repwt13      232-240    ///
  float   repwt14      241-249    ///
  float   repwt15      250-258    ///
  float   repwt16      259-267    ///
  float   repwt17      268-276    ///
  float   repwt18      277-285    ///
  float   repwt19      286-294    ///
  float   repwt20      295-303    ///
  float   repwt21      304-312    ///
  float   repwt22      313-321    ///
  float   repwt23      322-330    ///
  float   repwt24      331-339    ///
  float   repwt25      340-348    ///
  float   repwt26      349-357    ///
  float   repwt27      358-366    ///
  float   repwt28      367-375    ///
  float   repwt29      376-384    ///
  float   repwt30      385-393    ///
  float   repwt31      394-402    ///
  float   repwt32      403-411    ///
  float   repwt33      412-420    ///
  float   repwt34      421-429    ///
  float   repwt35      430-438    ///
  float   repwt36      439-447    ///
  float   repwt37      448-456    ///
  float   repwt38      457-465    ///
  float   repwt39      466-474    ///
  float   repwt40      475-483    ///
  float   repwt41      484-492    ///
  float   repwt42      493-501    ///
  float   repwt43      502-510    ///
  float   repwt44      511-519    ///
  float   repwt45      520-528    ///
  float   repwt46      529-537    ///
  float   repwt47      538-546    ///
  float   repwt48      547-555    ///
  float   repwt49      556-564    ///
  float   repwt50      565-573    ///
  float   repwt51      574-582    ///
  float   repwt52      583-591    ///
  float   repwt53      592-600    ///
  float   repwt54      601-609    ///
  float   repwt55      610-618    ///
  float   repwt56      619-627    ///
  float   repwt57      628-636    ///
  float   repwt58      637-645    ///
  float   repwt59      646-654    ///
  float   repwt60      655-663    ///
  float   repwt61      664-672    ///
  float   repwt62      673-681    ///
  float   repwt63      682-690    ///
  float   repwt64      691-699    ///
  float   repwt65      700-708    ///
  float   repwt66      709-717    ///
  float   repwt67      718-726    ///
  float   repwt68      727-735    ///
  float   repwt69      736-744    ///
  float   repwt70      745-753    ///
  float   repwt71      754-762    ///
  float   repwt72      763-771    ///
  float   repwt73      772-780    ///
  float   repwt74      781-789    ///
  float   repwt75      790-798    ///
  float   repwt76      799-807    ///
  float   repwt77      808-816    ///
  float   repwt78      817-825    ///
  float   repwt79      826-834    ///
  float   repwt80      835-843    ///
  float   repwt81      844-852    ///
  float   repwt82      853-861    ///
  float   repwt83      862-870    ///
  float   repwt84      871-879    ///
  float   repwt85      880-888    ///
  float   repwt86      889-897    ///
  float   repwt87      898-906    ///
  float   repwt88      907-915    ///
  float   repwt89      916-924    ///
  float   repwt90      925-933    ///
  float   repwt91      934-942    ///
  float   repwt92      943-951    ///
  float   repwt93      952-960    ///
  float   repwt94      961-969    ///
  float   repwt95      970-978    ///
  float   repwt96      979-987    ///
  float   repwt97      988-996    ///
  float   repwt98      997-1005   ///
  float   repwt99      1006-1014  ///
  float   repwt100     1015-1023  ///
  float   repwt101     1024-1032  ///
  float   repwt102     1033-1041  ///
  float   repwt103     1042-1050  ///
  float   repwt104     1051-1059  ///
  float   repwt105     1060-1068  ///
  float   repwt106     1069-1077  ///
  float   repwt107     1078-1086  ///
  float   repwt108     1087-1095  ///
  float   repwt109     1096-1104  ///
  float   repwt110     1105-1113  ///
  float   repwt111     1114-1122  ///
  float   repwt112     1123-1131  ///
  float   repwt113     1132-1140  ///
  float   repwt114     1141-1149  ///
  float   repwt115     1150-1158  ///
  float   repwt116     1159-1167  ///
  float   repwt117     1168-1176  ///
  float   repwt118     1177-1185  ///
  float   repwt119     1186-1194  ///
  float   repwt120     1195-1203  ///
  float   repwt121     1204-1212  ///
  float   repwt122     1213-1221  ///
  float   repwt123     1222-1230  ///
  float   repwt124     1231-1239  ///
  float   repwt125     1240-1248  ///
  float   repwt126     1249-1257  ///
  float   repwt127     1258-1266  ///
  float   repwt128     1267-1275  ///
  float   repwt129     1276-1284  ///
  float   repwt130     1285-1293  ///
  float   repwt131     1294-1302  ///
  float   repwt132     1303-1311  ///
  float   repwt133     1312-1320  ///
  float   repwt134     1321-1329  ///
  float   repwt135     1330-1338  ///
  float   repwt136     1339-1347  ///
  float   repwt137     1348-1356  ///
  float   repwt138     1357-1365  ///
  float   repwt139     1366-1374  ///
  float   repwt140     1375-1383  ///
  float   repwt141     1384-1392  ///
  float   repwt142     1393-1401  ///
  float   repwt143     1402-1410  ///
  float   repwt144     1411-1419  ///
  float   repwt145     1420-1428  ///
  float   repwt146     1429-1437  ///
  float   repwt147     1438-1446  ///
  float   repwt148     1447-1455  ///
  float   repwt149     1456-1464  ///
  float   repwt150     1465-1473  ///
  float   repwt151     1474-1482  ///
  float   repwt152     1483-1491  ///
  float   repwt153     1492-1500  ///
  float   repwt154     1501-1509  ///
  float   repwt155     1510-1518  ///
  float   repwt156     1519-1527  ///
  float   repwt157     1528-1536  ///
  float   repwt158     1537-1545  ///
  float   repwt159     1546-1554  ///
  float   repwt160     1555-1563  ///
  float   cpi99        1564-1567  ///
  byte    month        1568-1569  ///
  byte    pernum       1570-1571  ///
  byte    hiurule      1572-1572  ///
  byte    hiunpers     1573-1574  ///
  double  hiuid        1575-1582  ///
  int     hiufpginc    1583-1586  ///
  long    hiufpgbase   1587-1591  ///
  long    ctccrd       1592-1597  ///
  long    actccrd      1598-1602  ///
  long    ffngcare     1603-1607  ///
  long    ffngcaid     1608-1612  ///
  float   wtsupp       1613-1622  ///
  float   perwt04      1623-1630  ///
  float   earnwt       1631-1640  ///
  byte    lineno       1641-1642  ///
  byte    repwtp       1643-1643  ///
  double  hinswt       1644-1652  ///
  byte    momloc       1653-1654  ///
  byte    stepmom      1655-1655  ///
  byte    momrule      1656-1656  ///
  byte    poploc       1657-1658  ///
  byte    steppop      1659-1659  ///
  byte    poprule      1660-1660  ///
  byte    sploc        1661-1662  ///
  byte    sprule       1663-1663  ///
  byte    famsize      1664-1665  ///
  byte    nchild       1666-1666  ///
  byte    nchlt5       1667-1667  ///
  byte    famunit      1668-1669  ///
  byte    eldch        1670-1671  ///
  byte    yngch        1672-1673  ///
  byte    nsibs        1674-1674  ///
  byte    aspouse      1675-1676  ///
  byte    pecohab      1677-1678  ///
  byte    pelnmom      1679-1680  ///
  byte    pelndad      1681-1682  ///
  byte    pemomtyp     1683-1684  ///
  byte    pedadtyp     1685-1686  ///
  int     relate       1687-1690  ///
  byte    age          1691-1692  ///
  byte    sex          1693-1693  ///
  int     race         1694-1696  ///
  byte    marst        1697-1697  ///
  byte    popstat      1698-1698  ///
  long    bpl          1699-1703  ///
  int     yrimmig      1704-1707  ///
  byte    citizen      1708-1708  ///
  long    mbpl         1709-1713  ///
  long    fbpl         1714-1718  ///
  byte    nativity     1719-1719  ///
  int     hispan       1720-1722  ///
  int     educ         1723-1725  ///
  byte    educ99       1726-1727  ///
  byte    schlcoll     1728-1728  ///
  byte    empstat      1729-1730  ///
  byte    labforce     1731-1731  ///
  int     occ          1732-1735  ///
  int     occ1990      1736-1738  ///
  int     ind1990      1739-1741  ///
  int     occ1950      1742-1744  ///
  int     ind          1745-1748  ///
  int     ind1950      1749-1751  ///
  byte    classwkr     1752-1753  ///
  int     occly        1754-1757  ///
  int     occ50ly      1758-1760  ///
  int     indly        1761-1764  ///
  int     ind50ly      1765-1767  ///
  byte    classwly     1768-1769  ///
  byte    wkswork1     1770-1771  ///
  byte    wkswork2     1772-1772  ///
  byte    hrswork      1773-1774  ///
  byte    uhrswork     1775-1776  ///
  byte    wksunem1     1777-1778  ///
  byte    wksunem2     1779-1779  ///
  byte    absent       1780-1780  ///
  byte    durunem2     1781-1782  ///
  int     durunemp     1783-1785  ///
  byte    fullpart     1786-1786  ///
  byte    nwlookwk     1787-1788  ///
  float   hourwage     1789-1792  ///
  byte    paidhour     1793-1793  ///
  byte    pension      1794-1794  ///
  byte    union        1795-1795  ///
  byte    whyunemp     1796-1796  ///
  byte    firmsize     1797-1797  ///
  byte    whyabsnt     1798-1799  ///
  byte    wantjob      1800-1800  ///
  byte    whyptly      1801-1801  ///
  int     whyptlwk     1802-1804  ///
  byte    usftptlw     1805-1805  ///
  byte    payifabs     1806-1806  ///
  byte    numemps      1807-1807  ///
  byte    wnftlook     1808-1809  ///
  byte    wnlwnilf     1810-1811  ///
  byte    strechlk     1812-1812  ///
  byte    whynwly      1813-1813  ///
  byte    actnlfly     1814-1815  ///
  byte    ptweeks      1816-1817  ///
  double  ftotval      1818-1827  ///
  double  inctot       1828-1835  ///
  long    incwage      1836-1842  ///
  long    incbus       1843-1849  ///
  long    incfarm      1850-1856  ///
  long    incss        1857-1861  ///
  long    incwelfr     1862-1866  ///
  long    incretir     1867-1872  ///
  long    incssi       1873-1877  ///
  long    incint       1878-1882  ///
  long    incunemp     1883-1887  ///
  long    incwkcom     1888-1892  ///
  long    incvet       1893-1897  ///
  long    incsurv      1898-1903  ///
  long    incdisab     1904-1909  ///
  long    incdivid     1910-1915  ///
  long    incrent      1916-1920  ///
  long    inceduc      1921-1925  ///
  long    incchild     1926-1930  ///
  long    incalim      1931-1935  ///
  long    incasist     1936-1940  ///
  long    incother     1941-1945  ///
  float   earnweek     1946-1953  ///
  byte    gotvdisa     1954-1954  ///
  byte    gotveduc     1955-1955  ///
  byte    gotvothe     1956-1956  ///
  byte    gotvpens     1957-1957  ///
  byte    gotvsurv     1958-1958  ///
  long    incdisa1     1959-1963  ///
  long    incdisa2     1964-1968  ///
  long    inclongj     1969-1975  ///
  long    increti1     1976-1980  ///
  long    increti2     1981-1985  ///
  long    incsurv1     1986-1990  ///
  long    incsurv2     1991-1995  ///
  long    mthwelfr     1996-2000  ///
  long    oincbus      2001-2006  ///
  long    oincfarm     2007-2012  ///
  long    oincwage     2013-2019  ///
  byte    srcdisa1     2020-2021  ///
  byte    srcdisa2     2022-2023  ///
  byte    srcearn      2024-2024  ///
  byte    srceduc      2025-2025  ///
  byte    srcreti1     2026-2026  ///
  byte    srcreti2     2027-2027  ///
  byte    srcsurv1     2028-2029  ///
  byte    srcsurv2     2030-2031  ///
  byte    srcunemp     2032-2032  ///
  byte    srcwelfr     2033-2033  ///
  byte    srcwkcom     2034-2034  ///
  byte    ssikid       2035-2035  ///
  byte    sskid        2036-2036  ///
  byte    vetqa        2037-2037  ///
  byte    whyss1       2038-2038  ///
  byte    whyss2       2039-2039  ///
  byte    whyssi1      2040-2040  ///
  byte    whyssi2      2041-2041  ///
  int     stimulus     2042-2045  ///
  byte    offpov       2046-2047  ///
  byte    offpovuniv   2048-2049  ///
  double  offtotval    2050-2059  ///
  long    offcutoff    2060-2065  ///
  byte    offreason    2066-2067  ///
  byte    poverty      2068-2069  ///
  long    cutoff       2070-2075  ///
  long    adjginc      2076-2082  ///
  long    capgain      2083-2087  ///
  long    caploss      2088-2092  ///
  int     eitcred      2093-2096  ///
  long    fedretir     2097-2102  ///
  long    fedtax       2103-2108  ///
  long    fedtaxac     2109-2114  ///
  long    fica         2115-2119  ///
  byte    filestat     2120-2120  ///
  byte    depstat      2121-2122  ///
  byte    margtax      2123-2124  ///
  long    statetax     2125-2130  ///
  long    stataxac     2131-2136  ///
  long    taxinc       2137-2143  ///
  byte    vetstat      2144-2144  ///
  byte    vetlast      2145-2146  ///
  byte    vet1         2147-2148  ///
  byte    vet2         2149-2150  ///
  byte    vet3         2151-2152  ///
  byte    vet4         2153-2154  ///
  byte    disabwrk     2155-2155  ///
  byte    health       2156-2156  ///
  byte    quitsick     2157-2157  ///
  byte    diffhear     2158-2158  ///
  byte    diffeye      2159-2159  ///
  byte    diffrem      2160-2160  ///
  byte    diffphys     2161-2161  ///
  byte    diffmob      2162-2162  ///
  byte    diffcare     2163-2163  ///
  byte    diffany      2164-2164  ///
  byte    migsta1      2165-2166  ///
  byte    migsta5      2167-2168  ///
  byte    whymove      2169-2170  ///
  long    country      2171-2175  ///
  byte    migrate1     2176-2176  ///
  byte    migrate5     2177-2178  ///
  byte    katevac      2179-2180  ///
  byte    katevac2     2181-2182  ///
  byte    katprior     2183-2184  ///
  byte    hcovany      2185-2185  ///
  byte    hcovpriv     2186-2186  ///
  byte    hinsemp      2187-2187  ///
  byte    hinspur      2188-2188  ///
  byte    hcovpub      2189-2189  ///
  byte    hinscaid     2190-2190  ///
  byte    hinscare     2191-2191  ///
  byte    hinsmil      2192-2192  ///
  byte    inclugh      2193-2193  ///
  byte    paidgh       2194-2195  ///
  int     emcontrb     2196-2199  ///
  byte    himcaid      2200-2200  ///
  byte    himcare      2201-2201  ///
  byte    hichamp      2202-2202  ///
  byte    phinsur      2203-2203  ///
  byte    phiown       2204-2204  ///
  byte    caid         2205-2205  ///
  byte    care         2206-2206  ///
  long    pmvcaid      2207-2211  ///
  long    pmvcare      2212-2216  ///
  byte    champus      2217-2217  ///
  byte    champva      2218-2218  ///
  byte    militva      2219-2219  ///
  byte    indianh      2220-2220  ///
  byte    groupown     2221-2221  ///
  byte    groupdep     2222-2222  ///
  byte    privown      2223-2223  ///
  byte    privdep      2224-2224  ///
  byte    grouptyp     2225-2225  ///
  byte    privtyp      2226-2226  ///
  byte    grpwho1      2227-2228  ///
  byte    grpwho2      2229-2230  ///
  byte    privwho1     2231-2232  ///
  byte    privwho2     2233-2234  ///
  byte    mocaid       2235-2236  ///
  byte    schip        2237-2237  ///
  byte    verify       2238-2238  ///
  byte    groupout     2239-2239  ///
  byte    privout      2240-2240  ///
  byte    out          2241-2241  ///
  byte    covergh      2242-2242  ///
  byte    coverpi      2243-2243  ///
  byte    kidpriv      2244-2244  ///
  byte    kidcaid      2245-2245  ///
  byte    gedtrain     2246-2246  ///
  byte    gotwic       2247-2247  ///
  byte    jobclub      2248-2248  ///
  byte    jobready     2249-2249  ///
  byte    jobtrain     2250-2250  ///
  byte    kidcare      2251-2251  ///
  byte    kidcneed     2252-2252  ///
  byte    transaid     2253-2253  ///
  byte    workfare     2254-2254  ///
  byte    ftype        2255-2255  ///
  byte    wkstat       2256-2256  ///
  byte    csout        2257-2258  ///
  byte    cswhy        2259-2260  ///
  byte    cslegal      2261-2262  ///
  byte    csagree      2263-2264  ///
  byte    cstype       2265-2266  ///
  byte    cspay        2267-2268  ///
  int     csyear       2269-2272  ///
  int     csyearpay    2273-2276  ///
  byte    cssupp       2277-2278  ///
  byte    csreas       2279-2280  ///
  byte    csdduct      2281-2282  ///
  byte    cswaypay     2283-2284  ///
  byte    cschg        2285-2286  ///
  int     cschgyr      2287-2290  ///
  byte    csfreq1      2291-2292  ///
  byte    csback1      2293-2294  ///
  byte    csfreq2      2295-2296  ///
  byte    csback2      2297-2298  ///
  byte    csrecv       2299-2300  ///
  byte    csall        2301-2302  ///
  byte    cstime       2303-2304  ///
  byte    csfull       2305-2306  ///
  long    csamdue      2307-2312  ///
  long    csamrec      2313-2318  ///
  byte    cscontact    2319-2320  ///
  byte    csvis        2321-2322  ///
  byte    csvisleg     2323-2324  ///
  byte    csphycus     2325-2326  ///
  byte    cslegcus     2327-2328  ///
  int     csdays       2329-2331  ///
  int     csyrdiv      2332-2335  ///
  int     csyrmar      2336-2339  ///
  byte    cswork       2340-2341  ///
  byte    cswork5      2342-2343  ///
  byte    cselig       2344-2345  ///
  float   cssuppwt     2346-2355  ///
  byte    famkind      2356-2356  ///
  byte    eligorg      2357-2357  ///
  float   repwtp1      2358-2366  ///
  float   repwtp2      2367-2375  ///
  float   repwtp3      2376-2384  ///
  float   repwtp4      2385-2393  ///
  float   repwtp5      2394-2402  ///
  float   repwtp6      2403-2411  ///
  float   repwtp7      2412-2420  ///
  float   repwtp8      2421-2429  ///
  float   repwtp9      2430-2438  ///
  float   repwtp10     2439-2447  ///
  float   repwtp11     2448-2456  ///
  float   repwtp12     2457-2465  ///
  float   repwtp13     2466-2474  ///
  float   repwtp14     2475-2483  ///
  float   repwtp15     2484-2492  ///
  float   repwtp16     2493-2501  ///
  float   repwtp17     2502-2510  ///
  float   repwtp18     2511-2519  ///
  float   repwtp19     2520-2528  ///
  float   repwtp20     2529-2537  ///
  float   repwtp21     2538-2546  ///
  float   repwtp22     2547-2555  ///
  float   repwtp23     2556-2564  ///
  float   repwtp24     2565-2573  ///
  float   repwtp25     2574-2582  ///
  float   repwtp26     2583-2591  ///
  float   repwtp27     2592-2600  ///
  float   repwtp28     2601-2609  ///
  float   repwtp29     2610-2618  ///
  float   repwtp30     2619-2627  ///
  float   repwtp31     2628-2636  ///
  float   repwtp32     2637-2645  ///
  float   repwtp33     2646-2654  ///
  float   repwtp34     2655-2663  ///
  float   repwtp35     2664-2672  ///
  float   repwtp36     2673-2681  ///
  float   repwtp37     2682-2690  ///
  float   repwtp38     2691-2699  ///
  float   repwtp39     2700-2708  ///
  float   repwtp40     2709-2717  ///
  float   repwtp41     2718-2726  ///
  float   repwtp42     2727-2735  ///
  float   repwtp43     2736-2744  ///
  float   repwtp44     2745-2753  ///
  float   repwtp45     2754-2762  ///
  float   repwtp46     2763-2771  ///
  float   repwtp47     2772-2780  ///
  float   repwtp48     2781-2789  ///
  float   repwtp49     2790-2798  ///
  float   repwtp50     2799-2807  ///
  float   repwtp51     2808-2816  ///
  float   repwtp52     2817-2825  ///
  float   repwtp53     2826-2834  ///
  float   repwtp54     2835-2843  ///
  float   repwtp55     2844-2852  ///
  float   repwtp56     2853-2861  ///
  float   repwtp57     2862-2870  ///
  float   repwtp58     2871-2879  ///
  float   repwtp59     2880-2888  ///
  float   repwtp60     2889-2897  ///
  float   repwtp61     2898-2906  ///
  float   repwtp62     2907-2915  ///
  float   repwtp63     2916-2924  ///
  float   repwtp64     2925-2933  ///
  float   repwtp65     2934-2942  ///
  float   repwtp66     2943-2951  ///
  float   repwtp67     2952-2960  ///
  float   repwtp68     2961-2969  ///
  float   repwtp69     2970-2978  ///
  float   repwtp70     2979-2987  ///
  float   repwtp71     2988-2996  ///
  float   repwtp72     2997-3005  ///
  float   repwtp73     3006-3014  ///
  float   repwtp74     3015-3023  ///
  float   repwtp75     3024-3032  ///
  float   repwtp76     3033-3041  ///
  float   repwtp77     3042-3050  ///
  float   repwtp78     3051-3059  ///
  float   repwtp79     3060-3068  ///
  float   repwtp80     3069-3077  ///
  float   repwtp81     3078-3086  ///
  float   repwtp82     3087-3095  ///
  float   repwtp83     3096-3104  ///
  float   repwtp84     3105-3113  ///
  float   repwtp85     3114-3122  ///
  float   repwtp86     3123-3131  ///
  float   repwtp87     3132-3140  ///
  float   repwtp88     3141-3149  ///
  float   repwtp89     3150-3158  ///
  float   repwtp90     3159-3167  ///
  float   repwtp91     3168-3176  ///
  float   repwtp92     3177-3185  ///
  float   repwtp93     3186-3194  ///
  float   repwtp94     3195-3203  ///
  float   repwtp95     3204-3212  ///
  float   repwtp96     3213-3221  ///
  float   repwtp97     3222-3230  ///
  float   repwtp98     3231-3239  ///
  float   repwtp99     3240-3248  ///
  float   repwtp100    3249-3257  ///
  float   repwtp101    3258-3266  ///
  float   repwtp102    3267-3275  ///
  float   repwtp103    3276-3284  ///
  float   repwtp104    3285-3293  ///
  float   repwtp105    3294-3302  ///
  float   repwtp106    3303-3311  ///
  float   repwtp107    3312-3320  ///
  float   repwtp108    3321-3329  ///
  float   repwtp109    3330-3338  ///
  float   repwtp110    3339-3347  ///
  float   repwtp111    3348-3356  ///
  float   repwtp112    3357-3365  ///
  float   repwtp113    3366-3374  ///
  float   repwtp114    3375-3383  ///
  float   repwtp115    3384-3392  ///
  float   repwtp116    3393-3401  ///
  float   repwtp117    3402-3410  ///
  float   repwtp118    3411-3419  ///
  float   repwtp119    3420-3428  ///
  float   repwtp120    3429-3437  ///
  float   repwtp121    3438-3446  ///
  float   repwtp122    3447-3455  ///
  float   repwtp123    3456-3464  ///
  float   repwtp124    3465-3473  ///
  float   repwtp125    3474-3482  ///
  float   repwtp126    3483-3491  ///
  float   repwtp127    3492-3500  ///
  float   repwtp128    3501-3509  ///
  float   repwtp129    3510-3518  ///
  float   repwtp130    3519-3527  ///
  float   repwtp131    3528-3536  ///
  float   repwtp132    3537-3545  ///
  float   repwtp133    3546-3554  ///
  float   repwtp134    3555-3563  ///
  float   repwtp135    3564-3572  ///
  float   repwtp136    3573-3581  ///
  float   repwtp137    3582-3590  ///
  float   repwtp138    3591-3599  ///
  float   repwtp139    3600-3608  ///
  float   repwtp140    3609-3617  ///
  float   repwtp141    3618-3626  ///
  float   repwtp142    3627-3635  ///
  float   repwtp143    3636-3644  ///
  float   repwtp144    3645-3653  ///
  float   repwtp145    3654-3662  ///
  float   repwtp146    3663-3671  ///
  float   repwtp147    3672-3680  ///
  float   repwtp148    3681-3689  ///
  float   repwtp149    3690-3698  ///
  float   repwtp150    3699-3707  ///
  float   repwtp151    3708-3716  ///
  float   repwtp152    3717-3725  ///
  float   repwtp153    3726-3734  ///
  float   repwtp154    3735-3743  ///
  float   repwtp155    3744-3752  ///
  float   repwtp156    3753-3761  ///
  float   repwtp157    3762-3770  ///
  float   repwtp158    3771-3779  ///
  float   repwtp159    3780-3788  ///
  float   repwtp160    3789-3797  ///
  using `"cps_00018.dat"'

replace hwtsupp     = hwtsupp     / 10000
replace hhwt04      = hhwt04      / 100
replace repwt1      = repwt1      / 10000
replace repwt2      = repwt2      / 10000
replace repwt3      = repwt3      / 10000
replace repwt4      = repwt4      / 10000
replace repwt5      = repwt5      / 10000
replace repwt6      = repwt6      / 10000
replace repwt7      = repwt7      / 10000
replace repwt8      = repwt8      / 10000
replace repwt9      = repwt9      / 10000
replace repwt10     = repwt10     / 10000
replace repwt11     = repwt11     / 10000
replace repwt12     = repwt12     / 10000
replace repwt13     = repwt13     / 10000
replace repwt14     = repwt14     / 10000
replace repwt15     = repwt15     / 10000
replace repwt16     = repwt16     / 10000
replace repwt17     = repwt17     / 10000
replace repwt18     = repwt18     / 10000
replace repwt19     = repwt19     / 10000
replace repwt20     = repwt20     / 10000
replace repwt21     = repwt21     / 10000
replace repwt22     = repwt22     / 10000
replace repwt23     = repwt23     / 10000
replace repwt24     = repwt24     / 10000
replace repwt25     = repwt25     / 10000
replace repwt26     = repwt26     / 10000
replace repwt27     = repwt27     / 10000
replace repwt28     = repwt28     / 10000
replace repwt29     = repwt29     / 10000
replace repwt30     = repwt30     / 10000
replace repwt31     = repwt31     / 10000
replace repwt32     = repwt32     / 10000
replace repwt33     = repwt33     / 10000
replace repwt34     = repwt34     / 10000
replace repwt35     = repwt35     / 10000
replace repwt36     = repwt36     / 10000
replace repwt37     = repwt37     / 10000
replace repwt38     = repwt38     / 10000
replace repwt39     = repwt39     / 10000
replace repwt40     = repwt40     / 10000
replace repwt41     = repwt41     / 10000
replace repwt42     = repwt42     / 10000
replace repwt43     = repwt43     / 10000
replace repwt44     = repwt44     / 10000
replace repwt45     = repwt45     / 10000
replace repwt46     = repwt46     / 10000
replace repwt47     = repwt47     / 10000
replace repwt48     = repwt48     / 10000
replace repwt49     = repwt49     / 10000
replace repwt50     = repwt50     / 10000
replace repwt51     = repwt51     / 10000
replace repwt52     = repwt52     / 10000
replace repwt53     = repwt53     / 10000
replace repwt54     = repwt54     / 10000
replace repwt55     = repwt55     / 10000
replace repwt56     = repwt56     / 10000
replace repwt57     = repwt57     / 10000
replace repwt58     = repwt58     / 10000
replace repwt59     = repwt59     / 10000
replace repwt60     = repwt60     / 10000
replace repwt61     = repwt61     / 10000
replace repwt62     = repwt62     / 10000
replace repwt63     = repwt63     / 10000
replace repwt64     = repwt64     / 10000
replace repwt65     = repwt65     / 10000
replace repwt66     = repwt66     / 10000
replace repwt67     = repwt67     / 10000
replace repwt68     = repwt68     / 10000
replace repwt69     = repwt69     / 10000
replace repwt70     = repwt70     / 10000
replace repwt71     = repwt71     / 10000
replace repwt72     = repwt72     / 10000
replace repwt73     = repwt73     / 10000
replace repwt74     = repwt74     / 10000
replace repwt75     = repwt75     / 10000
replace repwt76     = repwt76     / 10000
replace repwt77     = repwt77     / 10000
replace repwt78     = repwt78     / 10000
replace repwt79     = repwt79     / 10000
replace repwt80     = repwt80     / 10000
replace repwt81     = repwt81     / 10000
replace repwt82     = repwt82     / 10000
replace repwt83     = repwt83     / 10000
replace repwt84     = repwt84     / 10000
replace repwt85     = repwt85     / 10000
replace repwt86     = repwt86     / 10000
replace repwt87     = repwt87     / 10000
replace repwt88     = repwt88     / 10000
replace repwt89     = repwt89     / 10000
replace repwt90     = repwt90     / 10000
replace repwt91     = repwt91     / 10000
replace repwt92     = repwt92     / 10000
replace repwt93     = repwt93     / 10000
replace repwt94     = repwt94     / 10000
replace repwt95     = repwt95     / 10000
replace repwt96     = repwt96     / 10000
replace repwt97     = repwt97     / 10000
replace repwt98     = repwt98     / 10000
replace repwt99     = repwt99     / 10000
replace repwt100    = repwt100    / 10000
replace repwt101    = repwt101    / 10000
replace repwt102    = repwt102    / 10000
replace repwt103    = repwt103    / 10000
replace repwt104    = repwt104    / 10000
replace repwt105    = repwt105    / 10000
replace repwt106    = repwt106    / 10000
replace repwt107    = repwt107    / 10000
replace repwt108    = repwt108    / 10000
replace repwt109    = repwt109    / 10000
replace repwt110    = repwt110    / 10000
replace repwt111    = repwt111    / 10000
replace repwt112    = repwt112    / 10000
replace repwt113    = repwt113    / 10000
replace repwt114    = repwt114    / 10000
replace repwt115    = repwt115    / 10000
replace repwt116    = repwt116    / 10000
replace repwt117    = repwt117    / 10000
replace repwt118    = repwt118    / 10000
replace repwt119    = repwt119    / 10000
replace repwt120    = repwt120    / 10000
replace repwt121    = repwt121    / 10000
replace repwt122    = repwt122    / 10000
replace repwt123    = repwt123    / 10000
replace repwt124    = repwt124    / 10000
replace repwt125    = repwt125    / 10000
replace repwt126    = repwt126    / 10000
replace repwt127    = repwt127    / 10000
replace repwt128    = repwt128    / 10000
replace repwt129    = repwt129    / 10000
replace repwt130    = repwt130    / 10000
replace repwt131    = repwt131    / 10000
replace repwt132    = repwt132    / 10000
replace repwt133    = repwt133    / 10000
replace repwt134    = repwt134    / 10000
replace repwt135    = repwt135    / 10000
replace repwt136    = repwt136    / 10000
replace repwt137    = repwt137    / 10000
replace repwt138    = repwt138    / 10000
replace repwt139    = repwt139    / 10000
replace repwt140    = repwt140    / 10000
replace repwt141    = repwt141    / 10000
replace repwt142    = repwt142    / 10000
replace repwt143    = repwt143    / 10000
replace repwt144    = repwt144    / 10000
replace repwt145    = repwt145    / 10000
replace repwt146    = repwt146    / 10000
replace repwt147    = repwt147    / 10000
replace repwt148    = repwt148    / 10000
replace repwt149    = repwt149    / 10000
replace repwt150    = repwt150    / 10000
replace repwt151    = repwt151    / 10000
replace repwt152    = repwt152    / 10000
replace repwt153    = repwt153    / 10000
replace repwt154    = repwt154    / 10000
replace repwt155    = repwt155    / 10000
replace repwt156    = repwt156    / 10000
replace repwt157    = repwt157    / 10000
replace repwt158    = repwt158    / 10000
replace repwt159    = repwt159    / 10000
replace repwt160    = repwt160    / 10000
replace cpi99       = cpi99       / 1000
replace wtsupp      = wtsupp      / 10000
replace perwt04     = perwt04     / 100
replace earnwt      = earnwt      / 10000
replace hourwage    = hourwage    / 100
replace earnweek    = earnweek    / 100
replace cssuppwt    = cssuppwt    / 10000
replace repwtp1     = repwtp1     / 10000
replace repwtp2     = repwtp2     / 10000
replace repwtp3     = repwtp3     / 10000
replace repwtp4     = repwtp4     / 10000
replace repwtp5     = repwtp5     / 10000
replace repwtp6     = repwtp6     / 10000
replace repwtp7     = repwtp7     / 10000
replace repwtp8     = repwtp8     / 10000
replace repwtp9     = repwtp9     / 10000
replace repwtp10    = repwtp10    / 10000
replace repwtp11    = repwtp11    / 10000
replace repwtp12    = repwtp12    / 10000
replace repwtp13    = repwtp13    / 10000
replace repwtp14    = repwtp14    / 10000
replace repwtp15    = repwtp15    / 10000
replace repwtp16    = repwtp16    / 10000
replace repwtp17    = repwtp17    / 10000
replace repwtp18    = repwtp18    / 10000
replace repwtp19    = repwtp19    / 10000
replace repwtp20    = repwtp20    / 10000
replace repwtp21    = repwtp21    / 10000
replace repwtp22    = repwtp22    / 10000
replace repwtp23    = repwtp23    / 10000
replace repwtp24    = repwtp24    / 10000
replace repwtp25    = repwtp25    / 10000
replace repwtp26    = repwtp26    / 10000
replace repwtp27    = repwtp27    / 10000
replace repwtp28    = repwtp28    / 10000
replace repwtp29    = repwtp29    / 10000
replace repwtp30    = repwtp30    / 10000
replace repwtp31    = repwtp31    / 10000
replace repwtp32    = repwtp32    / 10000
replace repwtp33    = repwtp33    / 10000
replace repwtp34    = repwtp34    / 10000
replace repwtp35    = repwtp35    / 10000
replace repwtp36    = repwtp36    / 10000
replace repwtp37    = repwtp37    / 10000
replace repwtp38    = repwtp38    / 10000
replace repwtp39    = repwtp39    / 10000
replace repwtp40    = repwtp40    / 10000
replace repwtp41    = repwtp41    / 10000
replace repwtp42    = repwtp42    / 10000
replace repwtp43    = repwtp43    / 10000
replace repwtp44    = repwtp44    / 10000
replace repwtp45    = repwtp45    / 10000
replace repwtp46    = repwtp46    / 10000
replace repwtp47    = repwtp47    / 10000
replace repwtp48    = repwtp48    / 10000
replace repwtp49    = repwtp49    / 10000
replace repwtp50    = repwtp50    / 10000
replace repwtp51    = repwtp51    / 10000
replace repwtp52    = repwtp52    / 10000
replace repwtp53    = repwtp53    / 10000
replace repwtp54    = repwtp54    / 10000
replace repwtp55    = repwtp55    / 10000
replace repwtp56    = repwtp56    / 10000
replace repwtp57    = repwtp57    / 10000
replace repwtp58    = repwtp58    / 10000
replace repwtp59    = repwtp59    / 10000
replace repwtp60    = repwtp60    / 10000
replace repwtp61    = repwtp61    / 10000
replace repwtp62    = repwtp62    / 10000
replace repwtp63    = repwtp63    / 10000
replace repwtp64    = repwtp64    / 10000
replace repwtp65    = repwtp65    / 10000
replace repwtp66    = repwtp66    / 10000
replace repwtp67    = repwtp67    / 10000
replace repwtp68    = repwtp68    / 10000
replace repwtp69    = repwtp69    / 10000
replace repwtp70    = repwtp70    / 10000
replace repwtp71    = repwtp71    / 10000
replace repwtp72    = repwtp72    / 10000
replace repwtp73    = repwtp73    / 10000
replace repwtp74    = repwtp74    / 10000
replace repwtp75    = repwtp75    / 10000
replace repwtp76    = repwtp76    / 10000
replace repwtp77    = repwtp77    / 10000
replace repwtp78    = repwtp78    / 10000
replace repwtp79    = repwtp79    / 10000
replace repwtp80    = repwtp80    / 10000
replace repwtp81    = repwtp81    / 10000
replace repwtp82    = repwtp82    / 10000
replace repwtp83    = repwtp83    / 10000
replace repwtp84    = repwtp84    / 10000
replace repwtp85    = repwtp85    / 10000
replace repwtp86    = repwtp86    / 10000
replace repwtp87    = repwtp87    / 10000
replace repwtp88    = repwtp88    / 10000
replace repwtp89    = repwtp89    / 10000
replace repwtp90    = repwtp90    / 10000
replace repwtp91    = repwtp91    / 10000
replace repwtp92    = repwtp92    / 10000
replace repwtp93    = repwtp93    / 10000
replace repwtp94    = repwtp94    / 10000
replace repwtp95    = repwtp95    / 10000
replace repwtp96    = repwtp96    / 10000
replace repwtp97    = repwtp97    / 10000
replace repwtp98    = repwtp98    / 10000
replace repwtp99    = repwtp99    / 10000
replace repwtp100   = repwtp100   / 10000
replace repwtp101   = repwtp101   / 10000
replace repwtp102   = repwtp102   / 10000
replace repwtp103   = repwtp103   / 10000
replace repwtp104   = repwtp104   / 10000
replace repwtp105   = repwtp105   / 10000
replace repwtp106   = repwtp106   / 10000
replace repwtp107   = repwtp107   / 10000
replace repwtp108   = repwtp108   / 10000
replace repwtp109   = repwtp109   / 10000
replace repwtp110   = repwtp110   / 10000
replace repwtp111   = repwtp111   / 10000
replace repwtp112   = repwtp112   / 10000
replace repwtp113   = repwtp113   / 10000
replace repwtp114   = repwtp114   / 10000
replace repwtp115   = repwtp115   / 10000
replace repwtp116   = repwtp116   / 10000
replace repwtp117   = repwtp117   / 10000
replace repwtp118   = repwtp118   / 10000
replace repwtp119   = repwtp119   / 10000
replace repwtp120   = repwtp120   / 10000
replace repwtp121   = repwtp121   / 10000
replace repwtp122   = repwtp122   / 10000
replace repwtp123   = repwtp123   / 10000
replace repwtp124   = repwtp124   / 10000
replace repwtp125   = repwtp125   / 10000
replace repwtp126   = repwtp126   / 10000
replace repwtp127   = repwtp127   / 10000
replace repwtp128   = repwtp128   / 10000
replace repwtp129   = repwtp129   / 10000
replace repwtp130   = repwtp130   / 10000
replace repwtp131   = repwtp131   / 10000
replace repwtp132   = repwtp132   / 10000
replace repwtp133   = repwtp133   / 10000
replace repwtp134   = repwtp134   / 10000
replace repwtp135   = repwtp135   / 10000
replace repwtp136   = repwtp136   / 10000
replace repwtp137   = repwtp137   / 10000
replace repwtp138   = repwtp138   / 10000
replace repwtp139   = repwtp139   / 10000
replace repwtp140   = repwtp140   / 10000
replace repwtp141   = repwtp141   / 10000
replace repwtp142   = repwtp142   / 10000
replace repwtp143   = repwtp143   / 10000
replace repwtp144   = repwtp144   / 10000
replace repwtp145   = repwtp145   / 10000
replace repwtp146   = repwtp146   / 10000
replace repwtp147   = repwtp147   / 10000
replace repwtp148   = repwtp148   / 10000
replace repwtp149   = repwtp149   / 10000
replace repwtp150   = repwtp150   / 10000
replace repwtp151   = repwtp151   / 10000
replace repwtp152   = repwtp152   / 10000
replace repwtp153   = repwtp153   / 10000
replace repwtp154   = repwtp154   / 10000
replace repwtp155   = repwtp155   / 10000
replace repwtp156   = repwtp156   / 10000
replace repwtp157   = repwtp157   / 10000
replace repwtp158   = repwtp158   / 10000
replace repwtp159   = repwtp159   / 10000
replace repwtp160   = repwtp160   / 10000

format hwtsupp     %10.4f
format hhwt04      %8.2f
format hrhhid      %15.0f
format hhincome    %8.0f
format repwt1      %9.4f
format repwt2      %9.4f
format repwt3      %9.4f
format repwt4      %9.4f
format repwt5      %9.4f
format repwt6      %9.4f
format repwt7      %9.4f
format repwt8      %9.4f
format repwt9      %9.4f
format repwt10     %9.4f
format repwt11     %9.4f
format repwt12     %9.4f
format repwt13     %9.4f
format repwt14     %9.4f
format repwt15     %9.4f
format repwt16     %9.4f
format repwt17     %9.4f
format repwt18     %9.4f
format repwt19     %9.4f
format repwt20     %9.4f
format repwt21     %9.4f
format repwt22     %9.4f
format repwt23     %9.4f
format repwt24     %9.4f
format repwt25     %9.4f
format repwt26     %9.4f
format repwt27     %9.4f
format repwt28     %9.4f
format repwt29     %9.4f
format repwt30     %9.4f
format repwt31     %9.4f
format repwt32     %9.4f
format repwt33     %9.4f
format repwt34     %9.4f
format repwt35     %9.4f
format repwt36     %9.4f
format repwt37     %9.4f
format repwt38     %9.4f
format repwt39     %9.4f
format repwt40     %9.4f
format repwt41     %9.4f
format repwt42     %9.4f
format repwt43     %9.4f
format repwt44     %9.4f
format repwt45     %9.4f
format repwt46     %9.4f
format repwt47     %9.4f
format repwt48     %9.4f
format repwt49     %9.4f
format repwt50     %9.4f
format repwt51     %9.4f
format repwt52     %9.4f
format repwt53     %9.4f
format repwt54     %9.4f
format repwt55     %9.4f
format repwt56     %9.4f
format repwt57     %9.4f
format repwt58     %9.4f
format repwt59     %9.4f
format repwt60     %9.4f
format repwt61     %9.4f
format repwt62     %9.4f
format repwt63     %9.4f
format repwt64     %9.4f
format repwt65     %9.4f
format repwt66     %9.4f
format repwt67     %9.4f
format repwt68     %9.4f
format repwt69     %9.4f
format repwt70     %9.4f
format repwt71     %9.4f
format repwt72     %9.4f
format repwt73     %9.4f
format repwt74     %9.4f
format repwt75     %9.4f
format repwt76     %9.4f
format repwt77     %9.4f
format repwt78     %9.4f
format repwt79     %9.4f
format repwt80     %9.4f
format repwt81     %9.4f
format repwt82     %9.4f
format repwt83     %9.4f
format repwt84     %9.4f
format repwt85     %9.4f
format repwt86     %9.4f
format repwt87     %9.4f
format repwt88     %9.4f
format repwt89     %9.4f
format repwt90     %9.4f
format repwt91     %9.4f
format repwt92     %9.4f
format repwt93     %9.4f
format repwt94     %9.4f
format repwt95     %9.4f
format repwt96     %9.4f
format repwt97     %9.4f
format repwt98     %9.4f
format repwt99     %9.4f
format repwt100    %9.4f
format repwt101    %9.4f
format repwt102    %9.4f
format repwt103    %9.4f
format repwt104    %9.4f
format repwt105    %9.4f
format repwt106    %9.4f
format repwt107    %9.4f
format repwt108    %9.4f
format repwt109    %9.4f
format repwt110    %9.4f
format repwt111    %9.4f
format repwt112    %9.4f
format repwt113    %9.4f
format repwt114    %9.4f
format repwt115    %9.4f
format repwt116    %9.4f
format repwt117    %9.4f
format repwt118    %9.4f
format repwt119    %9.4f
format repwt120    %9.4f
format repwt121    %9.4f
format repwt122    %9.4f
format repwt123    %9.4f
format repwt124    %9.4f
format repwt125    %9.4f
format repwt126    %9.4f
format repwt127    %9.4f
format repwt128    %9.4f
format repwt129    %9.4f
format repwt130    %9.4f
format repwt131    %9.4f
format repwt132    %9.4f
format repwt133    %9.4f
format repwt134    %9.4f
format repwt135    %9.4f
format repwt136    %9.4f
format repwt137    %9.4f
format repwt138    %9.4f
format repwt139    %9.4f
format repwt140    %9.4f
format repwt141    %9.4f
format repwt142    %9.4f
format repwt143    %9.4f
format repwt144    %9.4f
format repwt145    %9.4f
format repwt146    %9.4f
format repwt147    %9.4f
format repwt148    %9.4f
format repwt149    %9.4f
format repwt150    %9.4f
format repwt151    %9.4f
format repwt152    %9.4f
format repwt153    %9.4f
format repwt154    %9.4f
format repwt155    %9.4f
format repwt156    %9.4f
format repwt157    %9.4f
format repwt158    %9.4f
format repwt159    %9.4f
format repwt160    %9.4f
format cpi99       %4.3f
format hiuid       %8.0f
format wtsupp      %10.4f
format perwt04     %8.2f
format earnwt      %10.4f
format hinswt      %9.0f
format hourwage    %4.2f
format ftotval     %10.0f
format inctot      %8.0f
format earnweek    %8.2f
format offtotval   %10.0f
format cssuppwt    %10.4f
format repwtp1     %9.4f
format repwtp2     %9.4f
format repwtp3     %9.4f
format repwtp4     %9.4f
format repwtp5     %9.4f
format repwtp6     %9.4f
format repwtp7     %9.4f
format repwtp8     %9.4f
format repwtp9     %9.4f
format repwtp10    %9.4f
format repwtp11    %9.4f
format repwtp12    %9.4f
format repwtp13    %9.4f
format repwtp14    %9.4f
format repwtp15    %9.4f
format repwtp16    %9.4f
format repwtp17    %9.4f
format repwtp18    %9.4f
format repwtp19    %9.4f
format repwtp20    %9.4f
format repwtp21    %9.4f
format repwtp22    %9.4f
format repwtp23    %9.4f
format repwtp24    %9.4f
format repwtp25    %9.4f
format repwtp26    %9.4f
format repwtp27    %9.4f
format repwtp28    %9.4f
format repwtp29    %9.4f
format repwtp30    %9.4f
format repwtp31    %9.4f
format repwtp32    %9.4f
format repwtp33    %9.4f
format repwtp34    %9.4f
format repwtp35    %9.4f
format repwtp36    %9.4f
format repwtp37    %9.4f
format repwtp38    %9.4f
format repwtp39    %9.4f
format repwtp40    %9.4f
format repwtp41    %9.4f
format repwtp42    %9.4f
format repwtp43    %9.4f
format repwtp44    %9.4f
format repwtp45    %9.4f
format repwtp46    %9.4f
format repwtp47    %9.4f
format repwtp48    %9.4f
format repwtp49    %9.4f
format repwtp50    %9.4f
format repwtp51    %9.4f
format repwtp52    %9.4f
format repwtp53    %9.4f
format repwtp54    %9.4f
format repwtp55    %9.4f
format repwtp56    %9.4f
format repwtp57    %9.4f
format repwtp58    %9.4f
format repwtp59    %9.4f
format repwtp60    %9.4f
format repwtp61    %9.4f
format repwtp62    %9.4f
format repwtp63    %9.4f
format repwtp64    %9.4f
format repwtp65    %9.4f
format repwtp66    %9.4f
format repwtp67    %9.4f
format repwtp68    %9.4f
format repwtp69    %9.4f
format repwtp70    %9.4f
format repwtp71    %9.4f
format repwtp72    %9.4f
format repwtp73    %9.4f
format repwtp74    %9.4f
format repwtp75    %9.4f
format repwtp76    %9.4f
format repwtp77    %9.4f
format repwtp78    %9.4f
format repwtp79    %9.4f
format repwtp80    %9.4f
format repwtp81    %9.4f
format repwtp82    %9.4f
format repwtp83    %9.4f
format repwtp84    %9.4f
format repwtp85    %9.4f
format repwtp86    %9.4f
format repwtp87    %9.4f
format repwtp88    %9.4f
format repwtp89    %9.4f
format repwtp90    %9.4f
format repwtp91    %9.4f
format repwtp92    %9.4f
format repwtp93    %9.4f
format repwtp94    %9.4f
format repwtp95    %9.4f
format repwtp96    %9.4f
format repwtp97    %9.4f
format repwtp98    %9.4f
format repwtp99    %9.4f
format repwtp100   %9.4f
format repwtp101   %9.4f
format repwtp102   %9.4f
format repwtp103   %9.4f
format repwtp104   %9.4f
format repwtp105   %9.4f
format repwtp106   %9.4f
format repwtp107   %9.4f
format repwtp108   %9.4f
format repwtp109   %9.4f
format repwtp110   %9.4f
format repwtp111   %9.4f
format repwtp112   %9.4f
format repwtp113   %9.4f
format repwtp114   %9.4f
format repwtp115   %9.4f
format repwtp116   %9.4f
format repwtp117   %9.4f
format repwtp118   %9.4f
format repwtp119   %9.4f
format repwtp120   %9.4f
format repwtp121   %9.4f
format repwtp122   %9.4f
format repwtp123   %9.4f
format repwtp124   %9.4f
format repwtp125   %9.4f
format repwtp126   %9.4f
format repwtp127   %9.4f
format repwtp128   %9.4f
format repwtp129   %9.4f
format repwtp130   %9.4f
format repwtp131   %9.4f
format repwtp132   %9.4f
format repwtp133   %9.4f
format repwtp134   %9.4f
format repwtp135   %9.4f
format repwtp136   %9.4f
format repwtp137   %9.4f
format repwtp138   %9.4f
format repwtp139   %9.4f
format repwtp140   %9.4f
format repwtp141   %9.4f
format repwtp142   %9.4f
format repwtp143   %9.4f
format repwtp144   %9.4f
format repwtp145   %9.4f
format repwtp146   %9.4f
format repwtp147   %9.4f
format repwtp148   %9.4f
format repwtp149   %9.4f
format repwtp150   %9.4f
format repwtp151   %9.4f
format repwtp152   %9.4f
format repwtp153   %9.4f
format repwtp154   %9.4f
format repwtp155   %9.4f
format repwtp156   %9.4f
format repwtp157   %9.4f
format repwtp158   %9.4f
format repwtp159   %9.4f
format repwtp160   %9.4f

label var rectype     `"Record type"'
label var year        `"Survey year"'
label var serial      `"Household serial number"'
label var mish        `"Month in sample, household level"'
label var numprec     `"Number of person records following"'
label var hwtsupp     `"Household weight, Supplement"'
label var hhwt04      `"Household weight 2004"'
label var hrhhid      `"Household ID, part 1"'
label var hrhhid2     `"Household ID, part 2"'
label var huhhnum     `"Household number"'
label var gq          `"Group Quarters status"'
label var hhintype    `"Type of household"'
label var repwt       `"Household replicate weights"'
label var region      `"Region and division"'
label var statefip    `"State (FIPS code)"'
label var statecensus `"State (Census code)"'
label var asecflag    `"Flag for ASEC"'
label var metro       `"Metropolitan central city status"'
label var metarea     `"Metropolitan area"'
label var county      `"FIPS county code"'
label var ownershp    `"Ownership of dwelling"'
label var hhincome    `"Total household income"'
label var housret     `"Return to home equity"'
label var proptax     `"Annual property taxes"'
label var pubhous     `"Living in public housing"'
label var rentsub     `"Paying lower rent due to government subsidy"'
label var heatsub     `"Received energy subsidy"'
label var heatval     `"Value of energy subsidy"'
label var foodstmp    `"Food stamp recipiency"'
label var stampno     `"Number of persons covered by food stamps"'
label var stampmo     `"Number of months received food stamps"'
label var stampval    `"Total value of food stamps"'
label var atelunch    `"Number of children who ate complete school lunch"'
label var lunchsub    `"Government school lunch food subsidy"'
label var frelunch    `"Number of children with government school lunch subsidy"'
label var unitsstr    `"Units in structure"'
label var phone       `"Telephone availability"'
label var nfams       `"Number of families in household"'
label var ncouples    `"Number of married couples in household"'
label var nmothers    `"Number of mothers in household"'
label var nfathers    `"Number of fathers in household"'
label var repwt1      `"Household replicate weight 1"'
label var repwt2      `"Household replicate weight 2"'
label var repwt3      `"Household replicate weight 3"'
label var repwt4      `"Household replicate weight 4"'
label var repwt5      `"Household replicate weight 5"'
label var repwt6      `"Household replicate weight 6"'
label var repwt7      `"Household replicate weight 7"'
label var repwt8      `"Household replicate weight 8"'
label var repwt9      `"Household replicate weight 9"'
label var repwt10     `"Household replicate weight 10"'
label var repwt11     `"Household replicate weight 11"'
label var repwt12     `"Household replicate weight 12"'
label var repwt13     `"Household replicate weight 13"'
label var repwt14     `"Household replicate weight 14"'
label var repwt15     `"Household replicate weight 15"'
label var repwt16     `"Household replicate weight 16"'
label var repwt17     `"Household replicate weight 17"'
label var repwt18     `"Household replicate weight 18"'
label var repwt19     `"Household replicate weight 19"'
label var repwt20     `"Household replicate weight 20"'
label var repwt21     `"Household replicate weight 21"'
label var repwt22     `"Household replicate weight 22"'
label var repwt23     `"Household replicate weight 23"'
label var repwt24     `"Household replicate weight 24"'
label var repwt25     `"Household replicate weight 25"'
label var repwt26     `"Household replicate weight 26"'
label var repwt27     `"Household replicate weight 27"'
label var repwt28     `"Household replicate weight 28"'
label var repwt29     `"Household replicate weight 29"'
label var repwt30     `"Household replicate weight 30"'
label var repwt31     `"Household replicate weight 31"'
label var repwt32     `"Household replicate weight 32"'
label var repwt33     `"Household replicate weight 33"'
label var repwt34     `"Household replicate weight 34"'
label var repwt35     `"Household replicate weight 35"'
label var repwt36     `"Household replicate weight 36"'
label var repwt37     `"Household replicate weight 37"'
label var repwt38     `"Household replicate weight 38"'
label var repwt39     `"Household replicate weight 39"'
label var repwt40     `"Household replicate weight 40"'
label var repwt41     `"Household replicate weight 41"'
label var repwt42     `"Household replicate weight 42"'
label var repwt43     `"Household replicate weight 43"'
label var repwt44     `"Household replicate weight 44"'
label var repwt45     `"Household replicate weight 45"'
label var repwt46     `"Household replicate weight 46"'
label var repwt47     `"Household replicate weight 47"'
label var repwt48     `"Household replicate weight 48"'
label var repwt49     `"Household replicate weight 49"'
label var repwt50     `"Household replicate weight 50"'
label var repwt51     `"Household replicate weight 51"'
label var repwt52     `"Household replicate weight 52"'
label var repwt53     `"Household replicate weight 53"'
label var repwt54     `"Household replicate weight 54"'
label var repwt55     `"Household replicate weight 55"'
label var repwt56     `"Household replicate weight 56"'
label var repwt57     `"Household replicate weight 57"'
label var repwt58     `"Household replicate weight 58"'
label var repwt59     `"Household replicate weight 59"'
label var repwt60     `"Household replicate weight 60"'
label var repwt61     `"Household replicate weight 61"'
label var repwt62     `"Household replicate weight 62"'
label var repwt63     `"Household replicate weight 63"'
label var repwt64     `"Household replicate weight 64"'
label var repwt65     `"Household replicate weight 65"'
label var repwt66     `"Household replicate weight 66"'
label var repwt67     `"Household replicate weight 67"'
label var repwt68     `"Household replicate weight 68"'
label var repwt69     `"Household replicate weight 69"'
label var repwt70     `"Household replicate weight 70"'
label var repwt71     `"Household replicate weight 71"'
label var repwt72     `"Household replicate weight 72"'
label var repwt73     `"Household replicate weight 73"'
label var repwt74     `"Household replicate weight 74"'
label var repwt75     `"Household replicate weight 75"'
label var repwt76     `"Household replicate weight 76"'
label var repwt77     `"Household replicate weight 77"'
label var repwt78     `"Household replicate weight 78"'
label var repwt79     `"Household replicate weight 79"'
label var repwt80     `"Household replicate weight 80"'
label var repwt81     `"Household replicate weight 81"'
label var repwt82     `"Household replicate weight 82"'
label var repwt83     `"Household replicate weight 83"'
label var repwt84     `"Household replicate weight 84"'
label var repwt85     `"Household replicate weight 85"'
label var repwt86     `"Household replicate weight 86"'
label var repwt87     `"Household replicate weight 87"'
label var repwt88     `"Household replicate weight 88"'
label var repwt89     `"Household replicate weight 89"'
label var repwt90     `"Household replicate weight 90"'
label var repwt91     `"Household replicate weight 91"'
label var repwt92     `"Household replicate weight 92"'
label var repwt93     `"Household replicate weight 93"'
label var repwt94     `"Household replicate weight 94"'
label var repwt95     `"Household replicate weight 95"'
label var repwt96     `"Household replicate weight 96"'
label var repwt97     `"Household replicate weight 97"'
label var repwt98     `"Household replicate weight 98"'
label var repwt99     `"Household replicate weight 99"'
label var repwt100    `"Household replicate weight 100"'
label var repwt101    `"Household replicate weight 101"'
label var repwt102    `"Household replicate weight 102"'
label var repwt103    `"Household replicate weight 103"'
label var repwt104    `"Household replicate weight 104"'
label var repwt105    `"Household replicate weight 105"'
label var repwt106    `"Household replicate weight 106"'
label var repwt107    `"Household replicate weight 107"'
label var repwt108    `"Household replicate weight 108"'
label var repwt109    `"Household replicate weight 109"'
label var repwt110    `"Household replicate weight 110"'
label var repwt111    `"Household replicate weight 111"'
label var repwt112    `"Household replicate weight 112"'
label var repwt113    `"Household replicate weight 113"'
label var repwt114    `"Household replicate weight 114"'
label var repwt115    `"Household replicate weight 115"'
label var repwt116    `"Household replicate weight 116"'
label var repwt117    `"Household replicate weight 117"'
label var repwt118    `"Household replicate weight 118"'
label var repwt119    `"Household replicate weight119"'
label var repwt120    `"Household replicate weight 120"'
label var repwt121    `"Household replicate weight 121"'
label var repwt122    `"Household replicate weight 122"'
label var repwt123    `"Household replicate weight 123"'
label var repwt124    `"Household replicate weight 124"'
label var repwt125    `"Household replicate weight 125"'
label var repwt126    `"Household replicate weight 126"'
label var repwt127    `"Household replicate weight 127"'
label var repwt128    `"Household replicate weight 128"'
label var repwt129    `"Household replicate weight 129"'
label var repwt130    `"Household replicate weight 130"'
label var repwt131    `"Household replicate weight 131"'
label var repwt132    `"Household replicate weight 132"'
label var repwt133    `"Household replicate weight 133"'
label var repwt134    `"Household replicate weight 134"'
label var repwt135    `"Household replicate weight 135"'
label var repwt136    `"Household replicate weight 136"'
label var repwt137    `"Household replicate weight 137"'
label var repwt138    `"Household replicate weight 138"'
label var repwt139    `"Household replicate weight 139"'
label var repwt140    `"Household replicate weight 140"'
label var repwt141    `"Household replicate weight 141"'
label var repwt142    `"Household replicate weight 142"'
label var repwt143    `"Household replicate weight 143"'
label var repwt144    `"Household replicate weight 144"'
label var repwt145    `"Household replicate weight 145"'
label var repwt146    `"Household replicate weight 146"'
label var repwt147    `"Household replicate weight 147"'
label var repwt148    `"Household replicate weight 148"'
label var repwt149    `"Household replicate weight 149"'
label var repwt150    `"Household replicate weight 150"'
label var repwt151    `"Household replicate weight 151"'
label var repwt152    `"Household replicate weight 152"'
label var repwt153    `"Household replicate weight 153"'
label var repwt154    `"Household replicate weight 154"'
label var repwt155    `"Household replicate weight 155"'
label var repwt156    `"Household replicate weight 156"'
label var repwt157    `"Household replicate weight 157"'
label var repwt158    `"Household replicate weight 158"'
label var repwt159    `"Household replicate weight 159"'
label var repwt160    `"Household replicate weight 160"'
label var cpi99       `"CPI-U adjustment factor to 1999 dollars"'
label var month       `"Month"'
label var pernum      `"Person number in sample unit"'
label var hiurule     `"HIU pointer rule"'
label var hiunpers    `"HIU number of persons"'
label var hiuid       `"HIU identification"'
label var hiufpginc   `"Federal poverty guidelines (increment)"'
label var hiufpgbase  `"Federal poverty guidelines (base)"'
label var ctccrd      `"Child Tax Credit"'
label var actccrd     `"Additional Child Tax Credit"'
label var ffngcare    `"Family fungible value of Medicare"'
label var ffngcaid    `"Family fungible value of Medicaid"'
label var wtsupp      `"Supplement Weight"'
label var perwt04     `"Person weight 2004"'
label var earnwt      `"Earnings weight"'
label var lineno      `"Line number on original form"'
label var repwtp      `"Person replicate weights"'
label var hinswt      `"Summary health insurance weight"'
label var momloc      `"Mother's location in the household"'
label var stepmom     `"Probable step/adopted mother"'
label var momrule     `"Rule for linking mother"'
label var poploc      `"Father's location in the household"'
label var steppop     `"Probable step/adopted father"'
label var poprule     `"Rule for linking father"'
label var sploc       `"Spouse's location in household"'
label var sprule      `"Rule for linking spouse"'
label var famsize     `"Number of own family members in hh"'
label var nchild      `"Number of own children in household"'
label var nchlt5      `"Number of own children under age 5 in hh"'
label var famunit     `"Family unit membership"'
label var eldch       `"Age of eldest own child in household"'
label var yngch       `"Age of youngest own child in household"'
label var nsibs       `"Number of own siblings in household"'
label var aspouse     `"Spouse line number (self-reported)"'
label var pecohab     `"Cohabiting partner line number (self-reported)"'
label var pelnmom     `"Mother's line number (self-reported)"'
label var pelndad     `"Father's line number (self-reported)"'
label var pemomtyp    `"Mother's relationship to child (self-reported)"'
label var pedadtyp    `"Father's relationship to child (self-reported)"'
label var relate      `"Relationship to household head"'
label var age         `"Age"'
label var sex         `"Sex"'
label var race        `"Race"'
label var marst       `"Marital status"'
label var popstat     `"Adult civilian, armed forces, or child"'
label var bpl         `"Birthplace"'
label var yrimmig     `"Year of immigration"'
label var citizen     `"Citizenship status"'
label var mbpl        `"Mother's birthplace"'
label var fbpl        `"Father's birthplace"'
label var nativity    `"Foreign birthplace or parentage"'
label var hispan      `"Hispanic origin"'
label var educ        `"Educational attainment recode"'
label var educ99      `"Educational attainment, 1990"'
label var schlcoll    `"School or college attendance"'
label var empstat     `"Employment status"'
label var labforce    `"Labor force status"'
label var occ         `"Occupation"'
label var occ1990     `"Occupation, 1990 basis"'
label var ind1990     `"Industry, 1990 basis"'
label var occ1950     `"Occupation, 1950 basis"'
label var ind         `"Industry"'
label var ind1950     `"Industry, 1950 basis"'
label var classwkr    `"Class of worker"'
label var occly       `"Occupation last year"'
label var occ50ly     `"Occupation last year, 1950 basis"'
label var indly       `"Industry last year"'
label var ind50ly     `"Industry last year, 1950 basis"'
label var classwly    `"Class of worker last year"'
label var wkswork1    `"Weeks worked last year"'
label var wkswork2    `"Weeks worked last year, intervalled"'
label var hrswork     `"Hours worked last week"'
label var uhrswork    `"Usual hours worked per week (last yr)"'
label var wksunem1    `"Weeks unemployed last year"'
label var wksunem2    `"Weeks unemployed last year, intervalled"'
label var absent      `"Absent from work last week"'
label var durunem2    `"Continuous weeks unemployed, intervalled"'
label var durunemp    `"Continuous weeks unemployed"'
label var fullpart    `"Worked full or part time last year"'
label var nwlookwk    `"Weeks looked for work last year (didn't work)"'
label var hourwage    `"Hourly wage"'
label var paidhour    `"Paid by the hour"'
label var pension     `"Pension plan at work"'
label var union       `"Union membership"'
label var whyunemp    `"Reason for unemployment"'
label var firmsize    `"Number of employees"'
label var whyabsnt    `"Reason for absence from work"'
label var wantjob     `"Want regular job now"'
label var whyptly     `"Reason for working part-time last year"'
label var whyptlwk    `"Reason for working part time last week"'
label var usftptlw    `"Usually work full time (part time last week)"'
label var payifabs    `"Paid if absent from work last week"'
label var numemps     `"Number of employers last year"'
label var wnftlook    `"When last worked full time 2 consecutive weeks (looking last week)"'
label var wnlwnilf    `"When last worked for pay (NILF last week)"'
label var strechlk    `"Stretches of looking for work last year"'
label var whynwly     `"Reason not working last year"'
label var actnlfly    `"Activity when not in labor force last year (part-year workers)"'
label var ptweeks     `"Weeks working part time last year"'
label var ftotval     `"Total family income"'
label var inctot      `"Total personal income"'
label var incwage     `"Wage and salary income"'
label var incbus      `"Non-farm business income"'
label var incfarm     `"Farm income"'
label var incss       `"Social Security income"'
label var incwelfr    `"Welfare (public assistance) income"'
label var incretir    `"Retirement income"'
label var incssi      `"Income from SSI"'
label var incint      `"Income from interest"'
label var incunemp    `"Income from unemployment benefits"'
label var incwkcom    `"Income from worker's compensation"'
label var incvet      `"Income from veteran's benefits"'
label var incsurv     `"Income from survivor's benefits"'
label var incdisab    `"Income from disability benefits"'
label var incdivid    `"Income from dividends"'
label var incrent     `"Income from rent"'
label var inceduc     `"Income from educational assistance"'
label var incchild    `"Income from child support"'
label var incalim     `"Income from alimony"'
label var incasist    `"Income from assistance"'
label var incother    `"Income from other Source not specified"'
label var earnweek    `"Weekly earnings"'
label var gotvdisa    `"Received veterans' disability compensation"'
label var gotveduc    `"Received veterans' education assistance"'
label var gotvothe    `"Received other veterans' payments"'
label var gotvpens    `"Received veterans' pension"'
label var gotvsurv    `"Received veterans' survivor benefits"'
label var incdisa1    `"Disability income from first source"'
label var incdisa2    `"Disability income from second source"'
label var inclongj    `"Earnings from longest job"'
label var increti1    `"Retirement income from first source"'
label var increti2    `"Retirement income from second source"'
label var incsurv1    `"Survivor benefits income from first source"'
label var incsurv2    `"Survivor benefits income from second source"'
label var mthwelfr    `"Number of months received welfare income"'
label var oincbus     `"Earnings from other work included business self-employment earnings"'
label var oincfarm    `"Earnings from other work included farm self-employment earnings"'
label var oincwage    `"Earnings from other work included wage and salary earnings"'
label var srcdisa1    `"First source of disability income"'
label var srcdisa2    `"Second source of disability income"'
label var srcearn     `"Source of earnings from longest job"'
label var srceduc     `"Source of educational assistance"'
label var srcreti1    `"First source of retirement income"'
label var srcreti2    `"Second source of retirement income"'
label var srcsurv1    `"First source of survivor benefits"'
label var srcsurv2    `"Second source of survivor benefits"'
label var srcunemp    `"Source of unemployment income"'
label var srcwelfr    `"Source of welfare income"'
label var srcwkcom    `"Source of workmen's compensation"'
label var ssikid      `"Child under 18 received SSI income"'
label var sskid       `"Child under 19 received SS income"'
label var vetqa       `"Required to fill out annual income questionnaire for veterans' administration"'
label var whyss1      `"First reason for receiving social security income"'
label var whyss2      `"Second reason for receiving social security income"'
label var whyssi1     `"First reason for receiving supplementary security income"'
label var whyssi2     `"Second reason for receiving supplementary security income"'
label var stimulus    `"Federal stimulus payment"'
label var offpov      `"Official Poverty Status (IPUMS constructed)"'
label var offpovuniv  `"Official Poverty Rate Universe"'
label var offtotval   `"Total Family Income for Replicating Official Poverty Rates"'
label var offcutoff   `"Official Poverty Rate Cutoff"'
label var offreason   `"Reason for Discrepancy between OFFPOV and POVERTY"'
label var poverty     `"Original poverty status (PUMS original)"'
label var cutoff      `"Cutoff for original poverty status, in dollars"'
label var adjginc     `"Adjusted gross income"'
label var capgain     `"Amount of capital gain"'
label var caploss     `"Amount of capital loss"'
label var eitcred     `"Earned income tax credit"'
label var fedretir    `"Federal retirement payroll deduction"'
label var fedtax      `"Federal income tax liability, before credits"'
label var fedtaxac    `"Federal income tax liability, after all credits"'
label var fica        `"Social security retirement payroll deduction"'
label var filestat    `"Tax filer status"'
label var depstat     `"Dependency status pointer"'
label var margtax     `"Federal income marginal tax rate"'
label var statetax    `"State income tax liability, before credits"'
label var stataxac    `"State income tax liability, after all credits"'
label var taxinc      `"Taxable income amount"'
label var vetstat     `"Veteran status"'
label var vetlast     `"Veteran's most recent period of service"'
label var vet1        `"Veteran's most recent period of service, recode"'
label var vet2        `"Veteran's second most recent period of service"'
label var vet3        `"Veteran's third most recent period of service"'
label var vet4        `"Veteran's fourth most recent period of service"'
label var disabwrk    `"Work disability"'
label var health      `"Health status"'
label var quitsick    `"Quit job or retired for health reasons"'
label var diffhear    `"Hearing difficulty"'
label var diffeye     `"Vision difficulty"'
label var diffrem     `"Difficulty remembering"'
label var diffphys    `"Physical difficulty"'
label var diffmob     `"Disability limiting mobility"'
label var diffcare    `"Personal care limitation"'
label var diffany     `"Any difficulty"'
label var migsta1     `"State of residence 1 year ago"'
label var migsta5     `"State of residence 5 years ago"'
label var whymove     `"Reason for moving"'
label var country     `"Country of previous residence"'
label var migrate1    `"Migration status, 1 year"'
label var migrate5    `"Migration status, 5 years"'
label var katevac     `"Evacuate due to Katrina, household"'
label var katevac2    `"Evacuate due to Katrina, individual"'
label var katprior    `"State of residence prior to Katrina"'
label var hcovany     `"Any insurance, public or private (summary)"'
label var hcovpriv    `"Any private insurance (summary)"'
label var hinsemp     `"Employer-sponsored insurance (summary)"'
label var hinspur     `"Individually purchased insurance (summary)"'
label var hcovpub     `"Any public insurance (summary)"'
label var hinscaid    `"Any Medicaid/SCHIP/other public insurance (summary)"'
label var hinscare    `"Medicare coverage (summary)"'
label var hinsmil     `"Any military insurance (summary)"'
label var inclugh     `"Included in employer group healthplan  last year"'
label var paidgh      `"Employer paid for group health plan"'
label var emcontrb    `"Employer contribution for health insurance"'
label var himcaid     `"Covered by Medicaid last year"'
label var himcare     `"Covered by Medicare last year"'
label var hichamp     `"Covered by military health insurance last year"'
label var phinsur     `"Reported covered by private health insurance last year"'
label var phiown      `"Private health insurance in own name last year"'
label var caid        `"Covered by Medicaid last year"'
label var care        `"Covered by Medicare last year"'
label var pmvcaid     `"Person market value of Medicaid"'
label var pmvcare     `"Person market value of Medicare"'
label var champus     `"Covered by Champus/Tricare last year"'
label var champva     `"Covered by CHAMPVA last year"'
label var militva     `"Covered by VA or Military health care last year"'
label var indianh     `"Covered by Indian Health Service last year"'
label var groupown    `"Policyholder for employment-based insurance last year"'
label var groupdep    `"Dependent covered by employment-based insurance last year"'
label var privown     `"Policyholder for privately-purchased insurance last year"'
label var privdep     `"Dependent covered by privately-purchased insurance last year"'
label var grouptyp    `"Type of employment-based coverage last year"'
label var privtyp     `"Type of privately-purchased coverage last year"'
label var grpwho1     `"Line number of first policyholder of employment-based insurance"'
label var grpwho2     `"Line number of second policyholder of employment-based insurance"'
label var privwho1    `"Line number of first policyholder of privately-purchased insurance"'
label var privwho2    `"Line number of second policyholder of privately-purchased insurance"'
label var mocaid      `"Months of Medicaid coverage last year"'
label var schip       `"State Children's Health Insurance Program coverage last year"'
label var verify      `"Verification: Did individual actually have health insurance"'
label var groupout    `"Employment-based insurance covered non-household member"'
label var privout     `"Privately-purchased insurance covered non-household member"'
label var out         `"Covered by policy of person outside the household"'
label var covergh     `"Covered by group health insurance, last year"'
label var coverpi     `"Covered by private health insurance, last year"'
label var kidpriv     `"Child covered by private health insurance, last year"'
label var kidcaid     `"Child covered by Medicaid, last year"'
label var gedtrain    `"Attended GED classes"'
label var gotwic      `"Received WIC"'
label var jobclub     `"Attended job search program or job club"'
label var jobready    `"Attended job readiness training"'
label var jobtrain    `"Attended job training program"'
label var kidcare     `"Received child care assistance"'
label var kidcneed    `"Child needed care while parent worked"'
label var transaid    `"Received transportation assistance"'
label var workfare    `"In work program for cash assistance"'
label var ftype       `"Family Type"'
label var wkstat      `"Full or part time status"'
label var csout       `"Child has parent that lives outside the household"'
label var cswhy       `"Reason child does not have a biological/adoptive parent who lives outside the ho"'
label var cslegal     `"Ever had a legal child support agreement"'
label var csagree     `"Ever had a child support agreement other than legal"'
label var cstype      `"Type of agreement recode"'
label var cspay       `"Legal agreement included payments"'
label var csyear      `"Year first had agreement or made agreement legal"'
label var csyearpay   `"Year other parent was supposed to begin payments"'
label var cssupp      `"Other parent was supposed to pay child support last year"'
label var csreas      `"Reason other parent was not supposed to send payments last year"'
label var csdduct     `"Any payments were supposed to be deducted from other parent's paycheck"'
label var cswaypay    `"Method of child support payment delivery"'
label var cschg       `"Legal payment amount changed"'
label var cschgyr     `"Year payment amount changed"'
label var csfreq1     `"Frequency of payments - no change or before change"'
label var csback1     `"Child support included back payments - no change or before change"'
label var csfreq2     `"Frequency of payments - after change"'
label var csback2     `"Child support included back payments - after change"'
label var csrecv      `"Actually received any child support last year"'
label var csall       `"Actually received all support payments supposed to receive"'
label var cstime      `"How many payments were received on time"'
label var csfull      `"How many payments received were the full amount"'
label var csamdue     `"Amount of child support due"'
label var csamrec     `"Amount of child support actually received"'
label var cscontact   `"Had any contact with other parent last year"'
label var csvis       `"Other parent has visitation privileges"'
label var csvisleg    `"Visitation rights of other parent are legal"'
label var csphycus    `"Has physical custody agreement with other parent"'
label var cslegcus    `"Has legal custody agreement with other parent"'
label var csdays      `"Number of days other parent spent with child last year"'
label var csyrdiv     `"Year of most recent divorce or separation"'
label var csyrmar     `"Year of marriage from most recent divorce or separation"'
label var cswork      `"Working at the time of most recent divorce or separation"'
label var cswork5     `"Worked at some point 5 years prior to most recent divorce or separation"'
label var cselig      `"Eligibility to answer Child Support Supplement"'
label var cssuppwt    `"Child Support supplement weight"'
label var famkind     `"Kind of family"'
label var eligorg     `"(Earnings) eligibility flag"'
label var repwtp1     `"Person replicate weight 1"'
label var repwtp2     `"Person replicate weight 2"'
label var repwtp3     `"Person replicate weight 3"'
label var repwtp4     `"Person replicate weight 4"'
label var repwtp5     `"Person replicate weight 5"'
label var repwtp6     `"Person replicate weight 6"'
label var repwtp7     `"Person replicate weight 7"'
label var repwtp8     `"Person replicate weight 8"'
label var repwtp9     `"Person replicate weight 9"'
label var repwtp10    `"Person replicate weight 10"'
label var repwtp11    `"Person replicate weight 11"'
label var repwtp12    `"Person replicate weight 12"'
label var repwtp13    `"Person replicate weight 13"'
label var repwtp14    `"Person replicate weight 14"'
label var repwtp15    `"Person replicate weight 15"'
label var repwtp16    `"Person replicate weight 16"'
label var repwtp17    `"Person replicate weight 17"'
label var repwtp18    `"Person replicate weight 18"'
label var repwtp19    `"Person replicate weight 19"'
label var repwtp20    `"Person replicate weight 20"'
label var repwtp21    `"Person replicate weight 21"'
label var repwtp22    `"Person replicate weight 22"'
label var repwtp23    `"Person replicate weight 23"'
label var repwtp24    `"Person replicate weight 24"'
label var repwtp25    `"Person replicate weight 25"'
label var repwtp26    `"Person replicate weight 26"'
label var repwtp27    `"Person replicate weight 27"'
label var repwtp28    `"Person replicate weight 28"'
label var repwtp29    `"Person replicate weight 29"'
label var repwtp30    `"Person replicate weight 30"'
label var repwtp31    `"Person replicate weight 31"'
label var repwtp32    `"Person replicate weight 32"'
label var repwtp33    `"Person replicate weight 33"'
label var repwtp34    `"Person replicate weight 34"'
label var repwtp35    `"Person replicate weight 35"'
label var repwtp36    `"Person replicate weight 36"'
label var repwtp37    `"Person replicate weight 37"'
label var repwtp38    `"Person replicate weight 38"'
label var repwtp39    `"Person replicate weight 39"'
label var repwtp40    `"Person replicate weight 40"'
label var repwtp41    `"Person replicate weight 41"'
label var repwtp42    `"Person replicate weight 42"'
label var repwtp43    `"Person replicate weight 43"'
label var repwtp44    `"Person replicate weight 44"'
label var repwtp45    `"Person replicate weight 45"'
label var repwtp46    `"Person replicate weight 46"'
label var repwtp47    `"Person replicate weight 47"'
label var repwtp48    `"Person replicate weight 48"'
label var repwtp49    `"Person replicate weight 49"'
label var repwtp50    `"Person replicate weight 50"'
label var repwtp51    `"Person replicate weight 51"'
label var repwtp52    `"Person replicate weight 52"'
label var repwtp53    `"Person replicate weight 53"'
label var repwtp54    `"Person replicate weight 54"'
label var repwtp55    `"Person replicate weight 55"'
label var repwtp56    `"Person replicate weight 56"'
label var repwtp57    `"Person replicate weight 57"'
label var repwtp58    `"Person replicate weight 58"'
label var repwtp59    `"Person replicate weight 59"'
label var repwtp60    `"Person replicate weight 60"'
label var repwtp61    `"Person replicate weight 61"'
label var repwtp62    `"Person replicate weight 62"'
label var repwtp63    `"Person replicate weight 63"'
label var repwtp64    `"Person replicate weight 64"'
label var repwtp65    `"Person replicate weight 65"'
label var repwtp66    `"Person replicate weight 66"'
label var repwtp67    `"Person replicate weight 67"'
label var repwtp68    `"Person replicate weight 68"'
label var repwtp69    `"Person replicate weight 69"'
label var repwtp70    `"Person replicate weight 70"'
label var repwtp71    `"Person replicate weight 71"'
label var repwtp72    `"Person replicate weight 72"'
label var repwtp73    `"Person replicate weight 73"'
label var repwtp74    `"Person replicate weight 74"'
label var repwtp75    `"Person replicate weight 75"'
label var repwtp76    `"Person replicate weight 76"'
label var repwtp77    `"Person replicate weight 77"'
label var repwtp78    `"Person replicate weight 78"'
label var repwtp79    `"Person replicate weight 79"'
label var repwtp80    `"Person replicate weight 80"'
label var repwtp81    `"Person replicate weight 81"'
label var repwtp82    `"Person replicate weight 82"'
label var repwtp83    `"Person replicate weight 83"'
label var repwtp84    `"Person replicate weight 84"'
label var repwtp85    `"Person replicate weight 85"'
label var repwtp86    `"Person replicate weight 86"'
label var repwtp87    `"Person replicate weight 87"'
label var repwtp88    `"Person replicate weight 88"'
label var repwtp89    `"Person replicate weight 89"'
label var repwtp90    `"Person replicate weight 90"'
label var repwtp91    `"Person replicate weight 91"'
label var repwtp92    `"Person replicate weight 92"'
label var repwtp93    `"Person replicate weight 93"'
label var repwtp94    `"Person replicate weight 94"'
label var repwtp95    `"Person replicate weight 95"'
label var repwtp96    `"Person replicate weight 96"'
label var repwtp97    `"Person replicate weight 97"'
label var repwtp98    `"Person replicate weight 98"'
label var repwtp99    `"Person replicate weight 99"'
label var repwtp100   `"Person replicate weight 100"'
label var repwtp101   `"Person replicate weight 101"'
label var repwtp102   `"Person replicate weight 102"'
label var repwtp103   `"Person replicate weight 103"'
label var repwtp104   `"Person replicate weight 104"'
label var repwtp105   `"Person replicate weight 105"'
label var repwtp106   `"Person replicate weight 106"'
label var repwtp107   `"Person replicate weight 107"'
label var repwtp108   `"Person replicate weight 108"'
label var repwtp109   `"Person replicate weight 109"'
label var repwtp110   `"Person replicate weight 110"'
label var repwtp111   `"Person replicate weight 111"'
label var repwtp112   `"Person replicate weight 112"'
label var repwtp113   `"Person replicate weight 113"'
label var repwtp114   `"Person replicate weight 114"'
label var repwtp115   `"Person replicate weight 115"'
label var repwtp116   `"Person replicate weight 116"'
label var repwtp117   `"Person replicate weight 117"'
label var repwtp118   `"Person replicate weight 118"'
label var repwtp119   `"Person replicate weight 119"'
label var repwtp120   `"Person replicate weight 120"'
label var repwtp121   `"Person replicate weight 121"'
label var repwtp122   `"Person replicate weight 122"'
label var repwtp123   `"Person replicate weight 123"'
label var repwtp124   `"Person replicate weight 124"'
label var repwtp125   `"Person replicate weight 125"'
label var repwtp126   `"Person replicate weight 126"'
label var repwtp127   `"Person replicate weight 127"'
label var repwtp128   `"Person replicate weight 128"'
label var repwtp129   `"Person replicate weight 129"'
label var repwtp130   `"Person replicate weight 130"'
label var repwtp131   `"Person replicate weight 131"'
label var repwtp132   `"Person replicate weight 132"'
label var repwtp133   `"Person replicate weight 133"'
label var repwtp134   `"Person replicate weight 134"'
label var repwtp135   `"Person replicate weight 135"'
label var repwtp136   `"Person replicate weight 136"'
label var repwtp137   `"Person replicate weight 137"'
label var repwtp138   `"Person replicate weight 138"'
label var repwtp139   `"Person replicate weight 139"'
label var repwtp140   `"Person replicate weight 140"'
label var repwtp141   `"Person replicate weight 141"'
label var repwtp142   `"Person replicate weight 142"'
label var repwtp143   `"Person replicate weight 143"'
label var repwtp144   `"Person replicate weight 144"'
label var repwtp145   `"Person replicate weight 145"'
label var repwtp146   `"Person replicate weight 146"'
label var repwtp147   `"Person replicate weight 147"'
label var repwtp148   `"Person replicate weight 148"'
label var repwtp149   `"Person replicate weight 149"'
label var repwtp150   `"Person replicate weight 150"'
label var repwtp151   `"Person replicate weight 151"'
label var repwtp152   `"Person replicate weight 152"'
label var repwtp153   `"Person replicate weight 153"'
label var repwtp154   `"Person replicate weight 154"'
label var repwtp155   `"Person replicate weight 155"'
label var repwtp156   `"Person replicate weight 156"'
label var repwtp157   `"Person replicate weight 157"'
label var repwtp158   `"Person replicate weight 158"'
label var repwtp159   `"Person replicate weight 159"'
label var repwtp160   `"Person replicate weight 160"'

label define mish_lbl 1 `"One"'
label define mish_lbl 2 `"Two"', add
label define mish_lbl 3 `"Three"', add
label define mish_lbl 4 `"Four"', add
label define mish_lbl 5 `"Five"', add
label define mish_lbl 6 `"Six"', add
label define mish_lbl 7 `"Seven"', add
label define mish_lbl 8 `"Eight"', add
label values mish mish_lbl

label define huhhnum_lbl 00 `"00"'
label define huhhnum_lbl 01 `"01"', add
label define huhhnum_lbl 02 `"02"', add
label define huhhnum_lbl 03 `"03"', add
label define huhhnum_lbl 04 `"04"', add
label define huhhnum_lbl 05 `"05"', add
label define huhhnum_lbl 06 `"06"', add
label define huhhnum_lbl 07 `"07"', add
label define huhhnum_lbl 08 `"08"', add
label values huhhnum huhhnum_lbl

label define gq_lbl 0 `"NIU (Vacant units)"'
label define gq_lbl 1 `"Households"', add
label define gq_lbl 2 `"Group Quarters"', add
label values gq gq_lbl

label define hhintype_lbl 1 `"Interview"'
label define hhintype_lbl 2 `"Type A non-interview"', add
label define hhintype_lbl 3 `"Type B/C non-interview"', add
label values hhintype hhintype_lbl

label define repwt_lbl 1 `"Repwtp available"'
label values repwt repwt_lbl

label define region_lbl 11 `"New England Division"'
label define region_lbl 12 `"Middle Atlantic Division"', add
label define region_lbl 21 `"East North Central Division"', add
label define region_lbl 22 `"West North Central Division"', add
label define region_lbl 31 `"South Atlantic Division"', add
label define region_lbl 32 `"East South Central Division"', add
label define region_lbl 33 `"West South Central Division"', add
label define region_lbl 41 `"Mountain Division"', add
label define region_lbl 42 `"Pacific Division"', add
label define region_lbl 97 `"State not identified"', add
label values region region_lbl

label define statefip_lbl 01 `"Alabama"'
label define statefip_lbl 02 `"Alaska"', add
label define statefip_lbl 04 `"Arizona"', add
label define statefip_lbl 05 `"Arkansas"', add
label define statefip_lbl 06 `"California"', add
label define statefip_lbl 08 `"Colorado"', add
label define statefip_lbl 09 `"Connecticut"', add
label define statefip_lbl 10 `"Delaware"', add
label define statefip_lbl 11 `"District of Columbia"', add
label define statefip_lbl 12 `"Florida"', add
label define statefip_lbl 13 `"Georgia"', add
label define statefip_lbl 15 `"Hawaii"', add
label define statefip_lbl 16 `"Idaho"', add
label define statefip_lbl 17 `"Illinois"', add
label define statefip_lbl 18 `"Indiana"', add
label define statefip_lbl 19 `"Iowa"', add
label define statefip_lbl 20 `"Kansas"', add
label define statefip_lbl 21 `"Kentucky"', add
label define statefip_lbl 22 `"Louisiana"', add
label define statefip_lbl 23 `"Maine"', add
label define statefip_lbl 24 `"Maryland"', add
label define statefip_lbl 25 `"Massachusetts"', add
label define statefip_lbl 26 `"Michigan"', add
label define statefip_lbl 27 `"Minnesota"', add
label define statefip_lbl 28 `"Mississippi"', add
label define statefip_lbl 29 `"Missouri"', add
label define statefip_lbl 30 `"Montana"', add
label define statefip_lbl 31 `"Nebraska"', add
label define statefip_lbl 32 `"Nevada"', add
label define statefip_lbl 33 `"New Hampshire"', add
label define statefip_lbl 34 `"New Jersey"', add
label define statefip_lbl 35 `"New Mexico"', add
label define statefip_lbl 36 `"New York"', add
label define statefip_lbl 37 `"North Carolina"', add
label define statefip_lbl 38 `"North Dakota"', add
label define statefip_lbl 39 `"Ohio"', add
label define statefip_lbl 40 `"Oklahoma"', add
label define statefip_lbl 41 `"Oregon"', add
label define statefip_lbl 42 `"Pennsylvania"', add
label define statefip_lbl 44 `"Rhode Island"', add
label define statefip_lbl 45 `"South Carolina"', add
label define statefip_lbl 46 `"South Dakota"', add
label define statefip_lbl 47 `"Tennessee"', add
label define statefip_lbl 48 `"Texas"', add
label define statefip_lbl 49 `"Utah"', add
label define statefip_lbl 50 `"Vermont"', add
label define statefip_lbl 51 `"Virginia"', add
label define statefip_lbl 53 `"Washington"', add
label define statefip_lbl 54 `"West Virginia"', add
label define statefip_lbl 55 `"Wisconsin"', add
label define statefip_lbl 56 `"Wyoming"', add
label define statefip_lbl 61 `"Maine-New Hampshire-Vermont"', add
label define statefip_lbl 65 `"Montana-Idaho-Wyoming"', add
label define statefip_lbl 68 `"Alaska-Hawaii"', add
label define statefip_lbl 69 `"Nebraska-North Dakota-South Dakota"', add
label define statefip_lbl 70 `"Maine-Massachusetts-New Hampshire-Rhode Island-Vermont"', add
label define statefip_lbl 71 `"Michigan-Wisconsin"', add
label define statefip_lbl 72 `"Minnesota-Iowa"', add
label define statefip_lbl 73 `"Nebraska-North Dakota-South Dakota-Kansas"', add
label define statefip_lbl 74 `"Delaware-Virginia"', add
label define statefip_lbl 75 `"North Carolina-South Carolina"', add
label define statefip_lbl 76 `"Alabama-Mississippi"', add
label define statefip_lbl 77 `"Arkansas-Oklahoma"', add
label define statefip_lbl 78 `"Arizona-New Mexico-Colorado"', add
label define statefip_lbl 79 `"Idaho-Wyoming-Utah-Montana-Nevada"', add
label define statefip_lbl 80 `"Alaska-Washington-Hawaii"', add
label define statefip_lbl 81 `"New Hampshire-Maine-Vermont-Rhode Island"', add
label define statefip_lbl 83 `"South Carolina-Georgia"', add
label define statefip_lbl 84 `"Kentucky-Tennessee"', add
label define statefip_lbl 85 `"Arkansas-Louisiana-Oklahoma"', add
label define statefip_lbl 87 `"Iowa-N Dakota-S Dakota-Nebraska-Kansas-Minnesota-Missouri"', add
label define statefip_lbl 88 `"Washington-Oregon-Alaska-Hawaii"', add
label define statefip_lbl 89 `"Montana-Wyoming-Colorado-New Mexico-Utah-Nevada-Arizona"', add
label define statefip_lbl 90 `"Delaware-Maryland-Virginia-West Virginia"', add
label define statefip_lbl 99 `"State not identified"', add
label values statefip statefip_lbl

label define statecensus_lbl 00 `"Unknown"'
label define statecensus_lbl 11 `"Maine"', add
label define statecensus_lbl 12 `"New Hampshire"', add
label define statecensus_lbl 13 `"Vermont"', add
label define statecensus_lbl 14 `"Massachusetts"', add
label define statecensus_lbl 15 `"Rhode Island"', add
label define statecensus_lbl 16 `"Connecticut"', add
label define statecensus_lbl 21 `"New York"', add
label define statecensus_lbl 22 `"New Jersey"', add
label define statecensus_lbl 23 `"Pennsylvania"', add
label define statecensus_lbl 31 `"Ohio"', add
label define statecensus_lbl 32 `"Indiana"', add
label define statecensus_lbl 33 `"Illinois"', add
label define statecensus_lbl 34 `"Michigan"', add
label define statecensus_lbl 35 `"Wisconsin"', add
label define statecensus_lbl 41 `"Minnesota"', add
label define statecensus_lbl 42 `"Iowa"', add
label define statecensus_lbl 43 `"Missouri"', add
label define statecensus_lbl 44 `"North Dakota"', add
label define statecensus_lbl 45 `"South Dakota"', add
label define statecensus_lbl 46 `"Nebraska"', add
label define statecensus_lbl 47 `"Kansas"', add
label define statecensus_lbl 51 `"Delaware"', add
label define statecensus_lbl 52 `"Maryland"', add
label define statecensus_lbl 53 `"District of Columbia"', add
label define statecensus_lbl 54 `"Virginia"', add
label define statecensus_lbl 55 `"West Virginia"', add
label define statecensus_lbl 56 `"North Carolina"', add
label define statecensus_lbl 57 `"South Carolina"', add
label define statecensus_lbl 58 `"Georgia"', add
label define statecensus_lbl 59 `"Florida"', add
label define statecensus_lbl 61 `"Kentucky"', add
label define statecensus_lbl 62 `"Tennessee"', add
label define statecensus_lbl 63 `"Alabama"', add
label define statecensus_lbl 64 `"Mississippi"', add
label define statecensus_lbl 71 `"Arkansas"', add
label define statecensus_lbl 72 `"Louisiana"', add
label define statecensus_lbl 73 `"Oklahoma"', add
label define statecensus_lbl 74 `"Texas"', add
label define statecensus_lbl 81 `"Montana"', add
label define statecensus_lbl 82 `"Idaho"', add
label define statecensus_lbl 83 `"Wyoming"', add
label define statecensus_lbl 84 `"Colorado"', add
label define statecensus_lbl 85 `"New Mexico"', add
label define statecensus_lbl 86 `"Arizona"', add
label define statecensus_lbl 87 `"Utah"', add
label define statecensus_lbl 88 `"Nevada"', add
label define statecensus_lbl 91 `"Washington"', add
label define statecensus_lbl 92 `"Oregon"', add
label define statecensus_lbl 93 `"California"', add
label define statecensus_lbl 94 `"Alaska"', add
label define statecensus_lbl 95 `"Hawaii"', add
label values statecensus statecensus_lbl

label define asecflag_lbl 1 `"ASEC"'
label define asecflag_lbl 2 `"March Basic"', add
label values asecflag asecflag_lbl

label define metro_lbl 0 `"Not identifiable"'
label define metro_lbl 1 `"Not in metro area"', add
label define metro_lbl 2 `"Central city"', add
label define metro_lbl 3 `"Outside central city"', add
label define metro_lbl 4 `"Central city status unknown"', add
label define metro_lbl 9 `"Missing/Unknown"', add
label values metro metro_lbl

label define metarea_lbl 0080 `"Akron, OH"'
label define metarea_lbl 0120 `"Albany, GA"', add
label define metarea_lbl 0160 `"Albany-Schenectady-Troy, NY"', add
label define metarea_lbl 0200 `"Albuquerque, NM"', add
label define metarea_lbl 0240 `"Allentown-Bethlehem-Easton, PA/NJ"', add
label define metarea_lbl 0280 `"Altoona, PA MSA"', add
label define metarea_lbl 0320 `"Amarillo, TX"', add
label define metarea_lbl 0380 `"Anchorage, AK"', add
label define metarea_lbl 0400 `"Anderson, IN"', add
label define metarea_lbl 0440 `"Ann Arbor, MI"', add
label define metarea_lbl 0450 `"Anniston, AL"', add
label define metarea_lbl 0451 `"Anniston-Oxford, AL"', add
label define metarea_lbl 0460 `"Appleton,Oshkosh-Neenah, WI"', add
label define metarea_lbl 0461 `"Appleton, WI"', add
label define metarea_lbl 0462 `"Oshkosh-Neenah, WI"', add
label define metarea_lbl 0480 `"Asheville, NC"', add
label define metarea_lbl 0500 `"Athens, GA"', add
label define metarea_lbl 0501 `"Athens-Clark County, GA"', add
label define metarea_lbl 0520 `"Atlanta, GA"', add
label define metarea_lbl 0521 `"Atlanta-Sandy Springs-Marietta, GA"', add
label define metarea_lbl 0560 `"Atlantic City, NJ"', add
label define metarea_lbl 0600 `"Augusta-Aiken, GA-SC"', add
label define metarea_lbl 0601 `"Augusta-Richmond County, GA-SC"', add
label define metarea_lbl 0640 `"Austin, TX"', add
label define metarea_lbl 0641 `"Austin-Round Rock, TX"', add
label define metarea_lbl 0680 `"Bakersfield, CA"', add
label define metarea_lbl 0720 `"Baltimore, MD"', add
label define metarea_lbl 0721 `"Baltimore-Towson, MD"', add
label define metarea_lbl 0730 `"Bangor, ME"', add
label define metarea_lbl 0740 `"Barnstable-Yarmouth, MA"', add
label define metarea_lbl 0741 `"Barnstable Town, MA"', add
label define metarea_lbl 0760 `"Baton Rouge, LA"', add
label define metarea_lbl 0780 `"Battle Creek, MI"', add
label define metarea_lbl 0840 `"Beaumont-Port Arthur-Orange, TX"', add
label define metarea_lbl 0841 `"Beaumont-Port Arthur, TX"', add
label define metarea_lbl 0860 `"Bellingham, WA"', add
label define metarea_lbl 0870 `"Benton Harbor, MI"', add
label define metarea_lbl 0871 `"Niles-Benton Harbor, MI"', add
label define metarea_lbl 0880 `"Billings, MT"', add
label define metarea_lbl 0900 `"Bend, OR"', add
label define metarea_lbl 0920 `"Biloxi-Gulfport, MS"', add
label define metarea_lbl 0960 `"Binghamton, NY"', add
label define metarea_lbl 1000 `"Birmingham, AL"', add
label define metarea_lbl 1001 `"Birmingham-Hoover, AL"', add
label define metarea_lbl 1020 `"Bloomington, IN"', add
label define metarea_lbl 1040 `"Bloomington-Normal, IL"', add
label define metarea_lbl 1080 `"Boise City, ID"', add
label define metarea_lbl 1081 `"Boise City-Nampa, ID"', add
label define metarea_lbl 1120 `"Boston, MA"', add
label define metarea_lbl 1121 `"Lawrence-Haverhill. MA/NH"', add
label define metarea_lbl 1122 `"Lowell, MA/NH"', add
label define metarea_lbl 1123 `"Salem-Gloucester, MA"', add
label define metarea_lbl 1124 `"Boston-Cambridge-Quincy, MA-NH"', add
label define metarea_lbl 1130 `"Bowling Green, KY"', add
label define metarea_lbl 1140 `"Bradenton, FL"', add
label define metarea_lbl 1150 `"Bremerton-Silverdale, WA"', add
label define metarea_lbl 1160 `"Bridgeport, CT"', add
label define metarea_lbl 1161 `"Bridgeport-Stamford-Norwalk, CT"', add
label define metarea_lbl 1200 `"Brockton, MA"', add
label define metarea_lbl 1240 `"Brownsville-Harlingen-San Benito,TX"', add
label define metarea_lbl 1241 `"Brownsville-Harlingen, TX"', add
label define metarea_lbl 1280 `"Buffalo-Niagara Falls, NY"', add
label define metarea_lbl 1281 `"Niagara Falls, NY"', add
label define metarea_lbl 1310 `"Burlington, VT"', add
label define metarea_lbl 1311 `"Burlington-South Burlington, VT"', add
label define metarea_lbl 1320 `"Canton, OH"', add
label define metarea_lbl 1321 `"Canton-Massillon, OH"', add
label define metarea_lbl 1360 `"Cedar Rapids, IA"', add
label define metarea_lbl 1400 `"Champaign-Urbana-Rantoul, IL"', add
label define metarea_lbl 1401 `"Champaign-Urbana, IL"', add
label define metarea_lbl 1440 `"Charleston-North Charleston, SC"', add
label define metarea_lbl 1480 `"Charleston, WV"', add
label define metarea_lbl 1520 `"Charlotte-Gastonia-Rock Hill, NC/SC"', add
label define metarea_lbl 1521 `"Charlotte-Gastonia-Concord, NC/SC"', add
label define metarea_lbl 1560 `"Chattanooga, TN/GA"', add
label define metarea_lbl 1600 `"Chicago-Gary-Lake IL"', add
label define metarea_lbl 1601 `"Aurora-Elgin, IL"', add
label define metarea_lbl 1602 `"Gary-Hamond-East Chicago, IN"', add
label define metarea_lbl 1603 `"Joliet, IL"', add
label define metarea_lbl 1604 `"Lake County, IL"', add
label define metarea_lbl 1605 `"Chicago-Naperville-Joliet, IL-IN-WI"', add
label define metarea_lbl 1620 `"Chico,CA"', add
label define metarea_lbl 1640 `"Cincinnati-Hamilton,OH/KY/IN"', add
label define metarea_lbl 1641 `"Cincinnati-Middleton, OH/KY/IN"', add
label define metarea_lbl 1660 `"Clarksville-Hopkinsville,TN/KY"', add
label define metarea_lbl 1661 `"Clarksville, TN/KY, TN/KY"', add
label define metarea_lbl 1680 `"Cleveland, OH"', add
label define metarea_lbl 1681 `"Cleveland-Lorain-Mentor, OH"', add
label define metarea_lbl 1700 `"Coeur d'Alene, ID"', add
label define metarea_lbl 1720 `"Colorado Springs, CO"', add
label define metarea_lbl 1740 `"Columbia, MO"', add
label define metarea_lbl 1760 `"Columbia, SC"', add
label define metarea_lbl 1800 `"Columbus, GA/AL"', add
label define metarea_lbl 1840 `"Columbus, OH"', add
label define metarea_lbl 1880 `"Corpus Christi, TX"', add
label define metarea_lbl 1920 `"Dallas-Fort Worth, TX"', add
label define metarea_lbl 1921 `"Fort Worth-Arlington, TX"', add
label define metarea_lbl 1922 `"Dallas-Fort Worth-Arlington, TX"', add
label define metarea_lbl 1930 `"Danbury, CT"', add
label define metarea_lbl 1960 `"Davenport-Rock Island-Moline, IA/IL"', add
label define metarea_lbl 2000 `"Dayton-Springfield, OH"', add
label define metarea_lbl 2001 `"Springfield, OH"', add
label define metarea_lbl 2002 `"Dayton, OH"', add
label define metarea_lbl 2020 `"Daytona Beach, FL"', add
label define metarea_lbl 2021 `"Deltona-Daytona Beach-Ormond Beach, FL"', add
label define metarea_lbl 2030 `"Decatur, AL"', add
label define metarea_lbl 2040 `"Decatur, IL"', add
label define metarea_lbl 2080 `"Denver-Boulder-Longmont, CO"', add
label define metarea_lbl 2081 `"Boulder-Longmont, CO"', add
label define metarea_lbl 2082 `"Boulder, CO"', add
label define metarea_lbl 2083 `"Denver-Aurora, CO"', add
label define metarea_lbl 2120 `"Des Moines, IA"', add
label define metarea_lbl 2160 `"Detroit, MI"', add
label define metarea_lbl 2161 `"Detroit-Warren-Livonia, MI"', add
label define metarea_lbl 2190 `"Dover, DE"', add
label define metarea_lbl 2240 `"Duluth-Superior, MN/WI"', add
label define metarea_lbl 2241 `"Duluth, MN/WI"', add
label define metarea_lbl 2281 `"Dutchess County, NY"', add
label define metarea_lbl 2290 `"Eau Claire, WI"', add
label define metarea_lbl 2300 `"El Centro, CA"', add
label define metarea_lbl 2310 `"El Paso, TX"', add
label define metarea_lbl 2360 `"Erie, PA"', add
label define metarea_lbl 2400 `"Eugene-Springfield, OR"', add
label define metarea_lbl 2440 `"Evansville, IN/KY"', add
label define metarea_lbl 2520 `"Fargo-Moorhead, ND/MN"', add
label define metarea_lbl 2521 `"Fargo, ND/MN"', add
label define metarea_lbl 2540 `"Farmington, NM"', add
label define metarea_lbl 2560 `"Fayetteville, NC"', add
label define metarea_lbl 2580 `"Fayetteville-Springdale, AR"', add
label define metarea_lbl 2581 `"Fayetteville-Springdale-Rogers, AR-MO"', add
label define metarea_lbl 2600 `"Fitchburg-Leominster, MA"', add
label define metarea_lbl 2601 `"Leominster-Fitchburg-Gardner, MA"', add
label define metarea_lbl 2640 `"Flint, MI"', add
label define metarea_lbl 2650 `"Florence, AL"', add
label define metarea_lbl 2660 `"Florence, SC"', add
label define metarea_lbl 2670 `"Fort Collins-Loveland, CO"', add
label define metarea_lbl 2680 `"Fort Lauderdale-Hollywood-Pompano Beach, FL"', add
label define metarea_lbl 2700 `"Fort Myers-Cape Coral, FL"', add
label define metarea_lbl 2710 `"Fort Pierce, FL"', add
label define metarea_lbl 2711 `"Port St. Lucie-Fort Pierce, FL"', add
label define metarea_lbl 2720 `"Fort Smith, AR/OK"', add
label define metarea_lbl 2750 `"Fort Walton Beach, FL"', add
label define metarea_lbl 2751 `"Fort Walton Beach-Crestview-Destin, FL"', add
label define metarea_lbl 2760 `"Fort Wayne, IN"', add
label define metarea_lbl 2840 `"Fresno, CA"', add
label define metarea_lbl 2880 `"Gadsden, AL"', add
label define metarea_lbl 2900 `"Gainesville, FL"', add
label define metarea_lbl 2920 `"Galveston-Texas City, TX"', add
label define metarea_lbl 3000 `"Grand Rapids, MI"', add
label define metarea_lbl 3001 `"Grand Rapids-Wyoming, MI"', add
label define metarea_lbl 3002 `"Grand Rapids-Muskegon-Holland, MI MSA"', add
label define metarea_lbl 3003 `"Holland-Grand Haven, MI"', add
label define metarea_lbl 3060 `"Greeley, CO"', add
label define metarea_lbl 3080 `"Green Bay, WI"', add
label define metarea_lbl 3120 `"Greensboro-Winston Salem, NC"', add
label define metarea_lbl 3121 `"Winston-Salem, NC"', add
label define metarea_lbl 3122 `"Greensboro-High Point, NC"', add
label define metarea_lbl 3150 `"Greenville, NC"', add
label define metarea_lbl 3160 `"Greenville-Spartanburg-Anderson, SC"', add
label define metarea_lbl 3161 `"Anderson, SC"', add
label define metarea_lbl 3162 `"Greenville, SC"', add
label define metarea_lbl 3163 `"Spartanburg, SC"', add
label define metarea_lbl 3180 `"Hagerstown, MD"', add
label define metarea_lbl 3181 `"Hagerstown-Martinsburg, MD-WV"', add
label define metarea_lbl 3200 `"Hamilton-Middleton, OH"', add
label define metarea_lbl 3240 `"Harrisburg-Lebanon-Carlisle, PA"', add
label define metarea_lbl 3241 `"Harrisburg-Carlisle, PA"', add
label define metarea_lbl 3260 `"Harrisonburg, VA"', add
label define metarea_lbl 3280 `"Hartford-Bristol-Middleton- New Britain, CT"', add
label define metarea_lbl 3283 `"New Britain, CT"', add
label define metarea_lbl 3284 `"Hartford-West Hartford-East Hartford"', add
label define metarea_lbl 3285 `"Hartford, CT"', add
label define metarea_lbl 3290 `"Hickory-Morganton, NC"', add
label define metarea_lbl 3291 `"Hickory-Morganton-Lenoir, NC"', add
label define metarea_lbl 3320 `"Honolulu, HI"', add
label define metarea_lbl 3350 `"Houma-Thibodaux, LA"', add
label define metarea_lbl 3351 `"Houma-Bayou Cane-Thibodaux, LA"', add
label define metarea_lbl 3360 `"Houston-Brazoria,TX"', add
label define metarea_lbl 3361 `"Brazoria, TX"', add
label define metarea_lbl 3362 `"Houston-Baytown-Sugar Land, TX"', add
label define metarea_lbl 3400 `"Huntington-Ashland,WV/KY/OH"', add
label define metarea_lbl 3440 `"Huntsville,AL"', add
label define metarea_lbl 3480 `"Indianapolis, IN"', add
label define metarea_lbl 3500 `"Iowa City, IA"', add
label define metarea_lbl 3520 `"Jackson, MI"', add
label define metarea_lbl 3560 `"Jackson, MS"', add
label define metarea_lbl 3590 `"Jacksonville,FL"', add
label define metarea_lbl 3600 `"Jacksonville, NC"', add
label define metarea_lbl 3610 `"Jamestown-Dunkirk, NY"', add
label define metarea_lbl 3611 `"Jamestown, NY MSA"', add
label define metarea_lbl 3620 `"Janesville-Beloit, WI"', add
label define metarea_lbl 3621 `"Janvesville, WI"', add
label define metarea_lbl 3660 `"Johnson City-Kingsport-Bristol, TN/VA"', add
label define metarea_lbl 3661 `"Johnson City, TN"', add
label define metarea_lbl 3662 `"Kingsport-Bristol, TN-VA"', add
label define metarea_lbl 3680 `"Johnstown, PA"', add
label define metarea_lbl 3710 `"Joplin, MO"', add
label define metarea_lbl 3720 `"Kalamazoo-Portage, MI"', add
label define metarea_lbl 3721 `"Kalamazoo-Battle Creek, MI MSA"', add
label define metarea_lbl 3740 `"Kankakee, IL"', add
label define metarea_lbl 3741 `"Kankakee-Bradley, IL"', add
label define metarea_lbl 3760 `"Kansas City, MO/KS"', add
label define metarea_lbl 3810 `"Killeen-Temple,TX"', add
label define metarea_lbl 3811 `"Killeen-Temple-Fort Hood, TX"', add
label define metarea_lbl 3830 `"Kingston, NY"', add
label define metarea_lbl 3840 `"Knoxville, TN"', add
label define metarea_lbl 3870 `"LaCrosse, WI"', add
label define metarea_lbl 3880 `"Lafayette, LA"', add
label define metarea_lbl 3960 `"Lake Charles, LA"', add
label define metarea_lbl 3980 `"Lakeland-Winterhaven, FL"', add
label define metarea_lbl 4000 `"Lancaster, PA"', add
label define metarea_lbl 4040 `"Lansing-East Lansing, MI"', add
label define metarea_lbl 4080 `"Laredo, TX"', add
label define metarea_lbl 4100 `"Las Cruces, NM"', add
label define metarea_lbl 4120 `"Las Vegas, NV"', add
label define metarea_lbl 4130 `"Las Vegas-Paradise, NM"', add
label define metarea_lbl 4150 `"Lawrence, KS"', add
label define metarea_lbl 4200 `"Lawton, OK"', add
label define metarea_lbl 4280 `"Lexington-Fayette, KY"', add
label define metarea_lbl 4320 `"Lima, OH"', add
label define metarea_lbl 4360 `"Lincoln, NE"', add
label define metarea_lbl 4400 `"Little Rock-North Little Rock, AR"', add
label define metarea_lbl 4420 `"Longview-Marshall, TX"', add
label define metarea_lbl 4421 `"Longview, TX"', add
label define metarea_lbl 4440 `"Lorain-Elyria, OH"', add
label define metarea_lbl 4480 `"Los Angeles-Long Beach, CA"', add
label define metarea_lbl 4481 `"Anaheim-Santa Ana- Garden Grove, CA"', add
label define metarea_lbl 4482 `"Orange County, CA"', add
label define metarea_lbl 4483 `"Los Angeles-Long Beach-Santa Ana, CA"', add
label define metarea_lbl 4520 `"Louisville, KY/IN"', add
label define metarea_lbl 4600 `"Lubbock, TX"', add
label define metarea_lbl 4640 `"Lynchburg, VA"', add
label define metarea_lbl 4680 `"Macon-Warner Robins, GA"', add
label define metarea_lbl 4681 `"Macon, GA"', add
label define metarea_lbl 4682 `"Warner Robins, GA"', add
label define metarea_lbl 4700 `"Madera, CA"', add
label define metarea_lbl 4720 `"Madison, WI"', add
label define metarea_lbl 4760 `"Manchester, NH"', add
label define metarea_lbl 4800 `"Mansfield, OH"', add
label define metarea_lbl 4880 `"McAllen-Edinburg-Pharr-Mission, TX"', add
label define metarea_lbl 4881 `"McAllen-Edinburg-Pharr, TX"', add
label define metarea_lbl 4890 `"Medford, OR"', add
label define metarea_lbl 4900 `"Melbourne-Titusville-Cocoa-Palm Beach, FL"', add
label define metarea_lbl 4901 `"Palm Bay-Melbourne-Titusville, FL"', add
label define metarea_lbl 4920 `"Memphis, TN/AR/MS"', add
label define metarea_lbl 4940 `"Merced, CA"', add
label define metarea_lbl 5000 `"Miami-Hialeah, FL"', add
label define metarea_lbl 5001 `"Miami-Fort Lauderdale-Miami Beach, FL"', add
label define metarea_lbl 5020 `"Michigan City-La Porte, IN"', add
label define metarea_lbl 5080 `"Milwaukee, WI"', add
label define metarea_lbl 5081 `"Milwaukee-Waukesha-West Allis, WI"', add
label define metarea_lbl 5120 `"Minneapolis-St. Paul, MN"', add
label define metarea_lbl 5121 `"Minneapolis-St. Paul-Bloomington, MN/WI"', add
label define metarea_lbl 5160 `"Mobile, AL"', add
label define metarea_lbl 5170 `"Modesto, CA"', add
label define metarea_lbl 5190 `"Monmouth-Ocean, NJ"', add
label define metarea_lbl 5200 `"Monroe, LA"', add
label define metarea_lbl 5220 `"Monroe, MI"', add
label define metarea_lbl 5240 `"Montgomery, Al"', add
label define metarea_lbl 5320 `"Muskegon-Norton Shores-Muskegon Heights, MI"', add
label define metarea_lbl 5321 `"Muskegon-Norton Shores, MI"', add
label define metarea_lbl 5330 `"Myrtle Beach, SC"', add
label define metarea_lbl 5331 `"Myrtle Beach-Conway-North Myrtle Beach, SC"', add
label define metarea_lbl 5340 `"Naples, FL"', add
label define metarea_lbl 5341 `"Naples-Marco Island, FL"', add
label define metarea_lbl 5350 `"Nashua, NH"', add
label define metarea_lbl 5360 `"Nashville, TN"', add
label define metarea_lbl 5361 `"Nashville-Davidson-Murfreesboro, TN"', add
label define metarea_lbl 5400 `"New Bedford, MA"', add
label define metarea_lbl 5480 `"New Haven-Meriden, CT"', add
label define metarea_lbl 5481 `"New Haven, CT"', add
label define metarea_lbl 5520 `"New London-Norwich, CT/RI"', add
label define metarea_lbl 5560 `"New Orleans, LA"', add
label define metarea_lbl 5561 `"New Orleans-Metairie-Kenner, LA"', add
label define metarea_lbl 5600 `"New York-Northeastern NJ"', add
label define metarea_lbl 5601 `"Nassau-Suffolk, NY"', add
label define metarea_lbl 5602 `"Bergen-Passaic, NJ"', add
label define metarea_lbl 5603 `"Jersey City, NJ"', add
label define metarea_lbl 5604 `"Middlesex-Somerset-Hunterdon, NJ"', add
label define metarea_lbl 5605 `"Newark, NJ"', add
label define metarea_lbl 5606 `"New York-Northern New Jersey-Long Island, NY-NJ-PA"', add
label define metarea_lbl 5607 `"New York, NY"', add
label define metarea_lbl 5640 `"Newark, OH"', add
label define metarea_lbl 5660 `"Newburgh-Middletown, NY"', add
label define metarea_lbl 5720 `"Norfolk-Virginia Beach-Newport News, VA"', add
label define metarea_lbl 5721 `"Virginia Beach-Norfolk-Newport News, VA/NC"', add
label define metarea_lbl 5760 `"Norwalk, CT"', add
label define metarea_lbl 5790 `"Ocala, FL"', add
label define metarea_lbl 5800 `"Odessa, TX"', add
label define metarea_lbl 5801 `"Midland, TX"', add
label define metarea_lbl 5840 `"Ocean City, NJ"', add
label define metarea_lbl 5880 `"Oklahoma City, OK"', add
label define metarea_lbl 5910 `"Olympia, WA"', add
label define metarea_lbl 5920 `"Omaha, NE/IA"', add
label define metarea_lbl 5921 `"Omaha-Council Bluffs, NE/IA"', add
label define metarea_lbl 5950 `"Orange, NY"', add
label define metarea_lbl 5960 `"Orlando, FL"', add
label define metarea_lbl 6010 `"Panama City, FL"', add
label define metarea_lbl 6011 `"Panama City-Lynn Haven, FL"', add
label define metarea_lbl 6080 `"Pensacola, FL"', add
label define metarea_lbl 6081 `"Pensacola-Ferry Pass-Brent, FL"', add
label define metarea_lbl 6120 `"Peoria, IL"', add
label define metarea_lbl 6160 `"Philadelphia, PA/NJ"', add
label define metarea_lbl 6161 `"Philadelphia-Camden-Wilmington, PA/NJ/DE"', add
label define metarea_lbl 6200 `"Phoenix, AZ"', add
label define metarea_lbl 6201 `"Phoenix-Mesa-Scottsdale, AZ"', add
label define metarea_lbl 6280 `"Pittsburg, PA"', add
label define metarea_lbl 6281 `"Beaver County"', add
label define metarea_lbl 6400 `"Portland, ME"', add
label define metarea_lbl 6401 `"Portland-South Portland, ME"', add
label define metarea_lbl 6440 `"Portland-Vancouver, OR/WA"', add
label define metarea_lbl 6441 `"Vancouver, WA"', add
label define metarea_lbl 6442 `"Portland-Vancouver-Beaverton, OR/WA"', add
label define metarea_lbl 6450 `"Portsmouth-Dover-Rochester, NH/ME"', add
label define metarea_lbl 6451 `"Portsmouth-Rochester, NH/ME MSA"', add
label define metarea_lbl 6452 `"Rochester-Dover, NH/ME"', add
label define metarea_lbl 6460 `"Poughkeepsie, NY"', add
label define metarea_lbl 6461 `"Poughkeepsie-Newburgh-Middletown, NY"', add
label define metarea_lbl 6470 `"Prescott, AZ"', add
label define metarea_lbl 6480 `"Providence-Fall River-Pawtucket, MA/RI"', add
label define metarea_lbl 6482 `"Pawtuckett-Woonsocket-Attleboro, RI/MA"', add
label define metarea_lbl 6483 `"Providence-Fall River-Warwick, MA-RI"', add
label define metarea_lbl 6520 `"Provo-Orem, UT"', add
label define metarea_lbl 6560 `"Pueblo, CO"', add
label define metarea_lbl 6580 `"Punta Gorda, FL"', add
label define metarea_lbl 6600 `"Racine, WI"', add
label define metarea_lbl 6640 `"Raleigh-Durham, NC"', add
label define metarea_lbl 6641 `"Durham, NC"', add
label define metarea_lbl 6642 `"Raleigh-Carey, NC"', add
label define metarea_lbl 6680 `"Reading, PA"', add
label define metarea_lbl 6720 `"Reno, NV"', add
label define metarea_lbl 6721 `"Reno-Sparks, NV"', add
label define metarea_lbl 6760 `"Richmond-Petersburg, VA"', add
label define metarea_lbl 6761 `"Richmond, VA"', add
label define metarea_lbl 6780 `"Riverside-San Bernadino, CA"', add
label define metarea_lbl 6800 `"Roanoke, VA"', add
label define metarea_lbl 6840 `"Rochester, NY"', add
label define metarea_lbl 6880 `"Rockford, IL"', add
label define metarea_lbl 6920 `"Sacramento, CA"', add
label define metarea_lbl 6921 `"Sacramento-Arden Arcade-Roseville, CA"', add
label define metarea_lbl 6960 `"Saginaw-Bay City-Midland, MI"', add
label define metarea_lbl 6961 `"Saginaw-Saginaw Township North, MI"', add
label define metarea_lbl 6980 `"St. Cloud, MN"', add
label define metarea_lbl 7040 `"St. Louis, MO/IL"', add
label define metarea_lbl 7080 `"Salem, OR"', add
label define metarea_lbl 7120 `"Salinas-Sea Side-Monterey, CA"', add
label define metarea_lbl 7121 `"Salinas, CA"', add
label define metarea_lbl 7130 `"Salisbury, MD"', add
label define metarea_lbl 7160 `"Salt Lake City-Ogden, UT"', add
label define metarea_lbl 7161 `"Salt Lake City, UT"', add
label define metarea_lbl 7162 `"Ogden-Clearfield, UT"', add
label define metarea_lbl 7240 `"San Antonio, TX"', add
label define metarea_lbl 7320 `"San Diego, CA"', add
label define metarea_lbl 7321 `"San Diego-Carlsbad-San Marcos, CA"', add
label define metarea_lbl 7360 `"San Francisco-Oaklan-Vallejo, CA"', add
label define metarea_lbl 7361 `"Oakland, CA"', add
label define metarea_lbl 7362 `"Vallejo-Fairfield-Napa, CA"', add
label define metarea_lbl 7363 `"Vallejo-Fairfield, CA"', add
label define metarea_lbl 7364 `"Napa, CA"', add
label define metarea_lbl 7365 `"San Francisco-Oakland-Fremont, CA"', add
label define metarea_lbl 7400 `"San Jose, CA"', add
label define metarea_lbl 7401 `"San Jose-Sunnyvale-Santa Clara, CA"', add
label define metarea_lbl 7460 `"San Luis Obispo-Atascadero-Paso Robles, CA"', add
label define metarea_lbl 7461 `"San Luis Obispo-Paso Robles, CA"', add
label define metarea_lbl 7470 `"Santa Barbara-Santa Maria-Lompoc, CA"', add
label define metarea_lbl 7471 `"Santa Barbara-Santa Maria-Goleta, CA"', add
label define metarea_lbl 7480 `"Santa Cruz, CA"', add
label define metarea_lbl 7481 `"Santa Cruz-Watsonville, CA"', add
label define metarea_lbl 7490 `"Santa Fe, NM"', add
label define metarea_lbl 7500 `"Santa Rosa-Petaluma, CA"', add
label define metarea_lbl 7510 `"Sarasota, FL"', add
label define metarea_lbl 7511 `"Sarasota-Bradenton-Venice, FL"', add
label define metarea_lbl 7520 `"Savannah, GA"', add
label define metarea_lbl 7560 `"Scranton-Wilkes-Barre, PA"', add
label define metarea_lbl 7600 `"Seattle-Everett, WA"', add
label define metarea_lbl 7601 `"Seattle-Tacoma-Bellevue, WA"', add
label define metarea_lbl 7610 `"Sharon, PA"', add
label define metarea_lbl 7680 `"Shreveport, LA"', add
label define metarea_lbl 7681 `"Shreveport-Bossier City, LA"', add
label define metarea_lbl 7720 `"Sioux City, IA-NE"', add
label define metarea_lbl 7760 `"Sioux Falls, SD"', add
label define metarea_lbl 7800 `"South Bend-Mishawaka, IN"', add
label define metarea_lbl 7840 `"Spokane, WA"', add
label define metarea_lbl 7880 `"Springfield, IL"', add
label define metarea_lbl 7920 `"Springfield, MO"', add
label define metarea_lbl 8000 `"Springfield-Holyoke-Chicopee, MA"', add
label define metarea_lbl 8001 `"Springfield, MA/CT"', add
label define metarea_lbl 8040 `"Stamford, CT"', add
label define metarea_lbl 8120 `"Stockton, CA"', add
label define metarea_lbl 8160 `"Syracuse, NY"', add
label define metarea_lbl 8200 `"Tacoma, WA"', add
label define metarea_lbl 8240 `"Tallahassee, FL"', add
label define metarea_lbl 8280 `"Tampa-St. Petersburg-Clearwater, FL"', add
label define metarea_lbl 8320 `"Terre Haute, IN"', add
label define metarea_lbl 8400 `"Toledo, OH/MI"', add
label define metarea_lbl 8440 `"Topeka, KS"', add
label define metarea_lbl 8480 `"Trenton, NJ"', add
label define metarea_lbl 8481 `"Trenton-Ewing, NJ"', add
label define metarea_lbl 8520 `"Tucson, AZ"', add
label define metarea_lbl 8560 `"Tulsa, OK"', add
label define metarea_lbl 8600 `"Tuscaloosa, AL"', add
label define metarea_lbl 8680 `"Utica-Rome, NY"', add
label define metarea_lbl 8700 `"Valdosta, GA"', add
label define metarea_lbl 8730 `"Ventura-Oxnard-Simi Valley, CA"', add
label define metarea_lbl 8731 `"Oxnard-Thousand Oaks-Ventura, CA"', add
label define metarea_lbl 8740 `"Vero Beach, FL"', add
label define metarea_lbl 8750 `"Victoria, TX"', add
label define metarea_lbl 8760 `"Vineland-Milville-Bridgetown, NJ"', add
label define metarea_lbl 8780 `"Visalia-Tulare-Porterville, CA"', add
label define metarea_lbl 8781 `"Visalia-Porterville, CA"', add
label define metarea_lbl 8800 `"Waco, TX"', add
label define metarea_lbl 8840 `"Washington, DC/MD/VA"', add
label define metarea_lbl 8880 `"Waterbury, CT"', add
label define metarea_lbl 8920 `"Waterloo-Cedar Falls, IA"', add
label define metarea_lbl 8940 `"Wausau, WI"', add
label define metarea_lbl 8960 `"West Palm Beach-Boca Raton-Delray Beach, FL"', add
label define metarea_lbl 9000 `"Wheeling, WV/OH"', add
label define metarea_lbl 9040 `"Wichita, KS"', add
label define metarea_lbl 9140 `"Williamsport, PA"', add
label define metarea_lbl 9160 `"Wilmington, DE/NJ/MD"', add
label define metarea_lbl 9200 `"Wilmington, NC"', add
label define metarea_lbl 9240 `"Worcester, MA"', add
label define metarea_lbl 9260 `"Yakima, WA"', add
label define metarea_lbl 9270 `"Yolo, CA"', add
label define metarea_lbl 9280 `"York, PA"', add
label define metarea_lbl 9281 `"York-Hanover, PA"', add
label define metarea_lbl 9320 `"Youngstown-Warren, OH/PA"', add
label define metarea_lbl 9321 `"Youngstown-Warren-Boardman, OH"', add
label define metarea_lbl 9340 `"Yuba City, CA"', add
label define metarea_lbl 9360 `"Yuma, AZ"', add
label define metarea_lbl 9997 `"Other metropolitan areas, unidentified"', add
label define metarea_lbl 9998 `"NIU, household not in a metropolitan area"', add
label define metarea_lbl 9999 `"Missing data"', add
label values metarea metarea_lbl

label define ownershp_lbl 00 `"NIU"'
label define ownershp_lbl 10 `"Owned or being bought"', add
label define ownershp_lbl 21 `"No cash rent"', add
label define ownershp_lbl 22 `"With cash rent"', add
label values ownershp ownershp_lbl

label define pubhous_lbl 0 `"NIU"'
label define pubhous_lbl 1 `"No"', add
label define pubhous_lbl 2 `"Yes"', add
label values pubhous pubhous_lbl

label define rentsub_lbl 0 `"NIU"'
label define rentsub_lbl 1 `"No"', add
label define rentsub_lbl 2 `"Yes"', add
label values rentsub rentsub_lbl

label define heatsub_lbl 0 `"NIU"'
label define heatsub_lbl 1 `"No"', add
label define heatsub_lbl 2 `"Yes"', add
label values heatsub heatsub_lbl

label define foodstmp_lbl 0 `"NIU"'
label define foodstmp_lbl 1 `"No"', add
label define foodstmp_lbl 2 `"Yes"', add
label values foodstmp foodstmp_lbl

label define stampmo_lbl 00 `"NIU"'
label define stampmo_lbl 01 `"One"', add
label define stampmo_lbl 02 `"Two"', add
label define stampmo_lbl 03 `"Three"', add
label define stampmo_lbl 04 `"Four"', add
label define stampmo_lbl 05 `"Five"', add
label define stampmo_lbl 06 `"Six"', add
label define stampmo_lbl 07 `"Seven"', add
label define stampmo_lbl 08 `"Eight"', add
label define stampmo_lbl 09 `"Nine"', add
label define stampmo_lbl 10 `"Ten"', add
label define stampmo_lbl 11 `"Eleven"', add
label define stampmo_lbl 12 `"Twelve"', add
label values stampmo stampmo_lbl

label define atelunch_lbl 00 `"Zero"'
label define atelunch_lbl 01 `"One"', add
label define atelunch_lbl 02 `"Two"', add
label define atelunch_lbl 03 `"Three"', add
label define atelunch_lbl 04 `"Four"', add
label define atelunch_lbl 05 `"Five"', add
label define atelunch_lbl 06 `"Six"', add
label define atelunch_lbl 07 `"Seven"', add
label define atelunch_lbl 08 `"Eight"', add
label define atelunch_lbl 09 `"Nine or more"', add
label define atelunch_lbl 99 `"NIU"', add
label values atelunch atelunch_lbl

label define lunchsub_lbl 0 `"NIU"'
label define lunchsub_lbl 1 `"Yes, children receive free or reduced price lunch"', add
label define lunchsub_lbl 2 `"No, children did not receive free or reduced price lunch"', add
label values lunchsub lunchsub_lbl

label define frelunch_lbl 00 `"Zero"'
label define frelunch_lbl 01 `"One"', add
label define frelunch_lbl 02 `"Two"', add
label define frelunch_lbl 03 `"Three"', add
label define frelunch_lbl 04 `"Four"', add
label define frelunch_lbl 05 `"Five"', add
label define frelunch_lbl 06 `"Six"', add
label define frelunch_lbl 07 `"Seven"', add
label define frelunch_lbl 08 `"Eight"', add
label define frelunch_lbl 09 `"Nine or more"', add
label define frelunch_lbl 98 `"NIU -- Children didn't eat hot lunch"', add
label define frelunch_lbl 99 `"NIU -- No children in hh"', add
label values frelunch frelunch_lbl

label define unitsstr_lbl 00 `"NIU"'
label define unitsstr_lbl 01 `"Mobile home or trailer"', add
label define unitsstr_lbl 05 `"2 family building"', add
label define unitsstr_lbl 06 `"3-4 family building"', add
label define unitsstr_lbl 07 `"5-9 family building"', add
label define unitsstr_lbl 11 `"One unit, unspecified type"', add
label define unitsstr_lbl 12 `"10+ units in structure"', add
label values unitsstr unitsstr_lbl

label define phone_lbl 0 `"NIU"'
label define phone_lbl 1 `"No, no phone available"', add
label define phone_lbl 2 `"Yes, phone available"', add
label define phone_lbl 3 `"Yes, phone in household"', add
label define phone_lbl 9 `"Unknown"', add
label values phone phone_lbl

label define nfams_lbl 00 `"0 families (vacant unit)"'
label define nfams_lbl 01 `"1 family or N/A"', add
label define nfams_lbl 02 `"2 families"', add
label define nfams_lbl 03 `"3"', add
label define nfams_lbl 04 `"4"', add
label define nfams_lbl 05 `"5"', add
label define nfams_lbl 06 `"6"', add
label define nfams_lbl 07 `"7"', add
label define nfams_lbl 08 `"8"', add
label define nfams_lbl 09 `"9"', add
label define nfams_lbl 10 `"10"', add
label define nfams_lbl 11 `"11"', add
label define nfams_lbl 12 `"12"', add
label define nfams_lbl 13 `"13"', add
label define nfams_lbl 14 `"14"', add
label define nfams_lbl 15 `"15"', add
label define nfams_lbl 16 `"16"', add
label define nfams_lbl 17 `"17"', add
label define nfams_lbl 18 `"18"', add
label define nfams_lbl 19 `"19"', add
label define nfams_lbl 20 `"20"', add
label define nfams_lbl 21 `"21"', add
label define nfams_lbl 22 `"22"', add
label define nfams_lbl 23 `"23"', add
label define nfams_lbl 24 `"24"', add
label define nfams_lbl 25 `"25"', add
label define nfams_lbl 26 `"26"', add
label define nfams_lbl 27 `"27"', add
label define nfams_lbl 28 `"28"', add
label define nfams_lbl 29 `"29"', add
label define nfams_lbl 30 `"30"', add
label values nfams nfams_lbl

label define ncouples_lbl 0 `"0 couples or NIU"'
label define ncouples_lbl 1 `"1"', add
label define ncouples_lbl 2 `"2"', add
label define ncouples_lbl 3 `"3"', add
label define ncouples_lbl 4 `"4"', add
label define ncouples_lbl 5 `"5"', add
label define ncouples_lbl 6 `"6"', add
label define ncouples_lbl 7 `"7"', add
label define ncouples_lbl 9 `"9"', add
label values ncouples ncouples_lbl

label define nmothers_lbl 0 `"0 mothers or NIU"'
label define nmothers_lbl 1 `"1"', add
label define nmothers_lbl 2 `"2"', add
label define nmothers_lbl 3 `"3"', add
label define nmothers_lbl 4 `"4"', add
label define nmothers_lbl 5 `"5"', add
label define nmothers_lbl 6 `"6"', add
label values nmothers nmothers_lbl

label define nfathers_lbl 0 `"0 fathers or NIU"'
label define nfathers_lbl 1 `"1"', add
label define nfathers_lbl 2 `"2"', add
label define nfathers_lbl 3 `"3"', add
label define nfathers_lbl 4 `"4"', add
label define nfathers_lbl 5 `"5"', add
label define nfathers_lbl 6 `"6"', add
label values nfathers nfathers_lbl

label define month_lbl 01 `"January"'
label define month_lbl 02 `"February"', add
label define month_lbl 03 `"March"', add
label define month_lbl 04 `"April"', add
label define month_lbl 05 `"May"', add
label define month_lbl 06 `"June"', add
label define month_lbl 07 `"July"', add
label define month_lbl 08 `"August"', add
label define month_lbl 09 `"September"', add
label define month_lbl 10 `"October"', add
label define month_lbl 11 `"November"', add
label define month_lbl 12 `"December"', add
label values month month_lbl

label define hiurule_lbl 1 `"Point HIU reference person to self"'
label define hiurule_lbl 2 `"Point married female, with spouse present, to spouse "', add
label define hiurule_lbl 3 `"Point eligible child to father, if present "', add
label define hiurule_lbl 4 `"Point eligible child to mother, if present and father absent "', add
label define hiurule_lbl 5 `"Point single adults to self (single, spouse absent, separated, divorced, widowed and no own children in household) "', add
label define hiurule_lbl 6 `"Point related singleton children to first HIU reference person in household (no parents in household, but related to the reference person) "', add
label define hiurule_lbl 7 `"Point unrelated singleton children to self "', add
label define hiurule_lbl 9 `"N.A. (Not Available - 3/8 ASEC 2014 sample)"', add
label values hiurule hiurule_lbl

label define stepmom_lbl 0 `"No stepmother present"'
label define stepmom_lbl 1 `"Improbable age difference"', add
label define stepmom_lbl 2 `"Spouse of father"', add
label define stepmom_lbl 3 `"Identified stepmother"', add
label values stepmom stepmom_lbl

label define momrule_lbl 0 `"No mother link"'
label define momrule_lbl 1 `"Unambiguous mother link"', add
label define momrule_lbl 2 `"Daughter/granchild link"', add
label define momrule_lbl 3 `"Preceding male (no intervening person)"', add
label define momrule_lbl 7 `"Spouse of father becomes stepmother"', add
label values momrule momrule_lbl

label define steppop_lbl 0 `"No stepfather present"'
label define steppop_lbl 1 `"Improbable age difference"', add
label define steppop_lbl 2 `"Spouse of father"', add
label define steppop_lbl 3 `"Identified stepfather"', add
label values steppop steppop_lbl

label define poprule_lbl 0 `"No father link"'
label define poprule_lbl 1 `"Unambiguous father link"', add
label define poprule_lbl 2 `"Son/granchild link"', add
label define poprule_lbl 3 `"Preceding male (no intervening person)"', add
label define poprule_lbl 7 `"Husband of mother becomes stepfather"', add
label values poprule poprule_lbl

label define sprule_lbl 0 `"No spouse link"'
label define sprule_lbl 1 `"Wife follows husband"', add
label define sprule_lbl 2 `"Wife precedes husband"', add
label define sprule_lbl 3 `"Non-adjacent names -- consistent relationship to head/age differences"', add
label define sprule_lbl 4 `"Adjacent names (wife follows husband -- no age, other relative conflicts)"', add
label define sprule_lbl 5 `"Adjacent names (wife precedes husband -- no age, other relative conflicts)"', add
label define sprule_lbl 6 `"Alternate partner (if conflict in rules 1-5 link)"', add
label values sprule sprule_lbl

label define famsize_lbl 00 `"Missing"'
label define famsize_lbl 01 `"1 family member present"', add
label define famsize_lbl 02 `"2 family members present"', add
label define famsize_lbl 03 `"3 family members present"', add
label define famsize_lbl 04 `"4 family members present"', add
label define famsize_lbl 05 `"5 family members present"', add
label define famsize_lbl 06 `"6 family members present"', add
label define famsize_lbl 07 `"7 family members present"', add
label define famsize_lbl 08 `"8 family members present"', add
label define famsize_lbl 09 `"9 family members present"', add
label define famsize_lbl 10 `"10 family members present"', add
label define famsize_lbl 11 `"11 family members present"', add
label define famsize_lbl 12 `"12 family members present"', add
label define famsize_lbl 13 `"13 family members present"', add
label define famsize_lbl 14 `"14 family members present"', add
label define famsize_lbl 15 `"15 family members present"', add
label define famsize_lbl 16 `"16 family members present"', add
label define famsize_lbl 17 `"17 family members present"', add
label define famsize_lbl 18 `"18 family members present"', add
label define famsize_lbl 19 `"19 family members present"', add
label define famsize_lbl 20 `"20 family members present"', add
label define famsize_lbl 21 `"21 family members present"', add
label define famsize_lbl 22 `"22 family members present"', add
label define famsize_lbl 23 `"23 family members present"', add
label define famsize_lbl 24 `"24 family members present"', add
label define famsize_lbl 25 `"25 family members present"', add
label define famsize_lbl 26 `"26 family members present"', add
label define famsize_lbl 27 `"27 family members present"', add
label define famsize_lbl 28 `"28 family members present"', add
label define famsize_lbl 29 `"29 family members present"', add
label values famsize famsize_lbl

label define nchild_lbl 0 `"0 children present"'
label define nchild_lbl 1 `"1 child present"', add
label define nchild_lbl 2 `"2"', add
label define nchild_lbl 3 `"3"', add
label define nchild_lbl 4 `"4"', add
label define nchild_lbl 5 `"5"', add
label define nchild_lbl 6 `"6"', add
label define nchild_lbl 7 `"7"', add
label define nchild_lbl 8 `"8"', add
label define nchild_lbl 9 `"9+"', add
label values nchild nchild_lbl

label define nchlt5_lbl 0 `"No children under age 5"'
label define nchlt5_lbl 1 `"1 child under age 5"', add
label define nchlt5_lbl 2 `"2"', add
label define nchlt5_lbl 3 `"3"', add
label define nchlt5_lbl 4 `"4"', add
label define nchlt5_lbl 5 `"5"', add
label define nchlt5_lbl 6 `"6"', add
label define nchlt5_lbl 7 `"7"', add
label define nchlt5_lbl 8 `"8"', add
label define nchlt5_lbl 9 `"9+"', add
label values nchlt5 nchlt5_lbl

label define famunit_lbl 01 `"1st family in household or group quarters"'
label define famunit_lbl 02 `"2nd family in household or group quarters"', add
label define famunit_lbl 03 `"3rd"', add
label define famunit_lbl 04 `"4th"', add
label define famunit_lbl 05 `"5th"', add
label define famunit_lbl 06 `"6th"', add
label define famunit_lbl 07 `"7th"', add
label define famunit_lbl 08 `"8th"', add
label define famunit_lbl 09 `"9th"', add
label define famunit_lbl 10 `"10"', add
label define famunit_lbl 11 `"11"', add
label define famunit_lbl 12 `"12"', add
label define famunit_lbl 13 `"13"', add
label define famunit_lbl 14 `"14"', add
label define famunit_lbl 15 `"15"', add
label define famunit_lbl 16 `"16"', add
label define famunit_lbl 17 `"17"', add
label define famunit_lbl 18 `"18"', add
label define famunit_lbl 19 `"19"', add
label define famunit_lbl 20 `"20"', add
label define famunit_lbl 21 `"21"', add
label define famunit_lbl 22 `"22"', add
label define famunit_lbl 23 `"23"', add
label define famunit_lbl 24 `"24"', add
label define famunit_lbl 25 `"25"', add
label define famunit_lbl 26 `"26"', add
label define famunit_lbl 27 `"27"', add
label define famunit_lbl 28 `"28"', add
label define famunit_lbl 29 `"29"', add
label values famunit famunit_lbl

label define eldch_lbl 00 `"Less than 1 year old"'
label define eldch_lbl 01 `"1"', add
label define eldch_lbl 02 `"2"', add
label define eldch_lbl 03 `"3"', add
label define eldch_lbl 04 `"4"', add
label define eldch_lbl 05 `"5"', add
label define eldch_lbl 06 `"6"', add
label define eldch_lbl 07 `"7"', add
label define eldch_lbl 08 `"8"', add
label define eldch_lbl 09 `"9"', add
label define eldch_lbl 10 `"10"', add
label define eldch_lbl 11 `"11"', add
label define eldch_lbl 12 `"12"', add
label define eldch_lbl 13 `"13"', add
label define eldch_lbl 14 `"14"', add
label define eldch_lbl 15 `"15"', add
label define eldch_lbl 16 `"16"', add
label define eldch_lbl 17 `"17"', add
label define eldch_lbl 18 `"18"', add
label define eldch_lbl 19 `"19"', add
label define eldch_lbl 20 `"20"', add
label define eldch_lbl 21 `"21"', add
label define eldch_lbl 22 `"22"', add
label define eldch_lbl 23 `"23"', add
label define eldch_lbl 24 `"24"', add
label define eldch_lbl 25 `"25"', add
label define eldch_lbl 26 `"26"', add
label define eldch_lbl 27 `"27"', add
label define eldch_lbl 28 `"28"', add
label define eldch_lbl 29 `"29"', add
label define eldch_lbl 30 `"30"', add
label define eldch_lbl 31 `"31"', add
label define eldch_lbl 32 `"32"', add
label define eldch_lbl 33 `"33"', add
label define eldch_lbl 34 `"34"', add
label define eldch_lbl 35 `"35"', add
label define eldch_lbl 36 `"36"', add
label define eldch_lbl 37 `"37"', add
label define eldch_lbl 38 `"38"', add
label define eldch_lbl 39 `"39"', add
label define eldch_lbl 40 `"40"', add
label define eldch_lbl 41 `"41"', add
label define eldch_lbl 42 `"42"', add
label define eldch_lbl 43 `"43"', add
label define eldch_lbl 44 `"44"', add
label define eldch_lbl 45 `"45"', add
label define eldch_lbl 46 `"46"', add
label define eldch_lbl 47 `"47"', add
label define eldch_lbl 48 `"48"', add
label define eldch_lbl 49 `"49"', add
label define eldch_lbl 50 `"50"', add
label define eldch_lbl 51 `"51"', add
label define eldch_lbl 52 `"52"', add
label define eldch_lbl 53 `"53"', add
label define eldch_lbl 54 `"54"', add
label define eldch_lbl 55 `"55"', add
label define eldch_lbl 56 `"56"', add
label define eldch_lbl 57 `"57"', add
label define eldch_lbl 58 `"58"', add
label define eldch_lbl 59 `"59"', add
label define eldch_lbl 60 `"60"', add
label define eldch_lbl 61 `"61"', add
label define eldch_lbl 62 `"62"', add
label define eldch_lbl 63 `"63"', add
label define eldch_lbl 64 `"64"', add
label define eldch_lbl 65 `"65"', add
label define eldch_lbl 66 `"66"', add
label define eldch_lbl 67 `"67"', add
label define eldch_lbl 68 `"68"', add
label define eldch_lbl 69 `"69"', add
label define eldch_lbl 70 `"70"', add
label define eldch_lbl 71 `"71"', add
label define eldch_lbl 72 `"72"', add
label define eldch_lbl 73 `"73"', add
label define eldch_lbl 74 `"74"', add
label define eldch_lbl 75 `"75"', add
label define eldch_lbl 76 `"76"', add
label define eldch_lbl 77 `"77"', add
label define eldch_lbl 78 `"78"', add
label define eldch_lbl 79 `"79"', add
label define eldch_lbl 80 `"80"', add
label define eldch_lbl 81 `"81"', add
label define eldch_lbl 82 `"82"', add
label define eldch_lbl 83 `"83"', add
label define eldch_lbl 84 `"84"', add
label define eldch_lbl 85 `"85"', add
label define eldch_lbl 86 `"86"', add
label define eldch_lbl 87 `"87"', add
label define eldch_lbl 88 `"88"', add
label define eldch_lbl 89 `"89"', add
label define eldch_lbl 90 `"90"', add
label define eldch_lbl 91 `"91"', add
label define eldch_lbl 92 `"92"', add
label define eldch_lbl 93 `"93"', add
label define eldch_lbl 94 `"94"', add
label define eldch_lbl 95 `"95"', add
label define eldch_lbl 96 `"96"', add
label define eldch_lbl 97 `"97"', add
label define eldch_lbl 98 `"98"', add
label define eldch_lbl 99 `"NIU"', add
label values eldch eldch_lbl

label define yngch_lbl 00 `"Less than 1 year old"'
label define yngch_lbl 01 `"1"', add
label define yngch_lbl 02 `"2"', add
label define yngch_lbl 03 `"3"', add
label define yngch_lbl 04 `"4"', add
label define yngch_lbl 05 `"5"', add
label define yngch_lbl 06 `"6"', add
label define yngch_lbl 07 `"7"', add
label define yngch_lbl 08 `"8"', add
label define yngch_lbl 09 `"9"', add
label define yngch_lbl 10 `"10"', add
label define yngch_lbl 11 `"11"', add
label define yngch_lbl 12 `"12"', add
label define yngch_lbl 13 `"13"', add
label define yngch_lbl 14 `"14"', add
label define yngch_lbl 15 `"15"', add
label define yngch_lbl 16 `"16"', add
label define yngch_lbl 17 `"17"', add
label define yngch_lbl 18 `"18"', add
label define yngch_lbl 19 `"19"', add
label define yngch_lbl 20 `"20"', add
label define yngch_lbl 21 `"21"', add
label define yngch_lbl 22 `"22"', add
label define yngch_lbl 23 `"23"', add
label define yngch_lbl 24 `"24"', add
label define yngch_lbl 25 `"25"', add
label define yngch_lbl 26 `"26"', add
label define yngch_lbl 27 `"27"', add
label define yngch_lbl 28 `"28"', add
label define yngch_lbl 29 `"29"', add
label define yngch_lbl 30 `"30"', add
label define yngch_lbl 31 `"31"', add
label define yngch_lbl 32 `"32"', add
label define yngch_lbl 33 `"33"', add
label define yngch_lbl 34 `"34"', add
label define yngch_lbl 35 `"35"', add
label define yngch_lbl 36 `"36"', add
label define yngch_lbl 37 `"37"', add
label define yngch_lbl 38 `"38"', add
label define yngch_lbl 39 `"39"', add
label define yngch_lbl 40 `"40"', add
label define yngch_lbl 41 `"41"', add
label define yngch_lbl 42 `"42"', add
label define yngch_lbl 43 `"43"', add
label define yngch_lbl 44 `"44"', add
label define yngch_lbl 45 `"45"', add
label define yngch_lbl 46 `"46"', add
label define yngch_lbl 47 `"47"', add
label define yngch_lbl 48 `"48"', add
label define yngch_lbl 49 `"49"', add
label define yngch_lbl 50 `"50"', add
label define yngch_lbl 51 `"51"', add
label define yngch_lbl 52 `"52"', add
label define yngch_lbl 53 `"53"', add
label define yngch_lbl 54 `"54"', add
label define yngch_lbl 55 `"55"', add
label define yngch_lbl 56 `"56"', add
label define yngch_lbl 57 `"57"', add
label define yngch_lbl 58 `"58"', add
label define yngch_lbl 59 `"59"', add
label define yngch_lbl 60 `"60"', add
label define yngch_lbl 61 `"61"', add
label define yngch_lbl 62 `"62"', add
label define yngch_lbl 63 `"63"', add
label define yngch_lbl 64 `"64"', add
label define yngch_lbl 65 `"65"', add
label define yngch_lbl 66 `"66"', add
label define yngch_lbl 67 `"67"', add
label define yngch_lbl 68 `"68"', add
label define yngch_lbl 69 `"69"', add
label define yngch_lbl 70 `"70"', add
label define yngch_lbl 71 `"71"', add
label define yngch_lbl 72 `"72"', add
label define yngch_lbl 73 `"73"', add
label define yngch_lbl 74 `"74"', add
label define yngch_lbl 75 `"75"', add
label define yngch_lbl 76 `"76"', add
label define yngch_lbl 77 `"77"', add
label define yngch_lbl 78 `"78"', add
label define yngch_lbl 79 `"79"', add
label define yngch_lbl 80 `"80"', add
label define yngch_lbl 81 `"81"', add
label define yngch_lbl 82 `"82"', add
label define yngch_lbl 83 `"83"', add
label define yngch_lbl 84 `"84"', add
label define yngch_lbl 85 `"85"', add
label define yngch_lbl 86 `"86"', add
label define yngch_lbl 87 `"87"', add
label define yngch_lbl 88 `"88"', add
label define yngch_lbl 89 `"89"', add
label define yngch_lbl 90 `"90"', add
label define yngch_lbl 91 `"91"', add
label define yngch_lbl 92 `"92"', add
label define yngch_lbl 93 `"93"', add
label define yngch_lbl 94 `"94"', add
label define yngch_lbl 95 `"95"', add
label define yngch_lbl 96 `"96"', add
label define yngch_lbl 97 `"97"', add
label define yngch_lbl 98 `"98"', add
label define yngch_lbl 99 `"NIU"', add
label values yngch yngch_lbl

label define nsibs_lbl 0 `"0 Siblings"'
label define nsibs_lbl 1 `"1 Sibling"', add
label define nsibs_lbl 2 `"2 Siblings"', add
label define nsibs_lbl 3 `"3 Siblings"', add
label define nsibs_lbl 4 `"4 Siblings"', add
label define nsibs_lbl 5 `"5 Siblings"', add
label define nsibs_lbl 6 `"6 Siblings"', add
label define nsibs_lbl 7 `"7 Siblings"', add
label define nsibs_lbl 8 `"8 Siblings"', add
label define nsibs_lbl 9 `"9 or more Siblings"', add
label values nsibs nsibs_lbl

label define pemomtyp_lbl 00 `"No mother present"'
label define pemomtyp_lbl 01 `"Biological"', add
label define pemomtyp_lbl 02 `"Step"', add
label define pemomtyp_lbl 03 `"Adopted"', add
label values pemomtyp pemomtyp_lbl

label define pedadtyp_lbl 00 `"No father present"'
label define pedadtyp_lbl 01 `"Biological"', add
label define pedadtyp_lbl 02 `"Step"', add
label define pedadtyp_lbl 03 `"Adopted"', add
label values pedadtyp pedadtyp_lbl

label define relate_lbl 0101 `"Head/householder"'
label define relate_lbl 0201 `"Spouse"', add
label define relate_lbl 0301 `"Child"', add
label define relate_lbl 0303 `"Stepchild"', add
label define relate_lbl 0501 `"Parent"', add
label define relate_lbl 0701 `"Sibling"', add
label define relate_lbl 0901 `"Grandchild"', add
label define relate_lbl 1001 `"Other relatives, n.s."', add
label define relate_lbl 1113 `"Partner/roommate"', add
label define relate_lbl 1114 `"Unmarried partner"', add
label define relate_lbl 1115 `"Housemate/roomate"', add
label define relate_lbl 1241 `"Roomer/boarder/lodger"', add
label define relate_lbl 1242 `"Foster children"', add
label define relate_lbl 1260 `"Other nonrelatives"', add
label define relate_lbl 9100 `"Armed Forces, relationship unknown"', add
label define relate_lbl 9200 `"Age under 14, relationship unknown"', add
label define relate_lbl 9900 `"Relationship unknown"', add
label define relate_lbl 9999 `"9999"', add
label values relate relate_lbl

label define age_lbl 00 `"Under 1 year"'
label define age_lbl 01 `"1"', add
label define age_lbl 02 `"2"', add
label define age_lbl 03 `"3"', add
label define age_lbl 04 `"4"', add
label define age_lbl 05 `"5"', add
label define age_lbl 06 `"6"', add
label define age_lbl 07 `"7"', add
label define age_lbl 08 `"8"', add
label define age_lbl 09 `"9"', add
label define age_lbl 10 `"10"', add
label define age_lbl 11 `"11"', add
label define age_lbl 12 `"12"', add
label define age_lbl 13 `"13"', add
label define age_lbl 14 `"14"', add
label define age_lbl 15 `"15"', add
label define age_lbl 16 `"16"', add
label define age_lbl 17 `"17"', add
label define age_lbl 18 `"18"', add
label define age_lbl 19 `"19"', add
label define age_lbl 20 `"20"', add
label define age_lbl 21 `"21"', add
label define age_lbl 22 `"22"', add
label define age_lbl 23 `"23"', add
label define age_lbl 24 `"24"', add
label define age_lbl 25 `"25"', add
label define age_lbl 26 `"26"', add
label define age_lbl 27 `"27"', add
label define age_lbl 28 `"28"', add
label define age_lbl 29 `"29"', add
label define age_lbl 30 `"30"', add
label define age_lbl 31 `"31"', add
label define age_lbl 32 `"32"', add
label define age_lbl 33 `"33"', add
label define age_lbl 34 `"34"', add
label define age_lbl 35 `"35"', add
label define age_lbl 36 `"36"', add
label define age_lbl 37 `"37"', add
label define age_lbl 38 `"38"', add
label define age_lbl 39 `"39"', add
label define age_lbl 40 `"40"', add
label define age_lbl 41 `"41"', add
label define age_lbl 42 `"42"', add
label define age_lbl 43 `"43"', add
label define age_lbl 44 `"44"', add
label define age_lbl 45 `"45"', add
label define age_lbl 46 `"46"', add
label define age_lbl 47 `"47"', add
label define age_lbl 48 `"48"', add
label define age_lbl 49 `"49"', add
label define age_lbl 50 `"50"', add
label define age_lbl 51 `"51"', add
label define age_lbl 52 `"52"', add
label define age_lbl 53 `"53"', add
label define age_lbl 54 `"54"', add
label define age_lbl 55 `"55"', add
label define age_lbl 56 `"56"', add
label define age_lbl 57 `"57"', add
label define age_lbl 58 `"58"', add
label define age_lbl 59 `"59"', add
label define age_lbl 60 `"60"', add
label define age_lbl 61 `"61"', add
label define age_lbl 62 `"62"', add
label define age_lbl 63 `"63"', add
label define age_lbl 64 `"64"', add
label define age_lbl 65 `"65"', add
label define age_lbl 66 `"66"', add
label define age_lbl 67 `"67"', add
label define age_lbl 68 `"68"', add
label define age_lbl 69 `"69"', add
label define age_lbl 70 `"70"', add
label define age_lbl 71 `"71"', add
label define age_lbl 72 `"72"', add
label define age_lbl 73 `"73"', add
label define age_lbl 74 `"74"', add
label define age_lbl 75 `"75"', add
label define age_lbl 76 `"76"', add
label define age_lbl 77 `"77"', add
label define age_lbl 78 `"78"', add
label define age_lbl 79 `"79"', add
label define age_lbl 80 `"80"', add
label define age_lbl 81 `"81"', add
label define age_lbl 82 `"82"', add
label define age_lbl 83 `"83"', add
label define age_lbl 84 `"84"', add
label define age_lbl 85 `"85"', add
label define age_lbl 86 `"86"', add
label define age_lbl 87 `"87"', add
label define age_lbl 88 `"88"', add
label define age_lbl 89 `"89"', add
label define age_lbl 90 `"90 (90+, 1988-2002)"', add
label define age_lbl 91 `"91"', add
label define age_lbl 92 `"92"', add
label define age_lbl 93 `"93"', add
label define age_lbl 94 `"94"', add
label define age_lbl 95 `"95"', add
label define age_lbl 96 `"96"', add
label define age_lbl 97 `"97"', add
label define age_lbl 98 `"98"', add
label define age_lbl 99 `"99+"', add
label values age age_lbl

label define sex_lbl 1 `"Male"'
label define sex_lbl 2 `"Female"', add
label define sex_lbl 9 `"NIU"', add
label values sex sex_lbl

label define race_lbl 100 `"White"'
label define race_lbl 200 `"Black/Negro"', add
label define race_lbl 300 `"American Indian/Aleut/Eskimo"', add
label define race_lbl 650 `"Asian or Pacific Islander"', add
label define race_lbl 651 `"Asian only"', add
label define race_lbl 652 `"Hawaiian/Pacific Islander only"', add
label define race_lbl 700 `"Other (single) race, n.e.c."', add
label define race_lbl 801 `"White-Black"', add
label define race_lbl 802 `"White-American Indian"', add
label define race_lbl 803 `"White-Asian"', add
label define race_lbl 804 `"White-Hawaiian/Pacific Islander"', add
label define race_lbl 805 `"Black-American Indian"', add
label define race_lbl 806 `"Black-Asian"', add
label define race_lbl 807 `"Black-Hawaiian/Pacific Islander"', add
label define race_lbl 808 `"American Indian-Asian"', add
label define race_lbl 809 `"Asian-Hawaiian/Pacific Islander"', add
label define race_lbl 810 `"White-Black-American Indian"', add
label define race_lbl 811 `"White-Black-Asian"', add
label define race_lbl 812 `"White-American Indian-Asian"', add
label define race_lbl 813 `"White-Asian-Hawaiian/Pacific Islander"', add
label define race_lbl 814 `"White-Black-American Indian-Asian"', add
label define race_lbl 815 `"American Indian-Hawaiian/Pacific Islander"', add
label define race_lbl 816 `"White-Black--Hawaiian/Pacific Islander"', add
label define race_lbl 817 `"White-American Indian-Hawaiian/Pacific Islander"', add
label define race_lbl 818 `"Black-American Indian-Asian"', add
label define race_lbl 819 `"White-American Indian-Asian-Hawaiian/Pacific Islander"', add
label define race_lbl 820 `"Two or three races, unspecified"', add
label define race_lbl 830 `"Four or five races, unspecified"', add
label define race_lbl 999 `"NIU"', add
label values race race_lbl

label define marst_lbl 1 `"Married, spouse present"'
label define marst_lbl 2 `"Married, spouse absent"', add
label define marst_lbl 3 `"Separated"', add
label define marst_lbl 4 `"Divorced"', add
label define marst_lbl 5 `"Widowed"', add
label define marst_lbl 6 `"Never married/single"', add
label define marst_lbl 7 `"Widowed or Divorced"', add
label define marst_lbl 9 `"Unknown"', add
label values marst marst_lbl

label define popstat_lbl 1 `"Adult civilian"'
label define popstat_lbl 2 `"Armed Forces"', add
label define popstat_lbl 3 `"Child"', add
label values popstat popstat_lbl

label define bpl_lbl 09900 `"United States, n.s."'
label define bpl_lbl 10000 `"American Samoa"', add
label define bpl_lbl 10500 `"Guam"', add
label define bpl_lbl 10750 `"Northern Mariana Islands"', add
label define bpl_lbl 11000 `"Puerto Rico"', add
label define bpl_lbl 11500 `"U.S. Virgin Islands"', add
label define bpl_lbl 12090 `"U.S. outlying areas, n.s."', add
label define bpl_lbl 15000 `"Canada"', add
label define bpl_lbl 16010 `"Bermuda"', add
label define bpl_lbl 19900 `"North America, n.s."', add
label define bpl_lbl 20000 `"Mexico"', add
label define bpl_lbl 21010 `"Belize/British Honduras"', add
label define bpl_lbl 21020 `"Costa Rica"', add
label define bpl_lbl 21030 `"El Salvador"', add
label define bpl_lbl 21040 `"Guatemala"', add
label define bpl_lbl 21050 `"Honduras"', add
label define bpl_lbl 21060 `"Nicaragua"', add
label define bpl_lbl 21070 `"Panama"', add
label define bpl_lbl 21090 `"Central America, n.s."', add
label define bpl_lbl 25000 `"Cuba"', add
label define bpl_lbl 26010 `"Dominican Republic"', add
label define bpl_lbl 26020 `"Haiti"', add
label define bpl_lbl 26030 `"Jamaica"', add
label define bpl_lbl 26043 `"Bahamas"', add
label define bpl_lbl 26044 `"Barbados"', add
label define bpl_lbl 26054 `"Dominica"', add
label define bpl_lbl 26055 `"Grenada"', add
label define bpl_lbl 26060 `"Trinidad and Tobago"', add
label define bpl_lbl 26065 `"Antigua and Barbuda"', add
label define bpl_lbl 26070 `"St. Kitts--Nevis"', add
label define bpl_lbl 26075 `"St. Lucia"', add
label define bpl_lbl 26080 `"St. Vincent and the Grenadi"', add
label define bpl_lbl 26091 `"Caribbean, n.s."', add
label define bpl_lbl 30005 `"Argentina"', add
label define bpl_lbl 30010 `"Bolivia"', add
label define bpl_lbl 30015 `"Brazil"', add
label define bpl_lbl 30020 `"Chile"', add
label define bpl_lbl 30025 `"Colombia"', add
label define bpl_lbl 30030 `"Ecuador"', add
label define bpl_lbl 30040 `"Guyana/British Guiana"', add
label define bpl_lbl 30050 `"Peru"', add
label define bpl_lbl 30060 `"Uruguay"', add
label define bpl_lbl 30065 `"Venezuela"', add
label define bpl_lbl 30070 `"Paraguay"', add
label define bpl_lbl 30090 `"South America, n.s."', add
label define bpl_lbl 31000 `"Americas, n.s."', add
label define bpl_lbl 40000 `"Denmark"', add
label define bpl_lbl 40100 `"Finland"', add
label define bpl_lbl 40200 `"Iceland"', add
label define bpl_lbl 40400 `"Norway"', add
label define bpl_lbl 40500 `"Sweden"', add
label define bpl_lbl 41000 `"England"', add
label define bpl_lbl 41100 `"Scotland"', add
label define bpl_lbl 41200 `"Wales"', add
label define bpl_lbl 41300 `"United Kingdom, n.s."', add
label define bpl_lbl 41400 `"Ireland"', add
label define bpl_lbl 41410 `"Northern Ireland"', add
label define bpl_lbl 42000 `"Belgium"', add
label define bpl_lbl 42100 `"France"', add
label define bpl_lbl 42500 `"Netherlands"', add
label define bpl_lbl 42600 `"Switzerland"', add
label define bpl_lbl 43300 `"Greece"', add
label define bpl_lbl 43400 `"Italy"', add
label define bpl_lbl 43600 `"Portugal"', add
label define bpl_lbl 43610 `"Azores"', add
label define bpl_lbl 43800 `"Spain"', add
label define bpl_lbl 45000 `"Austria"', add
label define bpl_lbl 45200 `"Czechoslavakia"', add
label define bpl_lbl 45212 `"Slovakia"', add
label define bpl_lbl 45213 `"Czech Republic"', add
label define bpl_lbl 45300 `"Germany"', add
label define bpl_lbl 45400 `"Hungary"', add
label define bpl_lbl 45500 `"Poland"', add
label define bpl_lbl 45600 `"Romania"', add
label define bpl_lbl 45650 `"Bulgaria"', add
label define bpl_lbl 45675 `"Albania"', add
label define bpl_lbl 45700 `"Yugoslavia"', add
label define bpl_lbl 45720 `"Bosnia and Herzegovina"', add
label define bpl_lbl 45730 `"Croatia"', add
label define bpl_lbl 45740 `"Macedonia"', add
label define bpl_lbl 45750 `"Serbia"', add
label define bpl_lbl 45760 `"Kosovo"', add
label define bpl_lbl 45770 `"Montenego"', add
label define bpl_lbl 46100 `"Estonia"', add
label define bpl_lbl 46200 `"Latvia"', add
label define bpl_lbl 46300 `"Lithuania"', add
label define bpl_lbl 46500 `"Other USSR/Russia"', add
label define bpl_lbl 46530 `"Ukraine"', add
label define bpl_lbl 46535 `"Belarus"', add
label define bpl_lbl 46540 `"Moldova"', add
label define bpl_lbl 46590 `"USSR, n.s."', add
label define bpl_lbl 49900 `"Europe, n.s."', add
label define bpl_lbl 50000 `"China"', add
label define bpl_lbl 50010 `"Hong Kong"', add
label define bpl_lbl 50040 `"Taiwan"', add
label define bpl_lbl 50100 `"Japan"', add
label define bpl_lbl 50200 `"Korea"', add
label define bpl_lbl 50220 `"South Korea"', add
label define bpl_lbl 50300 `"Mongolia"', add
label define bpl_lbl 51100 `"Cambodia"', add
label define bpl_lbl 51200 `"Indonesia"', add
label define bpl_lbl 51300 `"Laos"', add
label define bpl_lbl 51400 `"Malaysia"', add
label define bpl_lbl 51500 `"Philippines"', add
label define bpl_lbl 51600 `"Singapore"', add
label define bpl_lbl 51700 `"Thailand"', add
label define bpl_lbl 51800 `"Vietnam"', add
label define bpl_lbl 52000 `"Afghanistan"', add
label define bpl_lbl 52100 `"India"', add
label define bpl_lbl 52110 `"Bangladesh"', add
label define bpl_lbl 52120 `"Bhutan"', add
label define bpl_lbl 52130 `"Burma"', add
label define bpl_lbl 52140 `"Pakistan"', add
label define bpl_lbl 52150 `"Sri Lanka"', add
label define bpl_lbl 52200 `"Nepal"', add
label define bpl_lbl 55100 `"Armenia"', add
label define bpl_lbl 55200 `"Azerbaijan"', add
label define bpl_lbl 55300 `"Georgia"', add
label define bpl_lbl 55400 `"Uzbekistan"', add
label define bpl_lbl 55500 `"Kazakhstan"', add
label define bpl_lbl 53000 `"Iran"', add
label define bpl_lbl 53200 `"Iraq"', add
label define bpl_lbl 53400 `"Israel"', add
label define bpl_lbl 53420 `"Palestine"', add
label define bpl_lbl 53500 `"Jordan"', add
label define bpl_lbl 53700 `"Lebanon"', add
label define bpl_lbl 54000 `"Saudi Arabia"', add
label define bpl_lbl 54100 `"Syria"', add
label define bpl_lbl 54200 `"Turkey"', add
label define bpl_lbl 54300 `"Cyprus"', add
label define bpl_lbl 54350 `"Kuwait"', add
label define bpl_lbl 54400 `"Yemen"', add
label define bpl_lbl 54500 `"United Arab Emirates"', add
label define bpl_lbl 54700 `"Middle East, n.s."', add
label define bpl_lbl 59900 `"Asia, n.e.c./n.s."', add
label define bpl_lbl 60010 `"Northern Africa"', add
label define bpl_lbl 60012 `"Egypt/United Arab Rep."', add
label define bpl_lbl 60014 `"Morocco"', add
label define bpl_lbl 60016 `"Algeria"', add
label define bpl_lbl 60018 `"Sudan"', add
label define bpl_lbl 60019 `"Libya"', add
label define bpl_lbl 60023 `"Ghana"', add
label define bpl_lbl 60031 `"Nigeria"', add
label define bpl_lbl 60032 `"Cameroon"', add
label define bpl_lbl 60033 `"Cape Verde"', add
label define bpl_lbl 60034 `"Liberia"', add
label define bpl_lbl 60035 `"Senegal"', add
label define bpl_lbl 60036 `"Sierra Leone"', add
label define bpl_lbl 60037 `"Guinea"', add
label define bpl_lbl 60038 `"Ivory Coast"', add
label define bpl_lbl 60039 `"Togo"', add
label define bpl_lbl 60040 `"Eritrea"', add
label define bpl_lbl 60044 `"Ethiopia"', add
label define bpl_lbl 60045 `"Kenya"', add
label define bpl_lbl 60050 `"Somalia"', add
label define bpl_lbl 60060 `"Tanzania"', add
label define bpl_lbl 60065 `"Uganda"', add
label define bpl_lbl 60070 `"Zimbabwe"', add
label define bpl_lbl 60094 `"South Africa (Union of)"', add
label define bpl_lbl 60095 `"Zaire"', add
label define bpl_lbl 60096 `"Congo"', add
label define bpl_lbl 60097 `"Zambia"', add
label define bpl_lbl 60099 `"Africa, n.s./n.e.c."', add
label define bpl_lbl 70010 `"Australia"', add
label define bpl_lbl 70020 `"New Zealand"', add
label define bpl_lbl 71000 `"Pacific Islands"', add
label define bpl_lbl 71021 `"Fiji"', add
label define bpl_lbl 71022 `"Tonga"', add
label define bpl_lbl 71023 `"Samoa"', add
label define bpl_lbl 71024 `"Marshall Islands"', add
label define bpl_lbl 72000 `"Micronesia"', add
label define bpl_lbl 96000 `"Other, n.e.c. and unknown"', add
label define bpl_lbl 99999 `"NIU"', add
label values bpl bpl_lbl

label define yrimmig_lbl 0000 `"NIU"'
label define yrimmig_lbl 1949 `"1949 or earlier"', add
label define yrimmig_lbl 1959 `"1950-1959"', add
label define yrimmig_lbl 1964 `"1960-1964"', add
label define yrimmig_lbl 1969 `"1965-1969"', add
label define yrimmig_lbl 1974 `"1970-1974"', add
label define yrimmig_lbl 1979 `"1975-1979"', add
label define yrimmig_lbl 1981 `"1980-1981"', add
label define yrimmig_lbl 1983 `"1982-1983"', add
label define yrimmig_lbl 1985 `"1984-1985"', add
label define yrimmig_lbl 1987 `"1986-1987"', add
label define yrimmig_lbl 1989 `"1988-1989"', add
label define yrimmig_lbl 1991 `"1990-1991"', add
label define yrimmig_lbl 1993 `"1992-1993"', add
label define yrimmig_lbl 1994 `"1992-1994"', add
label define yrimmig_lbl 1995 `"1994-1995"', add
label define yrimmig_lbl 1996 `"1994-1996"', add
label define yrimmig_lbl 1997 `"1996-1997"', add
label define yrimmig_lbl 1998 `"1996-1998 (2000 CPS: 1998)"', add
label define yrimmig_lbl 1999 `"1998-1999 (1999 CPS: 1996-1999)"', add
label define yrimmig_lbl 2000 `"1998-2000"', add
label define yrimmig_lbl 2001 `"2000-2001 (2001 CPS: 1998-2001)"', add
label define yrimmig_lbl 2002 `"2000-2002"', add
label define yrimmig_lbl 2003 `"2002-2003 (2003 CPS: 2000-2003)"', add
label define yrimmig_lbl 2004 `"2002-2004"', add
label define yrimmig_lbl 2005 `"2004-2005 (2005 CPS: 2002-2005)"', add
label define yrimmig_lbl 2006 `"2004-2006"', add
label define yrimmig_lbl 2007 `"2004-2007"', add
label define yrimmig_lbl 2008 `"2006-2008 (2006-2007 CPS: 2004-2008)"', add
label define yrimmig_lbl 2009 `"2006-2009"', add
label define yrimmig_lbl 2010 `"2008-2010 (2012 CPS: 2008-2009)"', add
label define yrimmig_lbl 2011 `"2008-2011"', add
label define yrimmig_lbl 2012 `"2010-2012 (2014 CPS: 2010-2011)"', add
label define yrimmig_lbl 2013 `"2010-2013"', add
label define yrimmig_lbl 2014 `"2012-2014"', add
label values yrimmig yrimmig_lbl

label define citizen_lbl 0 `"NIU"'
label define citizen_lbl 1 `"Born abroad of American parents"', add
label define citizen_lbl 2 `"Naturalized citizen"', add
label define citizen_lbl 3 `"Not a citizen"', add
label define citizen_lbl 9 `"NIU"', add
label values citizen citizen_lbl

label define mbpl_lbl 09900 `"U.S., n.s."'
label define mbpl_lbl 10000 `"American Samoa"', add
label define mbpl_lbl 10500 `"Guam"', add
label define mbpl_lbl 11000 `"Puerto Rico"', add
label define mbpl_lbl 11500 `"U.S. Virgin Islands"', add
label define mbpl_lbl 12090 `"U.S. outlying areas, n.s."', add
label define mbpl_lbl 15000 `"Canada"', add
label define mbpl_lbl 16010 `"Bermuda"', add
label define mbpl_lbl 10750 `"Northern Mariana Islands"', add
label define mbpl_lbl 19900 `"North America, n.s."', add
label define mbpl_lbl 20000 `"Mexico"', add
label define mbpl_lbl 21010 `"Belize/British Honduras"', add
label define mbpl_lbl 21020 `"Costa Rica"', add
label define mbpl_lbl 21030 `"El Salvador"', add
label define mbpl_lbl 21040 `"Guatemala"', add
label define mbpl_lbl 21050 `"Honduras"', add
label define mbpl_lbl 21060 `"Nicaragua"', add
label define mbpl_lbl 21070 `"Panama"', add
label define mbpl_lbl 21090 `"Central America, n.s."', add
label define mbpl_lbl 25000 `"Cuba"', add
label define mbpl_lbl 26000 `"West Indies"', add
label define mbpl_lbl 26010 `"Dominican Republic"', add
label define mbpl_lbl 26020 `"Haiti"', add
label define mbpl_lbl 26030 `"Jamaica"', add
label define mbpl_lbl 26043 `"Bahamas"', add
label define mbpl_lbl 26044 `"Barbados"', add
label define mbpl_lbl 26054 `"Dominica"', add
label define mbpl_lbl 26055 `"Grenada"', add
label define mbpl_lbl 26060 `"Trinidad and Tobago"', add
label define mbpl_lbl 26065 `"Antigua and Barbuda"', add
label define mbpl_lbl 26070 `"St. Kitts--Nevis"', add
label define mbpl_lbl 26075 `"St. Lucia"', add
label define mbpl_lbl 26080 `"St. Vincent and the Grenadi"', add
label define mbpl_lbl 30000 `"South America"', add
label define mbpl_lbl 30005 `"Argentina"', add
label define mbpl_lbl 30010 `"Bolivia"', add
label define mbpl_lbl 30015 `"Brazil"', add
label define mbpl_lbl 30020 `"Chile"', add
label define mbpl_lbl 30025 `"Colombia"', add
label define mbpl_lbl 30030 `"Ecuador"', add
label define mbpl_lbl 30040 `"Guyana/British Guiana"', add
label define mbpl_lbl 30050 `"Peru"', add
label define mbpl_lbl 30060 `"Uruguay"', add
label define mbpl_lbl 30065 `"Venezuala"', add
label define mbpl_lbl 30070 `"Paraguay"', add
label define mbpl_lbl 31000 `"Americas, n.s."', add
label define mbpl_lbl 40000 `"Denmark"', add
label define mbpl_lbl 40100 `"Finland"', add
label define mbpl_lbl 40200 `"Iceland"', add
label define mbpl_lbl 40400 `"Norway"', add
label define mbpl_lbl 40500 `"Sweden"', add
label define mbpl_lbl 41000 `"England"', add
label define mbpl_lbl 41100 `"Scotland"', add
label define mbpl_lbl 41200 `"Wales"', add
label define mbpl_lbl 41300 `"United Kingdom, n.s."', add
label define mbpl_lbl 41400 `"Ireland"', add
label define mbpl_lbl 41410 `"Northern Ireland"', add
label define mbpl_lbl 42000 `"Belgium"', add
label define mbpl_lbl 42100 `"France"', add
label define mbpl_lbl 42500 `"Netherlands"', add
label define mbpl_lbl 42600 `"Switzerland"', add
label define mbpl_lbl 43300 `"Greece"', add
label define mbpl_lbl 43400 `"Italy"', add
label define mbpl_lbl 43600 `"Portugal"', add
label define mbpl_lbl 43610 `"Azores"', add
label define mbpl_lbl 43800 `"Spain"', add
label define mbpl_lbl 45000 `"Austria"', add
label define mbpl_lbl 45200 `"Czechoslavakia"', add
label define mbpl_lbl 45212 `"Slovakia"', add
label define mbpl_lbl 45213 `"Czech Republic"', add
label define mbpl_lbl 45300 `"Germany"', add
label define mbpl_lbl 45400 `"Hungary"', add
label define mbpl_lbl 45500 `"Poland"', add
label define mbpl_lbl 45600 `"Romania"', add
label define mbpl_lbl 45650 `"Bulgaria"', add
label define mbpl_lbl 45675 `"Albania"', add
label define mbpl_lbl 45700 `"Yugoslavia"', add
label define mbpl_lbl 45720 `"Bosnia and Herzegovina"', add
label define mbpl_lbl 45730 `"Croatia"', add
label define mbpl_lbl 45740 `"Macedonia"', add
label define mbpl_lbl 45750 `"Serbia"', add
label define mbpl_lbl 45760 `"Kosovo"', add
label define mbpl_lbl 45770 `"Montenego"', add
label define mbpl_lbl 46100 `"Estonia"', add
label define mbpl_lbl 46200 `"Latvia"', add
label define mbpl_lbl 46300 `"Lithuania"', add
label define mbpl_lbl 46500 `"Other USSR/Russia"', add
label define mbpl_lbl 46530 `"Ukraine"', add
label define mbpl_lbl 46535 `"Belarus"', add
label define mbpl_lbl 46540 `"Moldova"', add
label define mbpl_lbl 46590 `"USSR, n.s."', add
label define mbpl_lbl 49900 `"Europe, n.s."', add
label define mbpl_lbl 50000 `"China"', add
label define mbpl_lbl 50010 `"Hong Kong"', add
label define mbpl_lbl 50040 `"Taiwan"', add
label define mbpl_lbl 50100 `"Japan"', add
label define mbpl_lbl 50200 `"Korea"', add
label define mbpl_lbl 50220 `"South Korea"', add
label define mbpl_lbl 50300 `"Mongolia"', add
label define mbpl_lbl 51100 `"Cambodia"', add
label define mbpl_lbl 51200 `"Indonesia"', add
label define mbpl_lbl 51300 `"Laos"', add
label define mbpl_lbl 51400 `"Malaysia"', add
label define mbpl_lbl 51500 `"Philippines"', add
label define mbpl_lbl 51600 `"Singapore"', add
label define mbpl_lbl 51700 `"Thailand"', add
label define mbpl_lbl 51800 `"Vietnam"', add
label define mbpl_lbl 52000 `"Afghanistan"', add
label define mbpl_lbl 52100 `"India"', add
label define mbpl_lbl 52110 `"Bangladesh"', add
label define mbpl_lbl 52120 `"Bhutan"', add
label define mbpl_lbl 52130 `"Burma (Myanmar)"', add
label define mbpl_lbl 52140 `"Pakistan"', add
label define mbpl_lbl 52150 `"Sri Lanka"', add
label define mbpl_lbl 52200 `"Nepal"', add
label define mbpl_lbl 55100 `"Armenia"', add
label define mbpl_lbl 55200 `"Azerbaijan"', add
label define mbpl_lbl 55300 `"Georgia"', add
label define mbpl_lbl 55400 `"Uzbekistan"', add
label define mbpl_lbl 55500 `"Kazakhstan"', add
label define mbpl_lbl 53000 `"Iran"', add
label define mbpl_lbl 53200 `"Iraq"', add
label define mbpl_lbl 53400 `"Isreal/Palestine"', add
label define mbpl_lbl 53420 `"Palestine"', add
label define mbpl_lbl 53500 `"Jordan"', add
label define mbpl_lbl 53700 `"Lebanon"', add
label define mbpl_lbl 54000 `"Saudi Arabia"', add
label define mbpl_lbl 54100 `"Syria"', add
label define mbpl_lbl 54200 `"Turkey"', add
label define mbpl_lbl 54300 `"Cyprus"', add
label define mbpl_lbl 54350 `"Kuwait"', add
label define mbpl_lbl 54400 `"Yemen"', add
label define mbpl_lbl 54500 `"United Arab Emirates"', add
label define mbpl_lbl 54700 `"Middle East, n.s."', add
label define mbpl_lbl 59900 `"Asia, n.e.c, /n.s."', add
label define mbpl_lbl 60000 `"Africa"', add
label define mbpl_lbl 60010 `"Northern Africa"', add
label define mbpl_lbl 60012 `"Egypt/United Arab Rep."', add
label define mbpl_lbl 60014 `"Morocco"', add
label define mbpl_lbl 60016 `"Algeria"', add
label define mbpl_lbl 60018 `"Sudan"', add
label define mbpl_lbl 60019 `"Libya"', add
label define mbpl_lbl 60023 `"Ghana"', add
label define mbpl_lbl 60031 `"Nigeria"', add
label define mbpl_lbl 60032 `"Cameroon"', add
label define mbpl_lbl 60033 `"Cape Verde"', add
label define mbpl_lbl 60034 `"Liberia"', add
label define mbpl_lbl 60035 `"Sengal"', add
label define mbpl_lbl 60036 `"Sierra Leone"', add
label define mbpl_lbl 60037 `"Guinea"', add
label define mbpl_lbl 60038 `"Ivory Coast"', add
label define mbpl_lbl 60039 `"Togo"', add
label define mbpl_lbl 60040 `"Eritrea"', add
label define mbpl_lbl 60044 `"Ethiopia"', add
label define mbpl_lbl 60045 `"Kenya"', add
label define mbpl_lbl 60050 `"Somalia"', add
label define mbpl_lbl 60060 `"Tanzania"', add
label define mbpl_lbl 60065 `"Uganda"', add
label define mbpl_lbl 60070 `"Zimbabwe"', add
label define mbpl_lbl 60095 `"Zaire"', add
label define mbpl_lbl 60096 `"Congo"', add
label define mbpl_lbl 60097 `"Zambia"', add
label define mbpl_lbl 60094 `"South Africa (Union of)"', add
label define mbpl_lbl 70010 `"Australia"', add
label define mbpl_lbl 70020 `"New Zealand"', add
label define mbpl_lbl 71000 `"Pacific Islands"', add
label define mbpl_lbl 71021 `"Fiji"', add
label define mbpl_lbl 71022 `"Tonga"', add
label define mbpl_lbl 71023 `"Samoa"', add
label define mbpl_lbl 71024 `"Marshall Islands"', add
label define mbpl_lbl 72000 `"Micronesia"', add
label define mbpl_lbl 96000 `"Other, n.e.c. and unknown"', add
label define mbpl_lbl 99999 `"NIU"', add
label values mbpl mbpl_lbl

label define fbpl_lbl 09900 `"U.S., n.s."'
label define fbpl_lbl 10000 `"American Samoa"', add
label define fbpl_lbl 10500 `"Guam"', add
label define fbpl_lbl 10750 `"Northern Mariana Islands"', add
label define fbpl_lbl 11000 `"Puerto Rico"', add
label define fbpl_lbl 11500 `"U.S. Virgin Islands"', add
label define fbpl_lbl 12090 `"U.S. outlying areas, n.s."', add
label define fbpl_lbl 15000 `"Canada"', add
label define fbpl_lbl 16010 `"Bermuda"', add
label define fbpl_lbl 19900 `"North America, n.s."', add
label define fbpl_lbl 20000 `"Mexico"', add
label define fbpl_lbl 21010 `"Belize/British Honduras"', add
label define fbpl_lbl 21020 `"Costa Rica"', add
label define fbpl_lbl 21030 `"El Salvador"', add
label define fbpl_lbl 21040 `"Guatemala"', add
label define fbpl_lbl 21050 `"Honduras"', add
label define fbpl_lbl 21060 `"Nicaragua"', add
label define fbpl_lbl 21070 `"Panama"', add
label define fbpl_lbl 21090 `"Central America, n.s."', add
label define fbpl_lbl 25000 `"Cuba"', add
label define fbpl_lbl 26000 `"West Indies"', add
label define fbpl_lbl 26010 `"Dominican Republic"', add
label define fbpl_lbl 26020 `"Haiti"', add
label define fbpl_lbl 26030 `"Jamaica"', add
label define fbpl_lbl 26043 `"Bahamas"', add
label define fbpl_lbl 26044 `"Barbados"', add
label define fbpl_lbl 26054 `"Dominica"', add
label define fbpl_lbl 26055 `"Grenada"', add
label define fbpl_lbl 26060 `"Trinidad and Tobago"', add
label define fbpl_lbl 26065 `"Antigua and Barbuda"', add
label define fbpl_lbl 26070 `"St. Kitts--Nevis"', add
label define fbpl_lbl 26075 `"St. Lucia"', add
label define fbpl_lbl 26080 `"St. Vincent and the Grenadi"', add
label define fbpl_lbl 26091 `"Caribbean, n.s."', add
label define fbpl_lbl 30000 `"South America"', add
label define fbpl_lbl 30005 `"Argentina"', add
label define fbpl_lbl 30010 `"Bolivia"', add
label define fbpl_lbl 30015 `"Brazil"', add
label define fbpl_lbl 30020 `"Chile"', add
label define fbpl_lbl 30025 `"Colombia"', add
label define fbpl_lbl 30030 `"Ecuador"', add
label define fbpl_lbl 30040 `"Guyana/British Guiana"', add
label define fbpl_lbl 30050 `"Peru"', add
label define fbpl_lbl 30060 `"Uruguay"', add
label define fbpl_lbl 30065 `"Venezuala"', add
label define fbpl_lbl 30070 `"Paraguay"', add
label define fbpl_lbl 31000 `"Americas, n.s."', add
label define fbpl_lbl 40000 `"Denmark"', add
label define fbpl_lbl 40100 `"Finland"', add
label define fbpl_lbl 40200 `"Iceland"', add
label define fbpl_lbl 40400 `"Norway"', add
label define fbpl_lbl 40500 `"Sweden"', add
label define fbpl_lbl 41000 `"England"', add
label define fbpl_lbl 41100 `"Scotland"', add
label define fbpl_lbl 41200 `"Wales"', add
label define fbpl_lbl 41300 `"United Kingdom, n.s."', add
label define fbpl_lbl 41400 `"Ireland"', add
label define fbpl_lbl 41410 `"Northern Ireland"', add
label define fbpl_lbl 42000 `"Belgium"', add
label define fbpl_lbl 42100 `"France"', add
label define fbpl_lbl 42500 `"Netherlands"', add
label define fbpl_lbl 42600 `"Switzerland"', add
label define fbpl_lbl 43300 `"Greece"', add
label define fbpl_lbl 43400 `"Italy"', add
label define fbpl_lbl 43600 `"Portugal"', add
label define fbpl_lbl 43610 `"Azores"', add
label define fbpl_lbl 43800 `"Spain"', add
label define fbpl_lbl 45000 `"Austria"', add
label define fbpl_lbl 45200 `"Czechoslavakia"', add
label define fbpl_lbl 45212 `"Slovakia"', add
label define fbpl_lbl 45213 `"Czech Republic"', add
label define fbpl_lbl 45300 `"Germany"', add
label define fbpl_lbl 45400 `"Hungary"', add
label define fbpl_lbl 45500 `"Poland"', add
label define fbpl_lbl 45600 `"Romania"', add
label define fbpl_lbl 45650 `"Bulgaria"', add
label define fbpl_lbl 45675 `"Albania"', add
label define fbpl_lbl 45700 `"Yugoslavia"', add
label define fbpl_lbl 45720 `"Bosnia and Herzegovina"', add
label define fbpl_lbl 45730 `"Croatia"', add
label define fbpl_lbl 45740 `"Macedonia"', add
label define fbpl_lbl 45750 `"Serbia"', add
label define fbpl_lbl 45760 `"Kosovo"', add
label define fbpl_lbl 45770 `"Montenegro"', add
label define fbpl_lbl 46100 `"Estonia"', add
label define fbpl_lbl 46200 `"Latvia"', add
label define fbpl_lbl 46300 `"Lithuania"', add
label define fbpl_lbl 46500 `"Other USSR/Russia"', add
label define fbpl_lbl 46530 `"Ukraine"', add
label define fbpl_lbl 46535 `"Belarus"', add
label define fbpl_lbl 46540 `"Moldova"', add
label define fbpl_lbl 46590 `"USSR, n.s."', add
label define fbpl_lbl 49900 `"Europe, n.s."', add
label define fbpl_lbl 50000 `"China"', add
label define fbpl_lbl 50010 `"Hong Kong"', add
label define fbpl_lbl 50040 `"Taiwan"', add
label define fbpl_lbl 50100 `"Japan"', add
label define fbpl_lbl 50200 `"Korea"', add
label define fbpl_lbl 50220 `"South Korea"', add
label define fbpl_lbl 50300 `"Mongolia"', add
label define fbpl_lbl 51100 `"Cambodia"', add
label define fbpl_lbl 51200 `"Indonesia"', add
label define fbpl_lbl 51300 `"Laos"', add
label define fbpl_lbl 51400 `"Malaysia"', add
label define fbpl_lbl 51500 `"Philippines"', add
label define fbpl_lbl 51600 `"Singapore"', add
label define fbpl_lbl 51700 `"Thailand"', add
label define fbpl_lbl 51800 `"Vietnam"', add
label define fbpl_lbl 52000 `"Afghanistan"', add
label define fbpl_lbl 52100 `"India"', add
label define fbpl_lbl 52110 `"Bangladesh"', add
label define fbpl_lbl 52120 `"Bhutan"', add
label define fbpl_lbl 52130 `"Burma (Myanmar)"', add
label define fbpl_lbl 52140 `"Pakistan"', add
label define fbpl_lbl 52150 `"Sri Lanka"', add
label define fbpl_lbl 52200 `"Nepal"', add
label define fbpl_lbl 55100 `"Armenia"', add
label define fbpl_lbl 55200 `"Azerbaijan"', add
label define fbpl_lbl 55300 `"Georgia"', add
label define fbpl_lbl 55400 `"Uzbekistan"', add
label define fbpl_lbl 55500 `"Kazakhstan"', add
label define fbpl_lbl 53000 `"Iran"', add
label define fbpl_lbl 53200 `"Iraq"', add
label define fbpl_lbl 53400 `"Isreal/Palestine"', add
label define fbpl_lbl 53420 `"Palestine"', add
label define fbpl_lbl 53500 `"Jordan"', add
label define fbpl_lbl 53700 `"Lebanon"', add
label define fbpl_lbl 54000 `"Saudi Arabia"', add
label define fbpl_lbl 54100 `"Syria"', add
label define fbpl_lbl 54200 `"Turkey"', add
label define fbpl_lbl 54300 `"Cyprus"', add
label define fbpl_lbl 54350 `"Kuwait"', add
label define fbpl_lbl 54400 `"Yemen"', add
label define fbpl_lbl 54500 `"United Arab Emirates"', add
label define fbpl_lbl 54700 `"Middle East, n.s."', add
label define fbpl_lbl 59900 `"Asia, n.e.c, /n.s."', add
label define fbpl_lbl 60000 `"Africa"', add
label define fbpl_lbl 60010 `"Northern Africa"', add
label define fbpl_lbl 60012 `"Egypt/United Arab Rep."', add
label define fbpl_lbl 60014 `"Morocco"', add
label define fbpl_lbl 60016 `"Algeria"', add
label define fbpl_lbl 60018 `"Sudan"', add
label define fbpl_lbl 60019 `"Libya"', add
label define fbpl_lbl 60023 `"Ghana"', add
label define fbpl_lbl 60031 `"Nigeria"', add
label define fbpl_lbl 60032 `"Cameroon"', add
label define fbpl_lbl 60033 `"Cape Verde"', add
label define fbpl_lbl 60034 `"Liberia"', add
label define fbpl_lbl 60035 `"Sengal"', add
label define fbpl_lbl 60036 `"Sierra Leone"', add
label define fbpl_lbl 60037 `"Guinea"', add
label define fbpl_lbl 60038 `"Ivory Coast"', add
label define fbpl_lbl 60039 `"Togo"', add
label define fbpl_lbl 60040 `"Eritrea"', add
label define fbpl_lbl 60044 `"Ethiopia"', add
label define fbpl_lbl 60045 `"Kenya"', add
label define fbpl_lbl 60050 `"Somalia"', add
label define fbpl_lbl 60060 `"Tanzania"', add
label define fbpl_lbl 60065 `"Uganda"', add
label define fbpl_lbl 60070 `"Zimbabwe"', add
label define fbpl_lbl 60094 `"South Africa (Union of)"', add
label define fbpl_lbl 60095 `"Zaire"', add
label define fbpl_lbl 60096 `"Congo"', add
label define fbpl_lbl 60097 `"Zambia"', add
label define fbpl_lbl 60099 `" Africa, n.s./n.e.c."', add
label define fbpl_lbl 70010 `"Australia"', add
label define fbpl_lbl 70020 `"New Zealand"', add
label define fbpl_lbl 71000 `"Pacific Islands"', add
label define fbpl_lbl 71021 `"Fiji"', add
label define fbpl_lbl 71022 `"Tonga"', add
label define fbpl_lbl 71023 `"Samoa"', add
label define fbpl_lbl 71024 `"Marshall Islands"', add
label define fbpl_lbl 72000 `"Micronesia"', add
label define fbpl_lbl 96000 `"Other, n.e.c. and unknown"', add
label define fbpl_lbl 99999 `"NIU"', add
label values fbpl fbpl_lbl

label define nativity_lbl 0 `"Unknown"'
label define nativity_lbl 1 `"Both parents native-born"', add
label define nativity_lbl 2 `"Father foreign, mother native"', add
label define nativity_lbl 3 `"Mother foreign, father native"', add
label define nativity_lbl 4 `"Both parents foreign"', add
label define nativity_lbl 5 `"Foreign born"', add
label values nativity nativity_lbl

label define hispan_lbl 000 `"Not Hispanic"'
label define hispan_lbl 100 `"Mexican"', add
label define hispan_lbl 102 `"Mexican American"', add
label define hispan_lbl 103 `"Mexicano/Mexicana"', add
label define hispan_lbl 104 `"Chicano/Chicana"', add
label define hispan_lbl 108 `"Mexican (Mexicano)"', add
label define hispan_lbl 109 `"Mexicano/Chicano"', add
label define hispan_lbl 200 `"Puerto Rican"', add
label define hispan_lbl 300 `"Cuban"', add
label define hispan_lbl 400 `"Dominican"', add
label define hispan_lbl 500 `"Salvadoran"', add
label define hispan_lbl 401 `"Other Hispanic"', add
label define hispan_lbl 410 `"Central/South American"', add
label define hispan_lbl 411 `"Central American, (excluding Salvadoran)"', add
label define hispan_lbl 412 `"South American"', add
label define hispan_lbl 901 `"Do not know"', add
label define hispan_lbl 902 `"N/A (and no response 1985-87)"', add
label values hispan hispan_lbl

label define educ_lbl 000 `"NIU or no schooling"'
label define educ_lbl 001 `"NIU or blank"', add
label define educ_lbl 002 `"None or preschool"', add
label define educ_lbl 010 `"Grades 1, 2, 3, or 4"', add
label define educ_lbl 011 `"Grade 1"', add
label define educ_lbl 012 `"Grade 2"', add
label define educ_lbl 013 `"Grade 3"', add
label define educ_lbl 014 `"Grade 4"', add
label define educ_lbl 020 `"Grades 5 or 6"', add
label define educ_lbl 021 `"Grade 5"', add
label define educ_lbl 022 `"Grade 6"', add
label define educ_lbl 030 `"Grades 7 or 8"', add
label define educ_lbl 031 `"Grade 7"', add
label define educ_lbl 032 `"Grade 8"', add
label define educ_lbl 040 `"Grade 9"', add
label define educ_lbl 050 `"Grade 10"', add
label define educ_lbl 060 `"Grade 11"', add
label define educ_lbl 070 `"Grade 12"', add
label define educ_lbl 071 `"12th grade, no diploma"', add
label define educ_lbl 072 `"12th grade, diploma unclear"', add
label define educ_lbl 073 `"High school diploma or equivalent"', add
label define educ_lbl 080 `"1 year of college"', add
label define educ_lbl 081 `"Some college but no degree"', add
label define educ_lbl 090 `"2 years of college"', add
label define educ_lbl 091 `"Associate's degree, occupational/vocational program"', add
label define educ_lbl 092 `"Associate's degree, academic program"', add
label define educ_lbl 100 `"3 years of college"', add
label define educ_lbl 110 `"4 years of college"', add
label define educ_lbl 111 `"Bachelor's degree"', add
label define educ_lbl 120 `"5+ years of college"', add
label define educ_lbl 121 `"5 years of college"', add
label define educ_lbl 122 `"6+ years of college"', add
label define educ_lbl 123 `"Master's degree"', add
label define educ_lbl 124 `"Professional school degree"', add
label define educ_lbl 125 `"Doctorate degree"', add
label define educ_lbl 999 `"Missing/Unknown"', add
label values educ educ_lbl

label define educ99_lbl 00 `"NIU"'
label define educ99_lbl 01 `"No school completed"', add
label define educ99_lbl 04 `"1st-4th grade"', add
label define educ99_lbl 05 `"5th-8th grade"', add
label define educ99_lbl 06 `"9th grade"', add
label define educ99_lbl 07 `"10th grade"', add
label define educ99_lbl 08 `"11th grade"', add
label define educ99_lbl 09 `"12th grade, no diploma"', add
label define educ99_lbl 10 `"High school graduate, or GED"', add
label define educ99_lbl 11 `"Some college, no degree"', add
label define educ99_lbl 12 `"Associate degree, type of program not specified"', add
label define educ99_lbl 13 `"Associate degree, occupational program"', add
label define educ99_lbl 14 `"Associate degree, academic program"', add
label define educ99_lbl 15 `"Bachelors degree"', add
label define educ99_lbl 16 `"Masters degree"', add
label define educ99_lbl 17 `"Professional degree"', add
label define educ99_lbl 18 `"Doctorate degree"', add
label values educ99 educ99_lbl

label define schlcoll_lbl 0 `"NIU"'
label define schlcoll_lbl 1 `"High school full time"', add
label define schlcoll_lbl 2 `"High school part time"', add
label define schlcoll_lbl 3 `"College or university full time"', add
label define schlcoll_lbl 4 `"College or university part time"', add
label define schlcoll_lbl 5 `"Does not attend school, college or university"', add
label values schlcoll schlcoll_lbl

label define empstat_lbl 00 `"NIU"'
label define empstat_lbl 01 `"Armed Forces"', add
label define empstat_lbl 10 `"At work"', add
label define empstat_lbl 12 `"Has job, not at work last week"', add
label define empstat_lbl 20 `"Unemployed"', add
label define empstat_lbl 21 `"Unemployed, experienced worker"', add
label define empstat_lbl 22 `"Unemployed, new worker"', add
label define empstat_lbl 30 `"Not in labor force"', add
label define empstat_lbl 31 `"NILF, housework"', add
label define empstat_lbl 32 `"NILF, unable to work"', add
label define empstat_lbl 33 `"NILF, school"', add
label define empstat_lbl 34 `"NILF, other"', add
label define empstat_lbl 35 `"NILF, unpaid, lt 15 hours"', add
label values empstat empstat_lbl

label define labforce_lbl 0 `"NIU"'
label define labforce_lbl 1 `"No, not in the labor force"', add
label define labforce_lbl 2 `"Yes, in the labor force"', add
label values labforce labforce_lbl

label define occ1990_lbl 003 `"Legislators"'
label define occ1990_lbl 004 `"Chief executives and public administrators"', add
label define occ1990_lbl 007 `"Financial managers"', add
label define occ1990_lbl 008 `"Human resources and labor relations managers"', add
label define occ1990_lbl 013 `"Managers and specialists in marketing, advertising, and public relations"', add
label define occ1990_lbl 014 `"Managers in education and related fields"', add
label define occ1990_lbl 015 `"Managers of medicine and health occupations"', add
label define occ1990_lbl 016 `"Postmasters and mail superintendents"', add
label define occ1990_lbl 017 `"Managers of food-serving and lodging establishments"', add
label define occ1990_lbl 018 `"Managers of properties and real estate"', add
label define occ1990_lbl 019 `"Funeral directors"', add
label define occ1990_lbl 021 `"Managers of service organizations, n.e.c."', add
label define occ1990_lbl 022 `"Managers and administrators, n.e.c."', add
label define occ1990_lbl 023 `"Accountants and auditors"', add
label define occ1990_lbl 024 `"Insurance underwriters"', add
label define occ1990_lbl 025 `"Other financial specialists"', add
label define occ1990_lbl 026 `"Management analysts"', add
label define occ1990_lbl 027 `"Personnel, HR, training, and labor relations specialists"', add
label define occ1990_lbl 028 `"Purchasing agents and buyers, of farm products"', add
label define occ1990_lbl 029 `"Buyers, wholesale and retail trade"', add
label define occ1990_lbl 033 `"Purchasing managers, agents and buyers, n.e.c."', add
label define occ1990_lbl 034 `"Business and promotion agents"', add
label define occ1990_lbl 035 `"Construction inspectors"', add
label define occ1990_lbl 036 `"Inspectors and compliance officers, outside construction"', add
label define occ1990_lbl 037 `"Management support occupations"', add
label define occ1990_lbl 043 `"Architects"', add
label define occ1990_lbl 044 `"Aerospace engineer"', add
label define occ1990_lbl 045 `"Metallurgical and materials engineers, variously phrased"', add
label define occ1990_lbl 047 `"Petroleum, mining, and geological engineers"', add
label define occ1990_lbl 048 `"Chemical engineers"', add
label define occ1990_lbl 053 `"Civil engineers"', add
label define occ1990_lbl 055 `"Electrical engineer"', add
label define occ1990_lbl 056 `"Industrial engineers"', add
label define occ1990_lbl 057 `"Mechanical engineers"', add
label define occ1990_lbl 059 `"Not-elsewhere-classified engineers"', add
label define occ1990_lbl 064 `"Computer systems analysts and computer scientists"', add
label define occ1990_lbl 065 `"Operations and systems researchers and analysts"', add
label define occ1990_lbl 066 `"Actuaries"', add
label define occ1990_lbl 067 `"Statisticians"', add
label define occ1990_lbl 068 `"Mathematicians and mathematical scientists"', add
label define occ1990_lbl 069 `"Physicists and astronomers"', add
label define occ1990_lbl 073 `"Chemists"', add
label define occ1990_lbl 074 `"Atmospheric and space scientists"', add
label define occ1990_lbl 075 `"Geologists"', add
label define occ1990_lbl 076 `"Physical scientists, n.e.c."', add
label define occ1990_lbl 077 `"Agricultural and food scientists"', add
label define occ1990_lbl 078 `"Biological scientists"', add
label define occ1990_lbl 079 `"Foresters and conservation scientists"', add
label define occ1990_lbl 083 `"Medical scientists"', add
label define occ1990_lbl 084 `"Physicians"', add
label define occ1990_lbl 085 `"Dentists"', add
label define occ1990_lbl 086 `"Veterinarians"', add
label define occ1990_lbl 087 `"Optometrists"', add
label define occ1990_lbl 088 `"Podiatrists"', add
label define occ1990_lbl 089 `"Other health and therapy"', add
label define occ1990_lbl 095 `"Registered nurses"', add
label define occ1990_lbl 096 `"Pharmacists"', add
label define occ1990_lbl 097 `"Dietitians and nutritionists"', add
label define occ1990_lbl 098 `"Respiratory therapists"', add
label define occ1990_lbl 099 `"Occupational therapists"', add
label define occ1990_lbl 103 `"Physical therapists"', add
label define occ1990_lbl 104 `"Speech therapists"', add
label define occ1990_lbl 105 `"Therapists, n.e.c."', add
label define occ1990_lbl 106 `"Physicians' assistants"', add
label define occ1990_lbl 113 `"Earth, environmental, and marine science instructors"', add
label define occ1990_lbl 114 `"Biological science instructors"', add
label define occ1990_lbl 115 `"Chemistry instructors"', add
label define occ1990_lbl 116 `"Physics instructors"', add
label define occ1990_lbl 118 `"Psychology instructors"', add
label define occ1990_lbl 119 `"Economics instructors"', add
label define occ1990_lbl 123 `"History instructors"', add
label define occ1990_lbl 125 `"Sociology instructors"', add
label define occ1990_lbl 127 `"Engineering instructors"', add
label define occ1990_lbl 128 `"Math instructors"', add
label define occ1990_lbl 139 `"Education instructors"', add
label define occ1990_lbl 145 `"Law instructors"', add
label define occ1990_lbl 147 `"Theology instructors"', add
label define occ1990_lbl 149 `"Home economics instructors"', add
label define occ1990_lbl 150 `"Humanities profs/instructors, college, nec"', add
label define occ1990_lbl 154 `"Subject instructors (HS/college)"', add
label define occ1990_lbl 155 `"Kindergarten and earlier school teachers"', add
label define occ1990_lbl 156 `"Primary school teachers"', add
label define occ1990_lbl 157 `"Secondary school teachers"', add
label define occ1990_lbl 158 `"Special education teachers"', add
label define occ1990_lbl 159 `"Teachers , n.e.c."', add
label define occ1990_lbl 163 `"Vocational and educational counselors"', add
label define occ1990_lbl 164 `"Librarians"', add
label define occ1990_lbl 165 `"Archivists and curators"', add
label define occ1990_lbl 166 `"Economists, market researchers, and survey researchers"', add
label define occ1990_lbl 167 `"Psychologists"', add
label define occ1990_lbl 168 `"Sociologists"', add
label define occ1990_lbl 169 `"Social scientists, n.e.c."', add
label define occ1990_lbl 173 `"Urban and regional planners"', add
label define occ1990_lbl 174 `"Social workers"', add
label define occ1990_lbl 175 `"Recreation workers"', add
label define occ1990_lbl 176 `"Clergy and religious workers"', add
label define occ1990_lbl 178 `"Lawyers "', add
label define occ1990_lbl 179 `"Judges"', add
label define occ1990_lbl 183 `"Writers and authors"', add
label define occ1990_lbl 184 `"Technical writers"', add
label define occ1990_lbl 185 `"Designers"', add
label define occ1990_lbl 186 `"Musician or composer"', add
label define occ1990_lbl 187 `"Actors, directors, producers"', add
label define occ1990_lbl 188 `"Art makers: painters, sculptors, craft-artists, and print-makers"', add
label define occ1990_lbl 189 `"Photographers"', add
label define occ1990_lbl 193 `"Dancers"', add
label define occ1990_lbl 194 `"Art/entertainment performers and related"', add
label define occ1990_lbl 195 `"Editors and reporters"', add
label define occ1990_lbl 198 `"Announcers"', add
label define occ1990_lbl 199 `"Athletes, sports instructors, and officials"', add
label define occ1990_lbl 200 `"Professionals, n.e.c."', add
label define occ1990_lbl 203 `"Clinical laboratory technologies and technicians"', add
label define occ1990_lbl 204 `"Dental hygenists"', add
label define occ1990_lbl 205 `"Health record tech specialists"', add
label define occ1990_lbl 206 `"Radiologic tech specialists"', add
label define occ1990_lbl 207 `"Licensed practical nurses"', add
label define occ1990_lbl 208 `"Health technologists and technicians, n.e.c."', add
label define occ1990_lbl 213 `"Electrical and electronic (engineering) technicians"', add
label define occ1990_lbl 214 `"Engineering technicians, n.e.c."', add
label define occ1990_lbl 215 `"Mechanical engineering technicians"', add
label define occ1990_lbl 217 `"Drafters"', add
label define occ1990_lbl 218 `"Surveyors, cartographers, mapping scientists and technicians"', add
label define occ1990_lbl 223 `"Biological technicians"', add
label define occ1990_lbl 224 `"Chemical technicians"', add
label define occ1990_lbl 225 `"Other science technicians"', add
label define occ1990_lbl 226 `"Airplane pilots and navigators"', add
label define occ1990_lbl 227 `"Air traffic controllers"', add
label define occ1990_lbl 228 `"Broadcast equipment operators"', add
label define occ1990_lbl 229 `"Computer software developers"', add
label define occ1990_lbl 233 `"Programmers of numerically controlled machine tools"', add
label define occ1990_lbl 234 `"Legal assistants, paralegals, legal support, etc"', add
label define occ1990_lbl 235 `"Technicians, n.e.c."', add
label define occ1990_lbl 243 `"Supervisors and proprietors of sales jobs"', add
label define occ1990_lbl 253 `"Insurance sales occupations"', add
label define occ1990_lbl 254 `"Real estate sales occupations"', add
label define occ1990_lbl 255 `"Financial services sales occupations"', add
label define occ1990_lbl 256 `"Advertising and related sales jobs"', add
label define occ1990_lbl 258 `"Sales engineers"', add
label define occ1990_lbl 274 `"Salespersons, n.e.c."', add
label define occ1990_lbl 275 `"Retail sales clerks"', add
label define occ1990_lbl 276 `"Cashiers"', add
label define occ1990_lbl 277 `"Door-to-door sales, street sales, and news vendors"', add
label define occ1990_lbl 283 `"Sales demonstrators / promoters / models"', add
label define occ1990_lbl 290 `"Sales workers--allocated (1990 internal census)"', add
label define occ1990_lbl 303 `"Office supervisors"', add
label define occ1990_lbl 308 `"Computer and peripheral equipment operators"', add
label define occ1990_lbl 313 `"Secretaries"', add
label define occ1990_lbl 314 `"Stenographers"', add
label define occ1990_lbl 315 `"Typists"', add
label define occ1990_lbl 316 `"Interviewers, enumerators, and surveyors"', add
label define occ1990_lbl 317 `"Hotel clerks"', add
label define occ1990_lbl 318 `"Transportation ticket and reservation agents"', add
label define occ1990_lbl 319 `"Receptionists"', add
label define occ1990_lbl 323 `"Information clerks, nec"', add
label define occ1990_lbl 326 `"Correspondence and order clerks"', add
label define occ1990_lbl 328 `"Human resources clerks, except payroll and timekeeping"', add
label define occ1990_lbl 329 `"Library assistants"', add
label define occ1990_lbl 335 `"File clerks"', add
label define occ1990_lbl 336 `"Records clerks"', add
label define occ1990_lbl 337 `"Bookkeepers and accounting and auditing clerks"', add
label define occ1990_lbl 338 `"Payroll and timekeeping clerks"', add
label define occ1990_lbl 343 `"Cost and rate clerks (financial records processing)"', add
label define occ1990_lbl 344 `"Billing clerks and related financial records processing"', add
label define occ1990_lbl 345 `"Duplication machine operators / office machine operators"', add
label define occ1990_lbl 346 `"Mail and paper handlers"', add
label define occ1990_lbl 347 `"Office machine operators, n.e.c."', add
label define occ1990_lbl 348 `"Telephone operators"', add
label define occ1990_lbl 349 `"Other telecom operators"', add
label define occ1990_lbl 354 `"Postal clerks, excluding mail carriers"', add
label define occ1990_lbl 355 `"Mail carriers for postal service"', add
label define occ1990_lbl 356 `"Mail clerks, outside of post office"', add
label define occ1990_lbl 357 `"Messengers"', add
label define occ1990_lbl 359 `"Dispatchers"', add
label define occ1990_lbl 361 `"Inspectors, n.e.c."', add
label define occ1990_lbl 364 `"Shipping and receiving clerks"', add
label define occ1990_lbl 365 `"Stock and inventory clerks"', add
label define occ1990_lbl 366 `"Meter readers"', add
label define occ1990_lbl 368 `"Weighers, measurers, and checkers"', add
label define occ1990_lbl 373 `"Material recording, scheduling, production, planning, and expediting clerks"', add
label define occ1990_lbl 375 `"Insurance adjusters, examiners, and investigators"', add
label define occ1990_lbl 376 `"Customer service reps, investigators and adjusters, except insurance"', add
label define occ1990_lbl 377 `"Eligibility clerks for government programs; social welfare"', add
label define occ1990_lbl 378 `"Bill and account collectors"', add
label define occ1990_lbl 379 `"General office clerks"', add
label define occ1990_lbl 383 `"Bank tellers"', add
label define occ1990_lbl 384 `"Proofreaders"', add
label define occ1990_lbl 385 `"Data entry keyers"', add
label define occ1990_lbl 386 `"Statistical clerks"', add
label define occ1990_lbl 387 `"Teacher's aides"', add
label define occ1990_lbl 389 `"Administrative support jobs, n.e.c."', add
label define occ1990_lbl 390 `"Professional, technical, and kindred workers--allocated (1990 internal census)"', add
label define occ1990_lbl 391 `"Clerical and kindred workers--allocated (1990 internal census)"', add
label define occ1990_lbl 405 `"Housekeepers, maids, butlers, stewards, and lodging quarters cleaners"', add
label define occ1990_lbl 407 `"Private household cleaners and servants"', add
label define occ1990_lbl 408 `"Private household workers--allocated (1990 internal census)"', add
label define occ1990_lbl 415 `"Supervisors of guards"', add
label define occ1990_lbl 417 `"Fire fighting, prevention, and inspection"', add
label define occ1990_lbl 418 `"Police, detectives, and private investigators"', add
label define occ1990_lbl 423 `"Other law enforcement: sheriffs, bailiffs, correctional institution officers"', add
label define occ1990_lbl 425 `"Crossing guards and bridge tenders"', add
label define occ1990_lbl 426 `"Guards, watchmen, doorkeepers"', add
label define occ1990_lbl 427 `"Protective services, n.e.c."', add
label define occ1990_lbl 434 `"Bartenders"', add
label define occ1990_lbl 435 `"Waiter/waitress"', add
label define occ1990_lbl 436 `"Cooks, variously defined"', add
label define occ1990_lbl 438 `"Food counter and fountain workers"', add
label define occ1990_lbl 439 `"Kitchen workers"', add
label define occ1990_lbl 443 `"Waiter's assistant"', add
label define occ1990_lbl 444 `"Misc food prep workers"', add
label define occ1990_lbl 445 `"Dental assistants"', add
label define occ1990_lbl 446 `"Health aides, except nursing"', add
label define occ1990_lbl 447 `"Nursing aides, orderlies, and attendants"', add
label define occ1990_lbl 448 `"Supervisors of cleaning and building service"', add
label define occ1990_lbl 453 `"Janitors"', add
label define occ1990_lbl 454 `"Elevator operators"', add
label define occ1990_lbl 455 `"Pest control occupations"', add
label define occ1990_lbl 456 `"Supervisors of personal service jobs, n.e.c."', add
label define occ1990_lbl 457 `"Barbers"', add
label define occ1990_lbl 458 `"Hairdressers and cosmetologists"', add
label define occ1990_lbl 459 `"Recreation facility attendants"', add
label define occ1990_lbl 461 `"Guides"', add
label define occ1990_lbl 462 `"Ushers"', add
label define occ1990_lbl 463 `"Public transportation attendants and inspectors"', add
label define occ1990_lbl 464 `"Baggage porters"', add
label define occ1990_lbl 465 `"Welfare service aides"', add
label define occ1990_lbl 468 `"Child care workers"', add
label define occ1990_lbl 469 `"Personal service occupations, nec"', add
label define occ1990_lbl 473 `"Farmers (owners and tenants)"', add
label define occ1990_lbl 474 `"Horticultural specialty farmers"', add
label define occ1990_lbl 475 `"Farm managers, except for horticultural farms"', add
label define occ1990_lbl 476 `"Managers of horticultural specialty farms"', add
label define occ1990_lbl 479 `"Farm workers"', add
label define occ1990_lbl 480 `"Farm laborers and farm foreman--allocated (1990 internal census)"', add
label define occ1990_lbl 483 `"Marine life cultivation workers"', add
label define occ1990_lbl 484 `"Nursery farming workers"', add
label define occ1990_lbl 485 `"Supervisors of agricultural occupations"', add
label define occ1990_lbl 486 `"Gardeners and groundskeepers"', add
label define occ1990_lbl 487 `"Animal caretakers except on farms"', add
label define occ1990_lbl 488 `"Graders and sorters of agricultural products"', add
label define occ1990_lbl 489 `"Inspectors of agricultural products"', add
label define occ1990_lbl 496 `"Timber, logging, and forestry workers"', add
label define occ1990_lbl 498 `"Fishers, hunters, and kindred"', add
label define occ1990_lbl 503 `"Supervisors of mechanics and repairers"', add
label define occ1990_lbl 505 `"Automobile mechanics"', add
label define occ1990_lbl 507 `"Bus, truck, and stationary engine mechanics"', add
label define occ1990_lbl 508 `"Aircraft mechanics"', add
label define occ1990_lbl 509 `"Small engine repairers"', add
label define occ1990_lbl 514 `"Auto body repairers"', add
label define occ1990_lbl 516 `"Heavy equipment and farm equipment mechanics"', add
label define occ1990_lbl 518 `"Industrial machinery repairers"', add
label define occ1990_lbl 519 `"Machinery maintenance occupations"', add
label define occ1990_lbl 523 `"Repairers of industrial electrical equipment "', add
label define occ1990_lbl 525 `"Repairers of data processing equipment"', add
label define occ1990_lbl 526 `"Repairers of household appliances and power tools"', add
label define occ1990_lbl 527 `"Telecom and line installers and repairers"', add
label define occ1990_lbl 533 `"Repairers of electrical equipment, n.e.c."', add
label define occ1990_lbl 534 `"Heating, air conditioning, and refigeration mechanics"', add
label define occ1990_lbl 535 `"Precision makers, repairers, and smiths"', add
label define occ1990_lbl 536 `"Locksmiths and safe repairers"', add
label define occ1990_lbl 538 `"Office machine repairers and mechanics"', add
label define occ1990_lbl 539 `"Repairers of mechanical controls and valves"', add
label define occ1990_lbl 543 `"Elevator installers and repairers"', add
label define occ1990_lbl 544 `"Millwrights"', add
label define occ1990_lbl 549 `"Mechanics and repairers, n.e.c."', add
label define occ1990_lbl 558 `"Supervisors of construction work"', add
label define occ1990_lbl 563 `"Masons, tilers, and carpet installers"', add
label define occ1990_lbl 567 `"Carpenters"', add
label define occ1990_lbl 573 `"Drywall installers"', add
label define occ1990_lbl 575 `"Electricians"', add
label define occ1990_lbl 577 `"Electric power installers and repairers"', add
label define occ1990_lbl 579 `"Painters, construction and maintenance"', add
label define occ1990_lbl 583 `"Paperhangers"', add
label define occ1990_lbl 584 `"Plasterers"', add
label define occ1990_lbl 585 `"Plumbers, pipe fitters, and steamfitters"', add
label define occ1990_lbl 588 `"Concrete and cement workers"', add
label define occ1990_lbl 589 `"Glaziers"', add
label define occ1990_lbl 593 `"Insulation workers"', add
label define occ1990_lbl 594 `"Paving, surfacing, and tamping equipment operators"', add
label define occ1990_lbl 595 `"Roofers and slaters"', add
label define occ1990_lbl 596 `"Sheet metal duct installers"', add
label define occ1990_lbl 597 `"Structural metal workers"', add
label define occ1990_lbl 598 `"Drillers of earth"', add
label define occ1990_lbl 599 `"Construction trades, n.e.c."', add
label define occ1990_lbl 614 `"Drillers of oil wells"', add
label define occ1990_lbl 615 `"Explosives workers"', add
label define occ1990_lbl 616 `"Miners"', add
label define occ1990_lbl 617 `"Other mining occupations"', add
label define occ1990_lbl 628 `"Production supervisors or foremen"', add
label define occ1990_lbl 634 `"Tool and die makers and die setters"', add
label define occ1990_lbl 637 `"Machinists"', add
label define occ1990_lbl 643 `"Boilermakers"', add
label define occ1990_lbl 644 `"Precision grinders and filers"', add
label define occ1990_lbl 645 `"Patternmakers and model makers"', add
label define occ1990_lbl 646 `"Lay-out workers"', add
label define occ1990_lbl 649 `"Engravers"', add
label define occ1990_lbl 653 `"Tinsmiths, coppersmiths, and sheet metal workers"', add
label define occ1990_lbl 657 `"Cabinetmakers and bench carpenters"', add
label define occ1990_lbl 658 `"Furniture and wood finishers"', add
label define occ1990_lbl 659 `"Other precision woodworkers"', add
label define occ1990_lbl 666 `"Dressmakers and seamstresses"', add
label define occ1990_lbl 667 `"Tailors"', add
label define occ1990_lbl 668 `"Upholsterers"', add
label define occ1990_lbl 669 `"Shoe repairers"', add
label define occ1990_lbl 674 `"Other precision apparel and fabric workers"', add
label define occ1990_lbl 675 `"Hand molders and shapers, except jewelers "', add
label define occ1990_lbl 677 `"Optical goods workers"', add
label define occ1990_lbl 678 `"Dental laboratory and medical appliance technicians"', add
label define occ1990_lbl 679 `"Bookbinders"', add
label define occ1990_lbl 684 `"Other precision and craft workers"', add
label define occ1990_lbl 686 `"Butchers and meat cutters"', add
label define occ1990_lbl 687 `"Bakers"', add
label define occ1990_lbl 688 `"Batch food makers"', add
label define occ1990_lbl 693 `"Adjusters and calibrators"', add
label define occ1990_lbl 694 `"Water and sewage treatment plant operators"', add
label define occ1990_lbl 695 `"Power plant operators"', add
label define occ1990_lbl 696 `"Plant and system operators, stationary engineers "', add
label define occ1990_lbl 699 `"Other plant and system operators"', add
label define occ1990_lbl 703 `"Lathe, milling, and turning machine operatives"', add
label define occ1990_lbl 706 `"Punching and stamping press operatives"', add
label define occ1990_lbl 707 `"Rollers, roll hands, and finishers of metal"', add
label define occ1990_lbl 708 `"Drilling and boring machine operators"', add
label define occ1990_lbl 709 `"Grinding, abrading, buffing, and polishing workers"', add
label define occ1990_lbl 713 `"Forge and hammer operators"', add
label define occ1990_lbl 717 `"Fabricating machine operators, n.e.c."', add
label define occ1990_lbl 719 `"Molders, and casting machine operators"', add
label define occ1990_lbl 723 `"Metal platers"', add
label define occ1990_lbl 724 `"Heat treating equipment operators"', add
label define occ1990_lbl 726 `"Wood lathe, routing, and planing machine operators"', add
label define occ1990_lbl 727 `"Sawing machine operators and sawyers"', add
label define occ1990_lbl 728 `"Shaping and joining machine operator (woodworking)"', add
label define occ1990_lbl 729 `"Nail and tacking machine operators  (woodworking)"', add
label define occ1990_lbl 733 `"Other woodworking machine operators"', add
label define occ1990_lbl 734 `"Printing machine operators, n.e.c."', add
label define occ1990_lbl 735 `"Photoengravers and lithographers"', add
label define occ1990_lbl 736 `"Typesetters and compositors"', add
label define occ1990_lbl 738 `"Winding and twisting textile/apparel operatives"', add
label define occ1990_lbl 739 `"Knitters, loopers, and toppers textile operatives"', add
label define occ1990_lbl 743 `"Textile cutting machine operators"', add
label define occ1990_lbl 744 `"Textile sewing machine operators"', add
label define occ1990_lbl 745 `"Shoemaking machine operators"', add
label define occ1990_lbl 747 `"Pressing machine operators (clothing)"', add
label define occ1990_lbl 748 `"Laundry workers"', add
label define occ1990_lbl 749 `"Misc textile machine operators"', add
label define occ1990_lbl 753 `"Cementing and gluing maching operators"', add
label define occ1990_lbl 754 `"Packers, fillers, and wrappers"', add
label define occ1990_lbl 755 `"Extruding and forming machine operators"', add
label define occ1990_lbl 756 `"Mixing and blending machine operatives"', add
label define occ1990_lbl 757 `"Separating, filtering, and clarifying machine operators"', add
label define occ1990_lbl 759 `"Painting machine operators"', add
label define occ1990_lbl 763 `"Roasting and baking machine operators (food)"', add
label define occ1990_lbl 764 `"Washing, cleaning, and pickling machine operators"', add
label define occ1990_lbl 765 `"Paper folding machine operators"', add
label define occ1990_lbl 766 `"Furnace, kiln, and oven operators, apart from food"', add
label define occ1990_lbl 768 `"Crushing and grinding machine operators"', add
label define occ1990_lbl 769 `"Slicing and cutting machine operators"', add
label define occ1990_lbl 773 `"Motion picture projectionists"', add
label define occ1990_lbl 774 `"Photographic process workers"', add
label define occ1990_lbl 779 `"Machine operators, n.e.c."', add
label define occ1990_lbl 783 `"Welders and metal cutters"', add
label define occ1990_lbl 784 `"Solderers"', add
label define occ1990_lbl 785 `"Assemblers of electrical equipment"', add
label define occ1990_lbl 789 `"Hand painting, coating, and decorating occupations"', add
label define occ1990_lbl 796 `"Production checkers and inspectors"', add
label define occ1990_lbl 799 `"Graders and sorters in manufacturing"', add
label define occ1990_lbl 803 `"Supervisors of motor vehicle transportation"', add
label define occ1990_lbl 804 `"Truck, delivery, and tractor drivers"', add
label define occ1990_lbl 808 `"Bus drivers"', add
label define occ1990_lbl 809 `"Taxi cab drivers and chauffeurs"', add
label define occ1990_lbl 813 `"Parking lot attendants"', add
label define occ1990_lbl 815 `"Transport equipment operatives--allocated (1990 internal census)"', add
label define occ1990_lbl 823 `"Railroad conductors and yardmasters"', add
label define occ1990_lbl 824 `"Locomotive operators (engineers and firemen)"', add
label define occ1990_lbl 825 `"Railroad brake, coupler, and switch operators"', add
label define occ1990_lbl 829 `"Ship crews and marine engineers"', add
label define occ1990_lbl 834 `"Water transport infrastructure tenders and crossing guards"', add
label define occ1990_lbl 844 `"Operating engineers of construction equipment"', add
label define occ1990_lbl 848 `"Crane, derrick, winch, and hoist operators"', add
label define occ1990_lbl 853 `"Excavating and loading machine operators"', add
label define occ1990_lbl 859 `"Misc material moving occupations"', add
label define occ1990_lbl 865 `"Helpers, constructions"', add
label define occ1990_lbl 866 `"Helpers, surveyors"', add
label define occ1990_lbl 869 `"Construction laborers"', add
label define occ1990_lbl 874 `"Production helpers"', add
label define occ1990_lbl 875 `"Garbage and recyclable material collectors"', add
label define occ1990_lbl 876 `"Materials movers: stevedores and longshore workers"', add
label define occ1990_lbl 877 `"Stock handlers"', add
label define occ1990_lbl 878 `"Machine feeders and offbearers"', add
label define occ1990_lbl 883 `"Freight, stock, and materials handlers"', add
label define occ1990_lbl 885 `"Garage and service station related occupations"', add
label define occ1990_lbl 887 `"Vehicle washers and equipment cleaners"', add
label define occ1990_lbl 888 `"Packers and packagers by hand"', add
label define occ1990_lbl 889 `"Laborers outside construction"', add
label define occ1990_lbl 890 `"Laborers, except farm--allocated (1990 internal census)"', add
label define occ1990_lbl 905 `"Military"', add
label define occ1990_lbl 991 `"Unemployed"', add
label define occ1990_lbl 999 `"Unknown"', add
label values occ1990 occ1990_lbl

label define ind1990_lbl 000 `"NIU"'
label define ind1990_lbl 010 `"   Agricultural production, crops"', add
label define ind1990_lbl 011 `"   Agricultural production, livestock"', add
label define ind1990_lbl 012 `"   Veterinary services"', add
label define ind1990_lbl 020 `"   Landscape and horticultural services"', add
label define ind1990_lbl 030 `"   Agricultural services, n.e.c."', add
label define ind1990_lbl 031 `"   Forestry"', add
label define ind1990_lbl 032 `"   Fishing, hunting, and trapping "', add
label define ind1990_lbl 040 `"   Metal mining"', add
label define ind1990_lbl 041 `"   Coal mining"', add
label define ind1990_lbl 042 `"   Oil and gas extraction"', add
label define ind1990_lbl 050 `"   Nonmetallic mining and quarrying, except fuels "', add
label define ind1990_lbl 060 `"   All construction "', add
label define ind1990_lbl 100 `"         Meat products"', add
label define ind1990_lbl 101 `"         Dairy products"', add
label define ind1990_lbl 102 `"         Canned, frozen, and preserved fruits and vegetables"', add
label define ind1990_lbl 110 `"         Grain mill products"', add
label define ind1990_lbl 111 `"         Bakery products"', add
label define ind1990_lbl 112 `"         Sugar and confectionery products"', add
label define ind1990_lbl 120 `"         Beverage industries"', add
label define ind1990_lbl 121 `"         Misc. food preparations and kindred products"', add
label define ind1990_lbl 122 `"         Food industries, n.s."', add
label define ind1990_lbl 130 `"      Tobacco manufactures"', add
label define ind1990_lbl 132 `"         Knitting mills"', add
label define ind1990_lbl 140 `"         Dyeing and finishing textiles, except wool and knit goods"', add
label define ind1990_lbl 141 `"         Carpets and rugs"', add
label define ind1990_lbl 142 `"         Yarn, thread, and fabric mills"', add
label define ind1990_lbl 150 `"         Miscellaneous textile mill products"', add
label define ind1990_lbl 151 `"         Apparel and accessories, except knit"', add
label define ind1990_lbl 152 `"         Miscellaneous fabricated textile products"', add
label define ind1990_lbl 160 `"         Pulp, paper, and paperboard mills"', add
label define ind1990_lbl 161 `"         Miscellaneous paper and pulp products"', add
label define ind1990_lbl 162 `"         Paperboard containers and boxes"', add
label define ind1990_lbl 171 `"         Newspaper publishing and printing"', add
label define ind1990_lbl 172 `"         Printing, publishing, and allied industries, except newspapers"', add
label define ind1990_lbl 180 `"         Plastics, synthetics, and resins"', add
label define ind1990_lbl 181 `"         Drugs"', add
label define ind1990_lbl 182 `"         Soaps and cosmetics"', add
label define ind1990_lbl 190 `"         Paints, varnishes, and related products"', add
label define ind1990_lbl 191 `"         Agricultural chemicals"', add
label define ind1990_lbl 192 `"         Industrial and miscellaneous chemicals"', add
label define ind1990_lbl 200 `"         Petroleum refining"', add
label define ind1990_lbl 201 `"         Miscellaneous petroleum and coal products"', add
label define ind1990_lbl 210 `"         Tires and inner tubes"', add
label define ind1990_lbl 211 `"         Other rubber products, and plastics footwear and belting"', add
label define ind1990_lbl 212 `"         Miscellaneous plastics products"', add
label define ind1990_lbl 220 `"         Leather tanning and finishing"', add
label define ind1990_lbl 221 `"         Footwear, except rubber and plastic"', add
label define ind1990_lbl 222 `"         Leather products, except footwear"', add
label define ind1990_lbl 229 `"Manufacturing, non-durable - allocated"', add
label define ind1990_lbl 230 `"         Logging"', add
label define ind1990_lbl 231 `"         Sawmills, planing mills, and millwork"', add
label define ind1990_lbl 232 `"         Wood buildings and mobile homes"', add
label define ind1990_lbl 241 `"         Miscellaneous wood products"', add
label define ind1990_lbl 242 `"      Furniture and fixtures"', add
label define ind1990_lbl 250 `"         Glass and glass products"', add
label define ind1990_lbl 251 `"         Cement, concrete, gypsum, and plaster products"', add
label define ind1990_lbl 252 `"         Structural clay products"', add
label define ind1990_lbl 261 `"         Pottery and related products"', add
label define ind1990_lbl 262 `"         Misc. nonmetallic mineral and stone products"', add
label define ind1990_lbl 270 `"         Blast furnaces, steelworks, rolling and finishing mills"', add
label define ind1990_lbl 271 `"         Iron and steel foundries"', add
label define ind1990_lbl 272 `"         Primary aluminum industries"', add
label define ind1990_lbl 280 `"         Other primary metal industries"', add
label define ind1990_lbl 281 `"         Cutlery, handtools, and general hardware"', add
label define ind1990_lbl 282 `"         Fabricated structural metal products"', add
label define ind1990_lbl 290 `"         Screw machine products"', add
label define ind1990_lbl 291 `"         Metal forgings and stampings"', add
label define ind1990_lbl 292 `"         Ordnance"', add
label define ind1990_lbl 300 `"         Miscellaneous fabricated metal products"', add
label define ind1990_lbl 301 `"         Metal industries, n.s."', add
label define ind1990_lbl 310 `"         Engines and turbines"', add
label define ind1990_lbl 311 `"         Farm machinery and equipment"', add
label define ind1990_lbl 312 `"         Construction and material handling machines"', add
label define ind1990_lbl 320 `"         Metalworking machinery"', add
label define ind1990_lbl 321 `"         Office and accounting machines"', add
label define ind1990_lbl 322 `"         Computers and related equipment"', add
label define ind1990_lbl 331 `"         Machinery, except electrical, n.e.c."', add
label define ind1990_lbl 332 `"         Machinery, n.s."', add
label define ind1990_lbl 340 `"         Household appliances"', add
label define ind1990_lbl 341 `"         Radio, TV, and communication equipment"', add
label define ind1990_lbl 342 `"         Electrical machinery, equipment, and supplies, n.e.c."', add
label define ind1990_lbl 350 `"         Electrical machinery, equipment, and supplies, n.s."', add
label define ind1990_lbl 351 `"         Motor vehicles and motor vehicle equipment"', add
label define ind1990_lbl 352 `"         Aircraft and parts"', add
label define ind1990_lbl 360 `"         Ship and boat building and repairing"', add
label define ind1990_lbl 361 `"         Railroad locomotives and equipment"', add
label define ind1990_lbl 362 `"         Guided missiles, space vehicles, and parts"', add
label define ind1990_lbl 370 `"         Cycles and miscellaneous transportation equipment"', add
label define ind1990_lbl 371 `"         Scientific and controlling instruments"', add
label define ind1990_lbl 372 `"         Medical, dental, and optical instruments and supplies"', add
label define ind1990_lbl 380 `"         Photographic equipment and supplies"', add
label define ind1990_lbl 381 `"         Watches, clocks, and clockwork operated devices"', add
label define ind1990_lbl 390 `"      Toys, amusement, and sporting goods"', add
label define ind1990_lbl 391 `"      Miscellaneous manufacturing industries"', add
label define ind1990_lbl 392 `"      Manufacturing industries, n.s. "', add
label define ind1990_lbl 400 `"      Railroads"', add
label define ind1990_lbl 401 `"      Bus service and urban transit"', add
label define ind1990_lbl 402 `"      Taxicab service"', add
label define ind1990_lbl 410 `"      Trucking service"', add
label define ind1990_lbl 411 `"      Warehousing and storage"', add
label define ind1990_lbl 412 `"      U.S. Postal Service"', add
label define ind1990_lbl 420 `"      Water transportation"', add
label define ind1990_lbl 421 `"      Air transportation"', add
label define ind1990_lbl 422 `"      Pipe lines, except natural gas"', add
label define ind1990_lbl 432 `"      Services incidental to transportation"', add
label define ind1990_lbl 440 `"      Radio and television broadcasting and cable"', add
label define ind1990_lbl 441 `"      Wired communications"', add
label define ind1990_lbl 442 `"      Telegraph and miscellaneous communications services"', add
label define ind1990_lbl 450 `"      Electric light and power"', add
label define ind1990_lbl 451 `"      Gas and steam supply systems"', add
label define ind1990_lbl 452 `"      Electric and gas, and other combinations"', add
label define ind1990_lbl 470 `"      Water supply and irrigation"', add
label define ind1990_lbl 471 `"      Sanitary services"', add
label define ind1990_lbl 472 `"      Utilities, n.s. "', add
label define ind1990_lbl 500 `"      Motor vehicles and equipment"', add
label define ind1990_lbl 501 `"      Furniture and home furnishings"', add
label define ind1990_lbl 502 `"      Lumber and construction materials"', add
label define ind1990_lbl 510 `"      Professional and commercial equipment and supplies"', add
label define ind1990_lbl 511 `"      Metals and minerals, except petroleum"', add
label define ind1990_lbl 512 `"      Electrical goods"', add
label define ind1990_lbl 521 `"      Hardware, plumbing and heating supplies"', add
label define ind1990_lbl 530 `"      Machinery, equipment, and supplies"', add
label define ind1990_lbl 531 `"      Scrap and waste materials"', add
label define ind1990_lbl 532 `"      Miscellaneous wholesale, durable goods"', add
label define ind1990_lbl 540 `"      Paper and paper products"', add
label define ind1990_lbl 541 `"      Drugs, chemicals, and allied products"', add
label define ind1990_lbl 542 `"      Apparel, fabrics, and notions"', add
label define ind1990_lbl 550 `"      Groceries and related products"', add
label define ind1990_lbl 551 `"      Farm-product raw materials"', add
label define ind1990_lbl 552 `"      Petroleum products"', add
label define ind1990_lbl 560 `"      Alcoholic beverages"', add
label define ind1990_lbl 561 `"      Farm supplies"', add
label define ind1990_lbl 562 `"      Miscellaneous wholesale, nondurable goods"', add
label define ind1990_lbl 571 `"      Wholesale trade, n.s. "', add
label define ind1990_lbl 580 `"   Lumber and building material retailing"', add
label define ind1990_lbl 581 `"   Hardware stores"', add
label define ind1990_lbl 582 `"   Retail nurseries and garden stores"', add
label define ind1990_lbl 590 `"   Mobile home dealers"', add
label define ind1990_lbl 591 `"   Department stores"', add
label define ind1990_lbl 592 `"   Variety stores"', add
label define ind1990_lbl 600 `"   Miscellaneous general merchandise stores"', add
label define ind1990_lbl 601 `"   Grocery stores"', add
label define ind1990_lbl 602 `"   Dairy products stores"', add
label define ind1990_lbl 610 `"   Retail bakeries"', add
label define ind1990_lbl 611 `"   Food stores, n.e.c."', add
label define ind1990_lbl 612 `"   Motor vehicle dealers"', add
label define ind1990_lbl 620 `"   Auto and home supply stores"', add
label define ind1990_lbl 621 `"   Gasoline service stations"', add
label define ind1990_lbl 622 `"   Miscellaneous vehicle dealers"', add
label define ind1990_lbl 623 `"   Apparel and accessory stores, except shoe"', add
label define ind1990_lbl 630 `"   Shoe stores"', add
label define ind1990_lbl 631 `"   Furniture and home furnishings stores"', add
label define ind1990_lbl 632 `"   Household appliance stores"', add
label define ind1990_lbl 633 `"   Radio, TV, and computer stores"', add
label define ind1990_lbl 640 `"   Music stores"', add
label define ind1990_lbl 641 `"   Eating and drinking places"', add
label define ind1990_lbl 642 `"   Drug stores"', add
label define ind1990_lbl 650 `"   Liquor stores"', add
label define ind1990_lbl 651 `"   Sporting goods, bicycles, and hobby stores"', add
label define ind1990_lbl 652 `"   Book and stationery stores"', add
label define ind1990_lbl 660 `"   Jewelry stores"', add
label define ind1990_lbl 661 `"   Gift, novelty, and souvenir shops"', add
label define ind1990_lbl 662 `"   Sewing, needlework, and piece goods stores"', add
label define ind1990_lbl 663 `"   Catalog and mail order houses"', add
label define ind1990_lbl 670 `"   Vending machine operators"', add
label define ind1990_lbl 671 `"   Direct selling establishments"', add
label define ind1990_lbl 672 `"   Fuel dealers"', add
label define ind1990_lbl 681 `"   Retail florists"', add
label define ind1990_lbl 682 `"   Miscellaneous retail stores"', add
label define ind1990_lbl 691 `"   Retail trade, n.s. "', add
label define ind1990_lbl 700 `"   Banking"', add
label define ind1990_lbl 701 `"   Savings institutions, including credit unions"', add
label define ind1990_lbl 702 `"   Credit agencies, n.e.c."', add
label define ind1990_lbl 710 `"   Security, commodity brokerage, and investment companies"', add
label define ind1990_lbl 711 `"   Insurance"', add
label define ind1990_lbl 712 `"   Real estate, including real estate-insurance offices "', add
label define ind1990_lbl 721 `"   Advertising"', add
label define ind1990_lbl 722 `"   Services to dwellings and other buildings"', add
label define ind1990_lbl 731 `"   Personnel supply services"', add
label define ind1990_lbl 732 `"   Computer and data processing services"', add
label define ind1990_lbl 740 `"   Detective and protective services"', add
label define ind1990_lbl 741 `"   Business services, n.e.c."', add
label define ind1990_lbl 742 `"   Automotive rental and leasing, without drivers"', add
label define ind1990_lbl 750 `"   Automobile parking and carwashes"', add
label define ind1990_lbl 751 `"   Automotive repair and related services"', add
label define ind1990_lbl 752 `"   Electrical repair shops"', add
label define ind1990_lbl 760 `"   Miscellaneous repair services "', add
label define ind1990_lbl 761 `"   Private households"', add
label define ind1990_lbl 762 `"   Hotels and motels"', add
label define ind1990_lbl 770 `"   Lodging places, except hotels and motels"', add
label define ind1990_lbl 771 `"   Laundry, cleaning, and garment services"', add
label define ind1990_lbl 772 `"   Beauty shops"', add
label define ind1990_lbl 780 `"   Barber shops"', add
label define ind1990_lbl 781 `"   Funeral service and crematories"', add
label define ind1990_lbl 782 `"   Shoe repair shops"', add
label define ind1990_lbl 790 `"   Dressmaking shops"', add
label define ind1990_lbl 791 `"   Miscellaneous personal services "', add
label define ind1990_lbl 800 `"   Theaters and motion pictures"', add
label define ind1990_lbl 801 `"   Video tape rental"', add
label define ind1990_lbl 802 `"   Bowling centers"', add
label define ind1990_lbl 810 `"   Miscellaneous entertainment and recreation services "', add
label define ind1990_lbl 812 `"   Offices and clinics of physicians"', add
label define ind1990_lbl 820 `"   Offices and clinics of dentists"', add
label define ind1990_lbl 821 `"   Offices and clinics of chiropractors"', add
label define ind1990_lbl 822 `"   Offices and clinics of optometrists"', add
label define ind1990_lbl 830 `"   Offices and clinics of health practitioners, n.e.c."', add
label define ind1990_lbl 831 `"   Hospitals"', add
label define ind1990_lbl 832 `"   Nursing and personal care facilities"', add
label define ind1990_lbl 840 `"   Health services, n.e.c."', add
label define ind1990_lbl 841 `"   Legal services"', add
label define ind1990_lbl 842 `"   Elementary and secondary schools"', add
label define ind1990_lbl 850 `"   Colleges and universities"', add
label define ind1990_lbl 851 `"   Vocational schools"', add
label define ind1990_lbl 852 `"   Libraries"', add
label define ind1990_lbl 860 `"   Educational services, n.e.c."', add
label define ind1990_lbl 861 `"   Job training and vocational rehabilitation services"', add
label define ind1990_lbl 862 `"   Child day care services"', add
label define ind1990_lbl 863 `"   Family child care homes"', add
label define ind1990_lbl 870 `"   Residential care facilities, without nursing"', add
label define ind1990_lbl 871 `"   Social services, n.e.c."', add
label define ind1990_lbl 872 `"   Museums, art galleries, and zoos"', add
label define ind1990_lbl 873 `"   Labor unions"', add
label define ind1990_lbl 880 `"   Religious organizations"', add
label define ind1990_lbl 881 `"   Membership organizations, n.e.c."', add
label define ind1990_lbl 882 `"   Engineering, architectural, and surveying services"', add
label define ind1990_lbl 890 `"   Accounting, auditing, and bookkeeping services"', add
label define ind1990_lbl 891 `"   Research, development, and testing services"', add
label define ind1990_lbl 892 `"   Management and public relations services"', add
label define ind1990_lbl 893 `"   Miscellaneous professional and related services "', add
label define ind1990_lbl 900 `"   Executive and legislative offices"', add
label define ind1990_lbl 901 `"   General government, n.e.c."', add
label define ind1990_lbl 910 `"   Justice, public order, and safety"', add
label define ind1990_lbl 921 `"   Public finance, taxation, and monetary policy"', add
label define ind1990_lbl 922 `"   Administration of human resources programs"', add
label define ind1990_lbl 930 `"   Administration of environmental quality and housing programs"', add
label define ind1990_lbl 931 `"   Administration of economic programs"', add
label define ind1990_lbl 932 `"   National security and international affairs "', add
label define ind1990_lbl 940 `"      Army"', add
label define ind1990_lbl 941 `"      Air Force"', add
label define ind1990_lbl 942 `"      Navy"', add
label define ind1990_lbl 950 `"      Marines"', add
label define ind1990_lbl 951 `"      Coast Guard"', add
label define ind1990_lbl 952 `"      Armed Forces, branch not specified"', add
label define ind1990_lbl 960 `"      Military Reserves or National Guard "', add
label define ind1990_lbl 998 `"Unknown"', add
label values ind1990 ind1990_lbl

label define occ1950_lbl 000 `"Accountants and auditors"'
label define occ1950_lbl 001 `"Actors and actresses"', add
label define occ1950_lbl 002 `"Airplane pilots and navigators"', add
label define occ1950_lbl 003 `"Architects"', add
label define occ1950_lbl 004 `"Artists and art teachers"', add
label define occ1950_lbl 005 `"Athletes"', add
label define occ1950_lbl 006 `"Authors"', add
label define occ1950_lbl 007 `"Chemists"', add
label define occ1950_lbl 008 `"Chiropractors"', add
label define occ1950_lbl 009 `"Clergymen"', add
label define occ1950_lbl 010 `"College presidents and deans"', add
label define occ1950_lbl 012 `"Agricultural sciences"', add
label define occ1950_lbl 013 `"Biological sciences"', add
label define occ1950_lbl 014 `"Chemistry"', add
label define occ1950_lbl 015 `"Economics"', add
label define occ1950_lbl 016 `"Engineering"', add
label define occ1950_lbl 017 `"Geology and geophysics"', add
label define occ1950_lbl 018 `"Mathematics"', add
label define occ1950_lbl 019 `"Medical sciences"', add
label define occ1950_lbl 023 `"Physics"', add
label define occ1950_lbl 024 `"Psychology"', add
label define occ1950_lbl 025 `"Statistics"', add
label define occ1950_lbl 026 `"Natural science (n.e.c.)"', add
label define occ1950_lbl 027 `"Social sciences (n.e.c.)"', add
label define occ1950_lbl 028 `"Nonscientific subjects"', add
label define occ1950_lbl 029 `"Subject not specified"', add
label define occ1950_lbl 031 `"Dancers and dancing teachers"', add
label define occ1950_lbl 032 `"Dentists"', add
label define occ1950_lbl 033 `"Designers"', add
label define occ1950_lbl 034 `"Dieticians and nutritionists"', add
label define occ1950_lbl 035 `"Draftsmen"', add
label define occ1950_lbl 036 `"Editors and reporters"', add
label define occ1950_lbl 041 `"Engineers, aeronautical"', add
label define occ1950_lbl 042 `"Engineers, chemical"', add
label define occ1950_lbl 043 `"Engineers, civil"', add
label define occ1950_lbl 044 `"Engineers, electrical"', add
label define occ1950_lbl 045 `"Engineers, industrial"', add
label define occ1950_lbl 046 `"Engineers, mechanical"', add
label define occ1950_lbl 047 `"Engineers, metallurgical, metallurgists"', add
label define occ1950_lbl 048 `"Engineers, mining"', add
label define occ1950_lbl 049 `"Engineers (n.e.c.)"', add
label define occ1950_lbl 051 `"Entertainers (n.e.c.)"', add
label define occ1950_lbl 052 `"Farm and home management advisors"', add
label define occ1950_lbl 053 `"Foresters and conservationists"', add
label define occ1950_lbl 054 `"Funeral directors and embalmers"', add
label define occ1950_lbl 055 `"Lawyers and judges"', add
label define occ1950_lbl 056 `"Librarians"', add
label define occ1950_lbl 057 `"Musicians and music teachers"', add
label define occ1950_lbl 058 `"Nurses, professional"', add
label define occ1950_lbl 059 `"Nurses, student professional"', add
label define occ1950_lbl 061 `"Agricultural scientists"', add
label define occ1950_lbl 062 `"Biological scientists"', add
label define occ1950_lbl 063 `"Geologists and geophysicists"', add
label define occ1950_lbl 067 `"Mathematicians"', add
label define occ1950_lbl 068 `"Physicists"', add
label define occ1950_lbl 069 `"Miscellaneous natural scientists"', add
label define occ1950_lbl 070 `"Optometrists"', add
label define occ1950_lbl 071 `"Osteopaths"', add
label define occ1950_lbl 072 `"Personnel and labor relations workers"', add
label define occ1950_lbl 073 `"Pharmacists"', add
label define occ1950_lbl 074 `"Photographers"', add
label define occ1950_lbl 075 `"Physicians and surgeons"', add
label define occ1950_lbl 076 `"Radio operators"', add
label define occ1950_lbl 077 `"Recreation and group workers"', add
label define occ1950_lbl 078 `"Religious workers"', add
label define occ1950_lbl 079 `"Social and welfare workers, except group"', add
label define occ1950_lbl 081 `"Economists"', add
label define occ1950_lbl 082 `"Psychologists"', add
label define occ1950_lbl 083 `"Statisticians and actuaries"', add
label define occ1950_lbl 084 `"Miscellaneous social scientists"', add
label define occ1950_lbl 091 `"Sports instructors and officials"', add
label define occ1950_lbl 092 `"Surveyors"', add
label define occ1950_lbl 093 `"Teachers (n.e.c.)"', add
label define occ1950_lbl 094 `"Technicians, medical and dental"', add
label define occ1950_lbl 095 `"Technicians, testing"', add
label define occ1950_lbl 096 `"Technicians (n.e.c.)"', add
label define occ1950_lbl 097 `"Therapists and healers (n.e.c.)"', add
label define occ1950_lbl 098 `"Veterinarians"', add
label define occ1950_lbl 099 `"Professional, technical and kindred workers (n.e.c.)"', add
label define occ1950_lbl 100 `"Farmers (owners and tenants)"', add
label define occ1950_lbl 123 `"Farm managers"', add
label define occ1950_lbl 200 `"Buyers and department heads, store"', add
label define occ1950_lbl 201 `"Buyers and shippers, farm products"', add
label define occ1950_lbl 203 `"Conductors, railroad"', add
label define occ1950_lbl 204 `"Credit men"', add
label define occ1950_lbl 205 `"Floormen and floor managers, store"', add
label define occ1950_lbl 210 `"Inspectors, public administration"', add
label define occ1950_lbl 230 `"Managers and superintendents, building"', add
label define occ1950_lbl 240 `"Officers, pilots, pursers and engineers, ship"', add
label define occ1950_lbl 250 `"Officials and administrators (n.e.c.), public administration"', add
label define occ1950_lbl 260 `"Officials, lodge, society, union, etc."', add
label define occ1950_lbl 270 `"Postmasters"', add
label define occ1950_lbl 280 `"Purchasing agents and buyers (n.e.c.)"', add
label define occ1950_lbl 290 `"Managers, officials, and proprietors (n.e.c.)"', add
label define occ1950_lbl 300 `"Agents (n.e.c.)"', add
label define occ1950_lbl 301 `"Attendants and assistants, library"', add
label define occ1950_lbl 302 `"Attendants, physicians and dentists office"', add
label define occ1950_lbl 304 `"Baggagemen, transportation"', add
label define occ1950_lbl 305 `"Bank tellers"', add
label define occ1950_lbl 310 `"Bookkeepers"', add
label define occ1950_lbl 320 `"Cashiers"', add
label define occ1950_lbl 321 `"Collectors, bill and account"', add
label define occ1950_lbl 322 `"Dispatchers and starters, vehicle"', add
label define occ1950_lbl 325 `"Express messengers and railway mail clerks"', add
label define occ1950_lbl 335 `"Mail carriers"', add
label define occ1950_lbl 340 `"Messengers and office boys"', add
label define occ1950_lbl 341 `"Office machine operators"', add
label define occ1950_lbl 342 `"Shipping and receiving clerks"', add
label define occ1950_lbl 350 `"Stenographers, typists, and secretaries"', add
label define occ1950_lbl 360 `"Telegraph messengers"', add
label define occ1950_lbl 365 `"Telegraph operators"', add
label define occ1950_lbl 370 `"Telephone operators"', add
label define occ1950_lbl 380 `"Ticket, station, and express agents"', add
label define occ1950_lbl 390 `"Clerical and kindred workers (n.e.c.)"', add
label define occ1950_lbl 400 `"Advertising agents and salesmen"', add
label define occ1950_lbl 410 `"Auctioneers"', add
label define occ1950_lbl 420 `"Demonstrators"', add
label define occ1950_lbl 430 `"Hucksters and peddlers"', add
label define occ1950_lbl 450 `"Insurance agents and brokers"', add
label define occ1950_lbl 460 `"Newsboys"', add
label define occ1950_lbl 470 `"Real estate agents and brokers"', add
label define occ1950_lbl 480 `"Stock and bond salesmen"', add
label define occ1950_lbl 490 `"Salesmen and sales clerks (n.e.c.)"', add
label define occ1950_lbl 500 `"Bakers"', add
label define occ1950_lbl 501 `"Blacksmiths"', add
label define occ1950_lbl 502 `"Bookbinders"', add
label define occ1950_lbl 503 `"Boilermakers"', add
label define occ1950_lbl 504 `"Brickmasons, stonemasons, and tile setters"', add
label define occ1950_lbl 505 `"Cabinetmakers"', add
label define occ1950_lbl 510 `"Carpenters"', add
label define occ1950_lbl 511 `"Cement and concrete finishers"', add
label define occ1950_lbl 512 `"Compositors and typesetters"', add
label define occ1950_lbl 513 `"Cranemen, derrickmen, and hoistmen"', add
label define occ1950_lbl 514 `"Decorators and window dressers"', add
label define occ1950_lbl 515 `"Electricians"', add
label define occ1950_lbl 520 `"Electrotypers and stereotypers"', add
label define occ1950_lbl 521 `"Engravers, except photoengravers"', add
label define occ1950_lbl 522 `"Excavating, grading, and road machinery operators"', add
label define occ1950_lbl 523 `"Foremen (n.e.c.)"', add
label define occ1950_lbl 524 `"Forgemen and hammermen"', add
label define occ1950_lbl 525 `"Furriers"', add
label define occ1950_lbl 530 `"Glaziers"', add
label define occ1950_lbl 531 `"Heat treaters, annealers, temperers"', add
label define occ1950_lbl 532 `"Inspectors, scalers, and graders, log and lumber"', add
label define occ1950_lbl 533 `"Inspectors (n.e.c.)"', add
label define occ1950_lbl 534 `"Jewelers, watchmakers, goldsmiths, and silversmiths"', add
label define occ1950_lbl 535 `"Job setters, metal"', add
label define occ1950_lbl 540 `"Linemen and servicemen, telegraph, telephone, and power"', add
label define occ1950_lbl 541 `"Locomotive engineers"', add
label define occ1950_lbl 542 `"Locomotive firemen"', add
label define occ1950_lbl 543 `"Loom fixers"', add
label define occ1950_lbl 544 `"Machinists"', add
label define occ1950_lbl 545 `"Mechanics and repairmen, airplane"', add
label define occ1950_lbl 550 `"Mechanics and repairmen, automobile"', add
label define occ1950_lbl 551 `"Mechanics and repairmen, office machine"', add
label define occ1950_lbl 552 `"Mechanics and repairmen, radio and television"', add
label define occ1950_lbl 553 `"Mechanics and repairmen, railroad and car shop"', add
label define occ1950_lbl 554 `"Mechanics and repairmen (n.e.c.)"', add
label define occ1950_lbl 555 `"Millers, grain, flour, feed, etc."', add
label define occ1950_lbl 560 `"Millwrights"', add
label define occ1950_lbl 561 `"Molders, metal"', add
label define occ1950_lbl 562 `"Motion picture projectionists"', add
label define occ1950_lbl 563 `"Opticians and lens grinders and polishers"', add
label define occ1950_lbl 564 `"Painters, construction and maintenance"', add
label define occ1950_lbl 565 `"Paperhangers"', add
label define occ1950_lbl 570 `"Pattern and model makers, except paper"', add
label define occ1950_lbl 571 `"Photoengravers and lithographers"', add
label define occ1950_lbl 572 `"Piano and organ tuners and repairmen"', add
label define occ1950_lbl 573 `"Plasterers"', add
label define occ1950_lbl 574 `"Plumbers and pipe fitters"', add
label define occ1950_lbl 575 `"Pressmen and plate printers, printing"', add
label define occ1950_lbl 580 `"Rollers and roll hands, metal"', add
label define occ1950_lbl 581 `"Roofers and slaters"', add
label define occ1950_lbl 582 `"Shoemakers and repairers, except factory"', add
label define occ1950_lbl 583 `"Stationary engineers"', add
label define occ1950_lbl 584 `"Stone cutters and stone carvers"', add
label define occ1950_lbl 585 `"Structural metal workers"', add
label define occ1950_lbl 590 `"Tailors and tailoresses"', add
label define occ1950_lbl 591 `"Tinsmiths, coppersmiths, and sheet metal workers"', add
label define occ1950_lbl 592 `"Tool makers, and die makers and setters"', add
label define occ1950_lbl 593 `"Upholsterers"', add
label define occ1950_lbl 594 `"Craftsmen and kindred workers (n.e.c.)"', add
label define occ1950_lbl 595 `"Members of the armed services"', add
label define occ1950_lbl 600 `"Apprentice auto mechanics"', add
label define occ1950_lbl 601 `"Apprentice bricklayers and masons"', add
label define occ1950_lbl 602 `"Apprentice carpenters"', add
label define occ1950_lbl 603 `"Apprentice electricians"', add
label define occ1950_lbl 604 `"Apprentice machinists and toolmakers"', add
label define occ1950_lbl 605 `"Apprentice mechanics, except auto"', add
label define occ1950_lbl 610 `"Apprentice plumbers and pipe fitters"', add
label define occ1950_lbl 611 `"Apprentices, building trades (n.e.c.)"', add
label define occ1950_lbl 612 `"Apprentices, metalworking trades (n.e.c.)"', add
label define occ1950_lbl 613 `"Apprentices, printing trades"', add
label define occ1950_lbl 614 `"Apprentices, other specified trades"', add
label define occ1950_lbl 615 `"Apprentices, trade not specified"', add
label define occ1950_lbl 620 `"Asbestos and insulation workers"', add
label define occ1950_lbl 621 `"Attendants, auto service and parking"', add
label define occ1950_lbl 622 `"Blasters and powdermen"', add
label define occ1950_lbl 623 `"Boatmen, canalmen, and lock keepers"', add
label define occ1950_lbl 624 `"Brakemen, railroad"', add
label define occ1950_lbl 625 `"Bus drivers"', add
label define occ1950_lbl 630 `"Chainmen, rodmen, and axmen, surveying"', add
label define occ1950_lbl 631 `"Conductors, bus and street railway"', add
label define occ1950_lbl 632 `"Deliverymen and routemen"', add
label define occ1950_lbl 633 `"Dressmakers and seamstresses, except factory"', add
label define occ1950_lbl 634 `"Dyers"', add
label define occ1950_lbl 635 `"Filers, grinders, and polishers, metal"', add
label define occ1950_lbl 640 `"Fruit, nut, veg graders and packers, except factory"', add
label define occ1950_lbl 641 `"Furnacemen, smeltermen and pourers"', add
label define occ1950_lbl 642 `"Heaters, metal"', add
label define occ1950_lbl 643 `"Laundry and dry cleaning operatives"', add
label define occ1950_lbl 644 `"Meat cutters, except slaughter and packing house"', add
label define occ1950_lbl 645 `"Milliners"', add
label define occ1950_lbl 650 `"Mine operatives and laborers"', add
label define occ1950_lbl 660 `"Motormen, mine, factory, logging camp, etc."', add
label define occ1950_lbl 661 `"Motormen, street, subway, and elevated railway"', add
label define occ1950_lbl 662 `"Oilers and greaser, except auto"', add
label define occ1950_lbl 670 `"Painters, except construction or maintenance"', add
label define occ1950_lbl 671 `"Photographic process workers"', add
label define occ1950_lbl 672 `"Power station operators"', add
label define occ1950_lbl 673 `"Sailors and deck hands"', add
label define occ1950_lbl 674 `"Sawyers"', add
label define occ1950_lbl 675 `"Spinners, textile"', add
label define occ1950_lbl 680 `"Stationary firemen"', add
label define occ1950_lbl 681 `"Switchmen, railroad"', add
label define occ1950_lbl 682 `"Taxicab drivers and chauffers"', add
label define occ1950_lbl 683 `"Truck and tractor drivers"', add
label define occ1950_lbl 684 `"Weavers, textile"', add
label define occ1950_lbl 685 `"Welders and flame cutters"', add
label define occ1950_lbl 690 `"Operative and kindred workers (n.e.c.)"', add
label define occ1950_lbl 700 `"Housekeepers, private household"', add
label define occ1950_lbl 710 `"Laundressses, private household"', add
label define occ1950_lbl 720 `"Private household workers (n.e.c.)"', add
label define occ1950_lbl 730 `"Attendants, hospital and other institution"', add
label define occ1950_lbl 731 `"Attendants, professional and personal service (n.e.c.)"', add
label define occ1950_lbl 732 `"Attendants, recreation and amusement"', add
label define occ1950_lbl 740 `"Barbers, beauticians, and manicurists"', add
label define occ1950_lbl 750 `"Bartenders"', add
label define occ1950_lbl 751 `"Bootblacks"', add
label define occ1950_lbl 752 `"Boarding and lodging house keepers"', add
label define occ1950_lbl 753 `"Charwomen and cleaners"', add
label define occ1950_lbl 754 `"Cooks, except private household"', add
label define occ1950_lbl 760 `"Counter and fountain workers"', add
label define occ1950_lbl 761 `"Elevator operators"', add
label define occ1950_lbl 762 `"Firemen, fire protection"', add
label define occ1950_lbl 763 `"Guards, watchmen, and doorkeepers"', add
label define occ1950_lbl 764 `"Housekeepers and stewards, except private household"', add
label define occ1950_lbl 770 `"Janitors and sextons"', add
label define occ1950_lbl 771 `"Marshals and constables"', add
label define occ1950_lbl 772 `"Midwives"', add
label define occ1950_lbl 773 `"Policemen and detectives"', add
label define occ1950_lbl 780 `"Porters"', add
label define occ1950_lbl 781 `"Practical nurses"', add
label define occ1950_lbl 782 `"Sheriffs and bailiffs"', add
label define occ1950_lbl 783 `"Ushers, recreation and amusement"', add
label define occ1950_lbl 784 `"Waiters and waitresses"', add
label define occ1950_lbl 785 `"Watchmen (crossing) and bridge tenders"', add
label define occ1950_lbl 790 `"Service workers, except private household (n.e.c.)"', add
label define occ1950_lbl 810 `"Farm foremen"', add
label define occ1950_lbl 820 `"Farm laborers, wage workers"', add
label define occ1950_lbl 830 `"Farm laborers, unpaid family workers"', add
label define occ1950_lbl 840 `"Farm service laborers, self-employed"', add
label define occ1950_lbl 910 `"Fishermen and oystermen"', add
label define occ1950_lbl 920 `"Garage laborers and car washers and greasers"', add
label define occ1950_lbl 930 `"Gardeners, except farm, and groundskeepers"', add
label define occ1950_lbl 940 `"Longshoremen and stevedores"', add
label define occ1950_lbl 950 `"Lumbermen, raftsmen, and woodchoppers"', add
label define occ1950_lbl 960 `"Teamsters"', add
label define occ1950_lbl 970 `"Laborers (n.e.c.)"', add
label define occ1950_lbl 997 `"Unknown"', add
label define occ1950_lbl 999 `"Unemployed- last worked over x years ago"', add
label values occ1950 occ1950_lbl

label define ind1950_lbl 000 `"NIU"'
label define ind1950_lbl 105 `"Agriculture"', add
label define ind1950_lbl 116 `"Forestry"', add
label define ind1950_lbl 126 `"Fisheries"', add
label define ind1950_lbl 206 `"Metal mining"', add
label define ind1950_lbl 216 `"Coal mining"', add
label define ind1950_lbl 226 `"Crude petroleum and natural gas extraction"', add
label define ind1950_lbl 236 `"Nonmetallic mining and quarrying, except fuel"', add
label define ind1950_lbl 246 `"Construction"', add
label define ind1950_lbl 306 `"Logging"', add
label define ind1950_lbl 307 `"Sawmills, planing mills, and millwork"', add
label define ind1950_lbl 308 `"Misc wood products"', add
label define ind1950_lbl 309 `"Furniture and fixtures"', add
label define ind1950_lbl 316 `"Glass and glass products"', add
label define ind1950_lbl 317 `"Cement, concrete, gypsum and plaster products"', add
label define ind1950_lbl 318 `"Structural clay products"', add
label define ind1950_lbl 319 `"Pottery and related products"', add
label define ind1950_lbl 326 `"Miscellaneous nonmetallic mineral and stone products"', add
label define ind1950_lbl 336 `"Blast furnaces, steel works, and rolling mills"', add
label define ind1950_lbl 337 `"Other primary iron and steel industries"', add
label define ind1950_lbl 338 `"Primary nonferrous industries"', add
label define ind1950_lbl 346 `"Fabricated steel products"', add
label define ind1950_lbl 347 `"Fabricated nonferrous metal products"', add
label define ind1950_lbl 348 `"Not specified metal industries"', add
label define ind1950_lbl 356 `"Agricultural machinery and tractors"', add
label define ind1950_lbl 357 `"Office and store machines and devices"', add
label define ind1950_lbl 358 `"Miscellaneous machinery"', add
label define ind1950_lbl 367 `"Electrical machinery, equipment, and supplies"', add
label define ind1950_lbl 376 `"Motor vehicles and motor vehicle equipment"', add
label define ind1950_lbl 377 `"Aircraft and parts"', add
label define ind1950_lbl 378 `"Ship and boat building and repairing"', add
label define ind1950_lbl 379 `"Railroad and miscellaneous transportation equipment"', add
label define ind1950_lbl 386 `"Professional equipment and supplies"', add
label define ind1950_lbl 387 `"Photographic equipment and supplies"', add
label define ind1950_lbl 388 `"Watches, clocks, and clockwork-operated devices"', add
label define ind1950_lbl 399 `"Miscellaneous manufacturing industries"', add
label define ind1950_lbl 406 `"Meat products"', add
label define ind1950_lbl 407 `"Dairy products"', add
label define ind1950_lbl 408 `"Canning and preserving fruits, vegetables, and seafoods"', add
label define ind1950_lbl 409 `"Grain-mill products"', add
label define ind1950_lbl 416 `"Bakery products"', add
label define ind1950_lbl 417 `"Confectionery and related products"', add
label define ind1950_lbl 418 `"Beverage industries"', add
label define ind1950_lbl 419 `"Miscellaneous food preparations and kindred products"', add
label define ind1950_lbl 426 `"Not specified food industries"', add
label define ind1950_lbl 429 `"Tobacco manufactures"', add
label define ind1950_lbl 436 `"Knitting mills"', add
label define ind1950_lbl 437 `"Dyeing and finishing textiles, except knit goods"', add
label define ind1950_lbl 438 `"Carpets, rugs, and other floor coverings"', add
label define ind1950_lbl 439 `"Yarn, thread, and fabric mills"', add
label define ind1950_lbl 446 `"Miscellaneous textile mill products"', add
label define ind1950_lbl 448 `"Apparel and accessories"', add
label define ind1950_lbl 449 `"Miscellaneous fabricated textile products"', add
label define ind1950_lbl 456 `"Pulp, paper, and paperboard mills"', add
label define ind1950_lbl 457 `"Paperboard containers and boxes"', add
label define ind1950_lbl 458 `"Miscellaneous paper and pulp products"', add
label define ind1950_lbl 459 `"Printing, publishing, and allied industries"', add
label define ind1950_lbl 466 `"Synthetic fibers"', add
label define ind1950_lbl 467 `"Drugs and medicines"', add
label define ind1950_lbl 468 `"Paints, varnishes, and related products"', add
label define ind1950_lbl 469 `"Miscellaneous chemicals and allied products"', add
label define ind1950_lbl 476 `"Petroleum refining"', add
label define ind1950_lbl 477 `"Miscellaneous petroleum and coal products"', add
label define ind1950_lbl 478 `"Rubber products"', add
label define ind1950_lbl 487 `"Leather: tanned, curried, and finished"', add
label define ind1950_lbl 488 `"Footwear, except rubber"', add
label define ind1950_lbl 489 `"Leather products, except footwear"', add
label define ind1950_lbl 499 `"Not specified manufacturing industries"', add
label define ind1950_lbl 506 `"Railroads and railway express service"', add
label define ind1950_lbl 516 `"Street railways and bus lines"', add
label define ind1950_lbl 526 `"Trucking service"', add
label define ind1950_lbl 527 `"Warehousing and storage"', add
label define ind1950_lbl 536 `"Taxicab service"', add
label define ind1950_lbl 546 `"Water transportation"', add
label define ind1950_lbl 556 `"Air transportation"', add
label define ind1950_lbl 567 `"Petroleum and gasoline pipe lines"', add
label define ind1950_lbl 568 `"Services incidental to transportation"', add
label define ind1950_lbl 578 `"Telephone"', add
label define ind1950_lbl 579 `"Telegraph"', add
label define ind1950_lbl 586 `"Electric light and power"', add
label define ind1950_lbl 587 `"Gas and steam supply systems"', add
label define ind1950_lbl 588 `"Electric-gas utilities"', add
label define ind1950_lbl 596 `"Water supply"', add
label define ind1950_lbl 597 `"Sanitary services"', add
label define ind1950_lbl 598 `"Other and not specified utilities"', add
label define ind1950_lbl 606 `"Motor vehicles and equipment"', add
label define ind1950_lbl 607 `"Drugs, chemicals, and allied products"', add
label define ind1950_lbl 608 `"Dry goods apparel"', add
label define ind1950_lbl 609 `"Food and related products"', add
label define ind1950_lbl 616 `"Electrical goods, hardware, and plumbing equipment"', add
label define ind1950_lbl 617 `"Machinery, equipment, and supplies"', add
label define ind1950_lbl 618 `"Petroleum products"', add
label define ind1950_lbl 619 `"Farm products--raw materials"', add
label define ind1950_lbl 626 `"Miscellaneous wholesale trade"', add
label define ind1950_lbl 627 `"Not specified wholesale trade"', add
label define ind1950_lbl 636 `"Food stores, except dairy products"', add
label define ind1950_lbl 637 `"Dairy products stores and milk retailing"', add
label define ind1950_lbl 646 `"General merchandise stores"', add
label define ind1950_lbl 647 `"Five and ten cent stores"', add
label define ind1950_lbl 656 `"Apparel and accessories stores, except shoe"', add
label define ind1950_lbl 657 `"Shoe stores"', add
label define ind1950_lbl 658 `"Furniture and house furnishing stores"', add
label define ind1950_lbl 659 `"Household appliance and radio stores"', add
label define ind1950_lbl 667 `"Motor vehicles and accessories retailing"', add
label define ind1950_lbl 668 `"Gasoline service stations"', add
label define ind1950_lbl 669 `"Drug stores"', add
label define ind1950_lbl 679 `"Eating and drinking places"', add
label define ind1950_lbl 686 `"Hardware and farm implement stores"', add
label define ind1950_lbl 687 `"Lumber and building material retailing"', add
label define ind1950_lbl 688 `"Liquor stores"', add
label define ind1950_lbl 689 `"Retail florists"', add
label define ind1950_lbl 696 `"Jewelry stores"', add
label define ind1950_lbl 697 `"Fuel and ice retailing"', add
label define ind1950_lbl 698 `"Miscellaneous retail stores"', add
label define ind1950_lbl 699 `"Not specified retail trade"', add
label define ind1950_lbl 716 `"Banking and credit agencies"', add
label define ind1950_lbl 726 `"Security and commodity brokerage and investment companies"', add
label define ind1950_lbl 736 `"Insurance"', add
label define ind1950_lbl 746 `"Real estate"', add
label define ind1950_lbl 806 `"Advertising"', add
label define ind1950_lbl 807 `"Accounting, auditing, and bookkeeping services"', add
label define ind1950_lbl 808 `"Miscellaneous business services"', add
label define ind1950_lbl 816 `"Auto repair services and garages"', add
label define ind1950_lbl 817 `"Miscellaneous repair services"', add
label define ind1950_lbl 826 `"Private households"', add
label define ind1950_lbl 836 `"Hotels and lodging places"', add
label define ind1950_lbl 846 `"Laundering, cleaning, and dyeing services"', add
label define ind1950_lbl 847 `"Dressmaking shops"', add
label define ind1950_lbl 848 `"Shoe repair shops"', add
label define ind1950_lbl 849 `"Miscellaneous personal services"', add
label define ind1950_lbl 856 `"Radio broadcasting and television"', add
label define ind1950_lbl 857 `"Theaters and motion pictures"', add
label define ind1950_lbl 858 `"Bowling alleys, and billiard and pool parlors"', add
label define ind1950_lbl 859 `"Miscellaneous entertainment and recreation services"', add
label define ind1950_lbl 868 `"Medical and other health services, except hospitals"', add
label define ind1950_lbl 869 `"Hospitals"', add
label define ind1950_lbl 879 `"Legal services"', add
label define ind1950_lbl 888 `"Educational services"', add
label define ind1950_lbl 896 `"Welfare and religious services"', add
label define ind1950_lbl 897 `"Nonprofit membership organizations"', add
label define ind1950_lbl 898 `"Engineering and architectural services"', add
label define ind1950_lbl 899 `"Miscellaneous professional and related services"', add
label define ind1950_lbl 906 `"Postal service"', add
label define ind1950_lbl 916 `"Federal public administration"', add
label define ind1950_lbl 926 `"State public administration"', add
label define ind1950_lbl 936 `"Local public administration"', add
label define ind1950_lbl 997 `"Nonclassifiable"', add
label define ind1950_lbl 998 `"Industry not reported"', add
label values ind1950 ind1950_lbl

label define classwkr_lbl 00 `"NIU"'
label define classwkr_lbl 10 `"Self-employed"', add
label define classwkr_lbl 13 `"Self-employed, not incorporated"', add
label define classwkr_lbl 14 `"Self-employed, incorporated"', add
label define classwkr_lbl 20 `"Works for wages or salary"', add
label define classwkr_lbl 21 `"Wage/salary, private"', add
label define classwkr_lbl 22 `"Private, for profit"', add
label define classwkr_lbl 23 `"Private, nonprofit"', add
label define classwkr_lbl 24 `"Wage/salary, government"', add
label define classwkr_lbl 25 `"Federal government employee"', add
label define classwkr_lbl 26 `"Armed forces"', add
label define classwkr_lbl 27 `"State government employee"', add
label define classwkr_lbl 28 `"Local government employee"', add
label define classwkr_lbl 29 `"Unpaid family worker"', add
label define classwkr_lbl 99 `"Missing/Unknown"', add
label values classwkr classwkr_lbl

label define occ50ly_lbl 000 `"Accountants and auditors"'
label define occ50ly_lbl 001 `"Actors and actresses"', add
label define occ50ly_lbl 002 `"Airplane pilots and navigators"', add
label define occ50ly_lbl 003 `"Architects"', add
label define occ50ly_lbl 004 `"Artists and art teachers"', add
label define occ50ly_lbl 005 `"Athletes"', add
label define occ50ly_lbl 006 `"Authors"', add
label define occ50ly_lbl 007 `"Chemists"', add
label define occ50ly_lbl 008 `"Chiropractors"', add
label define occ50ly_lbl 009 `"Clergymen"', add
label define occ50ly_lbl 010 `"College presidents and deans"', add
label define occ50ly_lbl 012 `"Agricultural sciences"', add
label define occ50ly_lbl 013 `"Biological sciences"', add
label define occ50ly_lbl 014 `"Chemistry"', add
label define occ50ly_lbl 015 `"Economics"', add
label define occ50ly_lbl 016 `"Engineering"', add
label define occ50ly_lbl 017 `"Geology and geophysics"', add
label define occ50ly_lbl 018 `"Mathematics"', add
label define occ50ly_lbl 019 `"Medical sciences"', add
label define occ50ly_lbl 023 `"Physics"', add
label define occ50ly_lbl 024 `"Psychology"', add
label define occ50ly_lbl 025 `"Statistics"', add
label define occ50ly_lbl 026 `"Natural science (n.e.c.)"', add
label define occ50ly_lbl 027 `"Social sciences (n.e.c.)"', add
label define occ50ly_lbl 028 `"Nonscientific subjects"', add
label define occ50ly_lbl 029 `"Subject not specified"', add
label define occ50ly_lbl 031 `"Dancers and dancing teachers"', add
label define occ50ly_lbl 032 `"Dentists"', add
label define occ50ly_lbl 033 `"Designers"', add
label define occ50ly_lbl 034 `"Dieticians and nutritionists"', add
label define occ50ly_lbl 035 `"Draftsmen"', add
label define occ50ly_lbl 036 `"Editors and reporters"', add
label define occ50ly_lbl 041 `"Engineers, aeronautical"', add
label define occ50ly_lbl 042 `"Engineers, chemical"', add
label define occ50ly_lbl 043 `"Engineers, civil"', add
label define occ50ly_lbl 044 `"Engineers, electrical"', add
label define occ50ly_lbl 045 `"Engineers, industrial"', add
label define occ50ly_lbl 046 `"Engineers, mechanical"', add
label define occ50ly_lbl 047 `"Engineers, metallurgical, metallurgists"', add
label define occ50ly_lbl 048 `"Engineers, mining"', add
label define occ50ly_lbl 049 `"Engineers (n.e.c.)"', add
label define occ50ly_lbl 051 `"Entertainers (n.e.c.)"', add
label define occ50ly_lbl 052 `"Farm and home management advisors"', add
label define occ50ly_lbl 053 `"Foresters and conservationists"', add
label define occ50ly_lbl 054 `"Funeral directors and embalmers"', add
label define occ50ly_lbl 055 `"Lawyers and judges"', add
label define occ50ly_lbl 056 `"Librarians"', add
label define occ50ly_lbl 057 `"Musicians and music teachers"', add
label define occ50ly_lbl 058 `"Nurses, professional"', add
label define occ50ly_lbl 059 `"Nurses, student professional"', add
label define occ50ly_lbl 061 `"Agricultural scientists"', add
label define occ50ly_lbl 062 `"Biological scientists"', add
label define occ50ly_lbl 063 `"Geologists and geophysicists"', add
label define occ50ly_lbl 067 `"Mathematicians"', add
label define occ50ly_lbl 068 `"Physicists"', add
label define occ50ly_lbl 069 `"Miscellaneous natural scientists"', add
label define occ50ly_lbl 070 `"Optometrists"', add
label define occ50ly_lbl 071 `"Osteopaths"', add
label define occ50ly_lbl 072 `"Personnel and labor relations workers"', add
label define occ50ly_lbl 073 `"Pharmacists"', add
label define occ50ly_lbl 074 `"Photographers"', add
label define occ50ly_lbl 075 `"Physicians and surgeons"', add
label define occ50ly_lbl 076 `"Radio operators"', add
label define occ50ly_lbl 077 `"Recreation and group workers"', add
label define occ50ly_lbl 078 `"Religious workers"', add
label define occ50ly_lbl 079 `"Social and welfare workers, except group"', add
label define occ50ly_lbl 081 `"Economists"', add
label define occ50ly_lbl 082 `"Psychologists"', add
label define occ50ly_lbl 083 `"Statisticians and actuaries"', add
label define occ50ly_lbl 084 `"Miscellaneous social scientists"', add
label define occ50ly_lbl 091 `"Sports instructors and officials"', add
label define occ50ly_lbl 092 `"Surveyors"', add
label define occ50ly_lbl 093 `"Teachers (n.e.c.)"', add
label define occ50ly_lbl 094 `"Technicians, medical and dental"', add
label define occ50ly_lbl 095 `"Technicians, testing"', add
label define occ50ly_lbl 096 `"Technicians (n.e.c.)"', add
label define occ50ly_lbl 097 `"Therapists and healers (n.e.c.)"', add
label define occ50ly_lbl 098 `"Veterinarians"', add
label define occ50ly_lbl 099 `"Professional, technical and kindred workers (n.e.c.)"', add
label define occ50ly_lbl 100 `"Farmers (owners and tenants)"', add
label define occ50ly_lbl 123 `"Farm managers"', add
label define occ50ly_lbl 200 `"Buyers and department heads, store"', add
label define occ50ly_lbl 201 `"Buyers and shippers, farm products"', add
label define occ50ly_lbl 203 `"Conductors, railroad"', add
label define occ50ly_lbl 204 `"Credit men"', add
label define occ50ly_lbl 205 `"Floormen and floor managers, store"', add
label define occ50ly_lbl 210 `"Inspectors, public administration"', add
label define occ50ly_lbl 230 `"Managers and superintendents, building"', add
label define occ50ly_lbl 240 `"Officers, pilots, pursers and engineers, ship"', add
label define occ50ly_lbl 250 `"Officials and administrators (n.e.c.), public administration"', add
label define occ50ly_lbl 260 `"Officials, lodge, society, union, etc."', add
label define occ50ly_lbl 270 `"Postmasters"', add
label define occ50ly_lbl 280 `"Purchasing agents and buyers (n.e.c.)"', add
label define occ50ly_lbl 290 `"Managers, officials, and proprietors (n.e.c.)"', add
label define occ50ly_lbl 300 `"Agents (n.e.c.)"', add
label define occ50ly_lbl 301 `"Attendants and assistants, library"', add
label define occ50ly_lbl 302 `"Attendants, physicians and dentists office"', add
label define occ50ly_lbl 304 `"Baggagemen, transportation"', add
label define occ50ly_lbl 305 `"Bank tellers"', add
label define occ50ly_lbl 310 `"Bookkeepers"', add
label define occ50ly_lbl 320 `"Cashiers"', add
label define occ50ly_lbl 321 `"Collectors, bill and account"', add
label define occ50ly_lbl 322 `"Dispatchers and starters, vehicle"', add
label define occ50ly_lbl 325 `"Express messengers and railway mail clerks"', add
label define occ50ly_lbl 335 `"Mail carriers"', add
label define occ50ly_lbl 340 `"Messengers and office boys"', add
label define occ50ly_lbl 341 `"Office machine operators"', add
label define occ50ly_lbl 342 `"Shipping and receiving clerks"', add
label define occ50ly_lbl 350 `"Stenographers, typists, and secretaries"', add
label define occ50ly_lbl 360 `"Telegraph messengers"', add
label define occ50ly_lbl 365 `"Telegraph operators"', add
label define occ50ly_lbl 370 `"Telephone operators"', add
label define occ50ly_lbl 380 `"Ticket, station, and express agents"', add
label define occ50ly_lbl 390 `"Clerical and kindred workers (n.e.c.)"', add
label define occ50ly_lbl 400 `"Advertising agents and salesmen"', add
label define occ50ly_lbl 410 `"Auctioneers"', add
label define occ50ly_lbl 420 `"Demonstrators"', add
label define occ50ly_lbl 430 `"Hucksters and peddlers"', add
label define occ50ly_lbl 450 `"Insurance agents and brokers"', add
label define occ50ly_lbl 460 `"Newsboys"', add
label define occ50ly_lbl 470 `"Real estate agents and brokers"', add
label define occ50ly_lbl 480 `"Stock and bond salesmen"', add
label define occ50ly_lbl 490 `"Salesmen and sales clerks (n.e.c.)"', add
label define occ50ly_lbl 500 `"Bakers"', add
label define occ50ly_lbl 501 `"Blacksmiths"', add
label define occ50ly_lbl 502 `"Bookbinders"', add
label define occ50ly_lbl 503 `"Boilermakers"', add
label define occ50ly_lbl 504 `"Brickmasons, stonemasons, and tile setters"', add
label define occ50ly_lbl 505 `"Cabinetmakers"', add
label define occ50ly_lbl 510 `"Carpenters"', add
label define occ50ly_lbl 511 `"Cement and concrete finishers"', add
label define occ50ly_lbl 512 `"Compositors and typesetters"', add
label define occ50ly_lbl 513 `"Cranemen, derrickmen, and hoistmen"', add
label define occ50ly_lbl 514 `"Decorators and window dressers"', add
label define occ50ly_lbl 515 `"Electricians"', add
label define occ50ly_lbl 520 `"Electrotypers and stereotypers"', add
label define occ50ly_lbl 521 `"Engravers, except photoengravers"', add
label define occ50ly_lbl 522 `"Excavating, grading, and road machinery operators"', add
label define occ50ly_lbl 523 `"Foremen (n.e.c.)"', add
label define occ50ly_lbl 524 `"Forgemen and hammermen"', add
label define occ50ly_lbl 525 `"Furriers"', add
label define occ50ly_lbl 530 `"Glaziers"', add
label define occ50ly_lbl 531 `"Heat treaters, annealers, temperers"', add
label define occ50ly_lbl 532 `"Inspectors, scalers, and graders, log and lumber"', add
label define occ50ly_lbl 533 `"Inspectors (n.e.c.)"', add
label define occ50ly_lbl 534 `"Jewelers, watchmakers, goldsmiths, and silversmiths"', add
label define occ50ly_lbl 535 `"Job setters, metal"', add
label define occ50ly_lbl 540 `"Linemen and servicemen, telegraph, telephone, and power"', add
label define occ50ly_lbl 541 `"Locomotive engineers"', add
label define occ50ly_lbl 542 `"Locomotive firemen"', add
label define occ50ly_lbl 543 `"Loom fixers"', add
label define occ50ly_lbl 544 `"Machinists"', add
label define occ50ly_lbl 545 `"Mechanics and repairmen, airplane"', add
label define occ50ly_lbl 550 `"Mechanics and repairmen, automobile"', add
label define occ50ly_lbl 551 `"Mechanics and repairmen, office machine"', add
label define occ50ly_lbl 552 `"Mechanics and repairmen, radio and television"', add
label define occ50ly_lbl 553 `"Mechanics and repairmen, railroad and car shop"', add
label define occ50ly_lbl 554 `"Mechanics and repairmen (n.e.c.)"', add
label define occ50ly_lbl 555 `"Millers, grain, flour, feed, etc."', add
label define occ50ly_lbl 560 `"Millwrights"', add
label define occ50ly_lbl 561 `"Molders, metal"', add
label define occ50ly_lbl 562 `"Motion picture projectionists"', add
label define occ50ly_lbl 563 `"Opticians and lens grinders and polishers"', add
label define occ50ly_lbl 564 `"Painters, construction and maintenance"', add
label define occ50ly_lbl 565 `"Paperhangers"', add
label define occ50ly_lbl 570 `"Pattern and model makers, except paper"', add
label define occ50ly_lbl 571 `"Photoengravers and lithographers"', add
label define occ50ly_lbl 572 `"Piano and organ tuners and repairmen"', add
label define occ50ly_lbl 573 `"Plasterers"', add
label define occ50ly_lbl 574 `"Plumbers and pipe fitters"', add
label define occ50ly_lbl 575 `"Pressmen and plate printers, printing"', add
label define occ50ly_lbl 580 `"Rollers and roll hands, metal"', add
label define occ50ly_lbl 581 `"Roofers and slaters"', add
label define occ50ly_lbl 582 `"Shoemakers and repairers, except factory"', add
label define occ50ly_lbl 583 `"Stationary engineers"', add
label define occ50ly_lbl 584 `"Stone cutters and stone carvers"', add
label define occ50ly_lbl 585 `"Structural metal workers"', add
label define occ50ly_lbl 590 `"Tailors and tailoresses"', add
label define occ50ly_lbl 591 `"Tinsmiths, coppersmiths, and sheet metal workers"', add
label define occ50ly_lbl 592 `"Tool makers, and die makers and setters"', add
label define occ50ly_lbl 593 `"Upholsterers"', add
label define occ50ly_lbl 594 `"Craftsmen and kindred workers (n.e.c.)"', add
label define occ50ly_lbl 595 `"Members of the armed services"', add
label define occ50ly_lbl 600 `"Apprentice auto mechanics"', add
label define occ50ly_lbl 601 `"Apprentice bricklayers and masons"', add
label define occ50ly_lbl 602 `"Apprentice carpenters"', add
label define occ50ly_lbl 603 `"Apprentice electricians"', add
label define occ50ly_lbl 604 `"Apprentice machinists and toolmakers"', add
label define occ50ly_lbl 605 `"Apprentice mechanics, except auto"', add
label define occ50ly_lbl 610 `"Apprentice plumbers and pipe fitters"', add
label define occ50ly_lbl 611 `"Apprentices, building trades (n.e.c.)"', add
label define occ50ly_lbl 612 `"Apprentices, metalworking trades (n.e.c.)"', add
label define occ50ly_lbl 613 `"Apprentices, printing trades"', add
label define occ50ly_lbl 614 `"Apprentices, other specified trades"', add
label define occ50ly_lbl 615 `"Apprentices, trade not specified"', add
label define occ50ly_lbl 620 `"Asbestos and insulation workers"', add
label define occ50ly_lbl 621 `"Attendants, auto service and parking"', add
label define occ50ly_lbl 622 `"Blasters and powdermen"', add
label define occ50ly_lbl 623 `"Boatmen, canalmen, and lock keepers"', add
label define occ50ly_lbl 624 `"Brakemen, railroad"', add
label define occ50ly_lbl 625 `"Bus drivers"', add
label define occ50ly_lbl 630 `"Chainmen, rodmen, and axmen, surveying"', add
label define occ50ly_lbl 631 `"Conductors, bus and street railway"', add
label define occ50ly_lbl 632 `"Deliverymen and routemen"', add
label define occ50ly_lbl 633 `"Dressmakers and seamstresses, except factory"', add
label define occ50ly_lbl 634 `"Dyers"', add
label define occ50ly_lbl 635 `"Filers, grinders, and polishers, metal"', add
label define occ50ly_lbl 640 `"Fruit, nut, veg graders and packers, except factory"', add
label define occ50ly_lbl 641 `"Furnacemen, smeltermen and pourers"', add
label define occ50ly_lbl 642 `"Heaters, metal"', add
label define occ50ly_lbl 643 `"Laundry and dry cleaning operatives"', add
label define occ50ly_lbl 644 `"Meat cutters, except slaughter and packing house"', add
label define occ50ly_lbl 645 `"Milliners"', add
label define occ50ly_lbl 650 `"Mine operatives and laborers"', add
label define occ50ly_lbl 660 `"Motormen, mine, factory, logging camp, etc."', add
label define occ50ly_lbl 661 `"Motormen, street, subway, and elevated railway"', add
label define occ50ly_lbl 662 `"Oilers and greaser, except auto"', add
label define occ50ly_lbl 670 `"Painters, except construction or maintenance"', add
label define occ50ly_lbl 671 `"Photographic process workers"', add
label define occ50ly_lbl 672 `"Power station operators"', add
label define occ50ly_lbl 673 `"Sailors and deck hands"', add
label define occ50ly_lbl 674 `"Sawyers"', add
label define occ50ly_lbl 675 `"Spinners, textile"', add
label define occ50ly_lbl 680 `"Stationary firemen"', add
label define occ50ly_lbl 681 `"Switchmen, railroad"', add
label define occ50ly_lbl 682 `"Taxicab drivers and chauffers"', add
label define occ50ly_lbl 683 `"Truck and tractor drivers"', add
label define occ50ly_lbl 684 `"Weavers, textile"', add
label define occ50ly_lbl 685 `"Welders and flame cutters"', add
label define occ50ly_lbl 690 `"Operative and kindred workers (n.e.c.)"', add
label define occ50ly_lbl 700 `"Housekeepers, private household"', add
label define occ50ly_lbl 710 `"Laundressses, private household"', add
label define occ50ly_lbl 720 `"Private household workers (n.e.c.)"', add
label define occ50ly_lbl 730 `"Attendants, hospital and other institution"', add
label define occ50ly_lbl 731 `"Attendants, professional and personal service (n.e.c.)"', add
label define occ50ly_lbl 732 `"Attendants, recreation and amusement"', add
label define occ50ly_lbl 740 `"Barbers, beauticians, and manicurists"', add
label define occ50ly_lbl 750 `"Bartenders"', add
label define occ50ly_lbl 751 `"Bootblacks"', add
label define occ50ly_lbl 752 `"Boarding and lodging house keepers"', add
label define occ50ly_lbl 753 `"Charwomen and cleaners"', add
label define occ50ly_lbl 754 `"Cooks, except private household"', add
label define occ50ly_lbl 760 `"Counter and fountain workers"', add
label define occ50ly_lbl 761 `"Elevator operators"', add
label define occ50ly_lbl 762 `"Firemen, fire protection"', add
label define occ50ly_lbl 763 `"Guards, watchmen, and doorkeepers"', add
label define occ50ly_lbl 764 `"Housekeepers and stewards, except private household"', add
label define occ50ly_lbl 770 `"Janitors and sextons"', add
label define occ50ly_lbl 771 `"Marshals and constables"', add
label define occ50ly_lbl 772 `"Midwives"', add
label define occ50ly_lbl 773 `"Policemen and detectives"', add
label define occ50ly_lbl 780 `"Porters"', add
label define occ50ly_lbl 781 `"Practical nurses"', add
label define occ50ly_lbl 782 `"Sheriffs and bailiffs"', add
label define occ50ly_lbl 783 `"Ushers, recreation and amusement"', add
label define occ50ly_lbl 784 `"Waiters and waitresses"', add
label define occ50ly_lbl 785 `"Watchmen (crossing) and bridge tenders"', add
label define occ50ly_lbl 790 `"Service workers, except private household (n.e.c.)"', add
label define occ50ly_lbl 810 `"Farm foremen"', add
label define occ50ly_lbl 820 `"Farm laborers, wage workers"', add
label define occ50ly_lbl 830 `"Farm laborers, unpaid family workers"', add
label define occ50ly_lbl 840 `"Farm service laborers, self-employed"', add
label define occ50ly_lbl 910 `"Fishermen and oystermen"', add
label define occ50ly_lbl 920 `"Garage laborers and car washers and greasers"', add
label define occ50ly_lbl 930 `"Gardeners, except farm, and groundskeepers"', add
label define occ50ly_lbl 940 `"Longshoremen and stevedores"', add
label define occ50ly_lbl 950 `"Lumbermen, raftsmen, and woodchoppers"', add
label define occ50ly_lbl 960 `"Teamsters"', add
label define occ50ly_lbl 970 `"Laborers (n.e.c.)"', add
label define occ50ly_lbl 997 `"Unknown"', add
label define occ50ly_lbl 999 `"Unemployed- last worked over x years ago"', add
label values occ50ly occ50ly_lbl

label define ind50ly_lbl 000 `"NIU"'
label define ind50ly_lbl 105 `"Agriculture"', add
label define ind50ly_lbl 116 `"Forestry"', add
label define ind50ly_lbl 126 `"Fisheries"', add
label define ind50ly_lbl 206 `"Metal mining"', add
label define ind50ly_lbl 216 `"Coal mining"', add
label define ind50ly_lbl 226 `"Crude petroleum and natural gas extraction"', add
label define ind50ly_lbl 236 `"Nonmetallic mining and quarrying, except fuel"', add
label define ind50ly_lbl 239 `"Mining not specified"', add
label define ind50ly_lbl 246 `"Construction"', add
label define ind50ly_lbl 306 `"Logging"', add
label define ind50ly_lbl 307 `"Sawmills, planing mills, and millwork"', add
label define ind50ly_lbl 308 `"Misc wood products"', add
label define ind50ly_lbl 309 `"Furniture and fixtures"', add
label define ind50ly_lbl 316 `"Glass and glass products"', add
label define ind50ly_lbl 317 `"Cement, concrete, gypsum and plaster products"', add
label define ind50ly_lbl 318 `"Structural clay products"', add
label define ind50ly_lbl 319 `"Pottery and related products"', add
label define ind50ly_lbl 326 `"Miscellaneous nonmetallic mineral and stone products"', add
label define ind50ly_lbl 336 `"Blast furnaces, steel works, and rolling mills"', add
label define ind50ly_lbl 337 `"Other primary iron and steel industries"', add
label define ind50ly_lbl 338 `"Primary nonferrous industries"', add
label define ind50ly_lbl 346 `"Fabricated steel products"', add
label define ind50ly_lbl 347 `"Fabricated nonferrous metal products"', add
label define ind50ly_lbl 348 `"Not specified metal industries"', add
label define ind50ly_lbl 356 `"Agricultural machinery and tractors"', add
label define ind50ly_lbl 357 `"Office and store machines and devices"', add
label define ind50ly_lbl 358 `"Miscellaneous machinery"', add
label define ind50ly_lbl 367 `"Electrical machinery, equipment, and supplies"', add
label define ind50ly_lbl 376 `"Motor vehicles and motor vehicle equipment"', add
label define ind50ly_lbl 377 `"Aircraft and parts"', add
label define ind50ly_lbl 378 `"Ship and boat building and repairing"', add
label define ind50ly_lbl 379 `"Railroad and miscellaneous transportation equipment"', add
label define ind50ly_lbl 386 `"Professional equipment and supplies"', add
label define ind50ly_lbl 387 `"Photographic equipment and supplies"', add
label define ind50ly_lbl 388 `"Watches, clocks, and clockwork-operated devices"', add
label define ind50ly_lbl 399 `"Miscellaneous manufacturing industries"', add
label define ind50ly_lbl 406 `"Meat products"', add
label define ind50ly_lbl 407 `"Dairy products"', add
label define ind50ly_lbl 408 `"Canning and preserving fruits, vegetables, and seafoods"', add
label define ind50ly_lbl 409 `"Grain-mill products"', add
label define ind50ly_lbl 416 `"Bakery products"', add
label define ind50ly_lbl 417 `"Confectionery and related products"', add
label define ind50ly_lbl 418 `"Beverage industries"', add
label define ind50ly_lbl 419 `"Miscellaneous food preparations and kindred products"', add
label define ind50ly_lbl 426 `"Not specified food industries"', add
label define ind50ly_lbl 429 `"Tobacco manufactures"', add
label define ind50ly_lbl 436 `"Knitting mills"', add
label define ind50ly_lbl 437 `"Dyeing and finishing textiles, except knit goods"', add
label define ind50ly_lbl 438 `"Carpets, rugs, and other floor coverings"', add
label define ind50ly_lbl 439 `"Yarn, thread, and fabric mills"', add
label define ind50ly_lbl 446 `"Miscellaneous textile mill products"', add
label define ind50ly_lbl 448 `"Apparel and accessories"', add
label define ind50ly_lbl 449 `"Miscellaneous fabricated textile products"', add
label define ind50ly_lbl 456 `"Pulp, paper, and paperboard mills"', add
label define ind50ly_lbl 457 `"Paperboard containers and boxes"', add
label define ind50ly_lbl 458 `"Miscellaneous paper and pulp products"', add
label define ind50ly_lbl 459 `"Printing, publishing, and allied industries"', add
label define ind50ly_lbl 466 `"Synthetic fibers"', add
label define ind50ly_lbl 467 `"Drugs and medicines"', add
label define ind50ly_lbl 468 `"Paints, varnishes, and related products"', add
label define ind50ly_lbl 469 `"Miscellaneous chemicals and allied products"', add
label define ind50ly_lbl 476 `"Petroleum refining"', add
label define ind50ly_lbl 477 `"Miscellaneous petroleum and coal products"', add
label define ind50ly_lbl 478 `"Rubber products"', add
label define ind50ly_lbl 487 `"Leather: tanned, curried, and finished"', add
label define ind50ly_lbl 488 `"Footwear, except rubber"', add
label define ind50ly_lbl 489 `"Leather products, except footwear"', add
label define ind50ly_lbl 499 `"Not specified manufacturing industries"', add
label define ind50ly_lbl 506 `"Railroads and railway express service"', add
label define ind50ly_lbl 516 `"Street railways and bus lines"', add
label define ind50ly_lbl 526 `"Trucking service"', add
label define ind50ly_lbl 527 `"Warehousing and storage"', add
label define ind50ly_lbl 536 `"Taxicab service"', add
label define ind50ly_lbl 546 `"Water transportation"', add
label define ind50ly_lbl 556 `"Air transportation"', add
label define ind50ly_lbl 567 `"Petroleum and gasoline pipe lines"', add
label define ind50ly_lbl 568 `"Services incidental to transportation"', add
label define ind50ly_lbl 578 `"Telephone"', add
label define ind50ly_lbl 579 `"Telegraph"', add
label define ind50ly_lbl 586 `"Electric light and power"', add
label define ind50ly_lbl 587 `"Gas and steam supply systems"', add
label define ind50ly_lbl 588 `"Electric-gas utilities"', add
label define ind50ly_lbl 596 `"Water supply"', add
label define ind50ly_lbl 597 `"Sanitary services"', add
label define ind50ly_lbl 598 `"Other and not specified utilities"', add
label define ind50ly_lbl 606 `"Motor vehicles and equipment"', add
label define ind50ly_lbl 607 `"Drugs, chemicals, and allied products"', add
label define ind50ly_lbl 608 `"Dry goods apparel"', add
label define ind50ly_lbl 609 `"Food and related products"', add
label define ind50ly_lbl 616 `"Electrical goods, hardware, and plumbing equipment"', add
label define ind50ly_lbl 617 `"Machinery, equipment, and supplies"', add
label define ind50ly_lbl 618 `"Petroleum products"', add
label define ind50ly_lbl 619 `"Farm products--raw materials"', add
label define ind50ly_lbl 626 `"Miscellaneous wholesale trade"', add
label define ind50ly_lbl 627 `"Not specified wholesale trade"', add
label define ind50ly_lbl 636 `"Food stores, except dairy products"', add
label define ind50ly_lbl 637 `"Dairy products stores and milk retailing"', add
label define ind50ly_lbl 646 `"General merchandise stores"', add
label define ind50ly_lbl 647 `"Five and ten cent stores"', add
label define ind50ly_lbl 656 `"Apparel and accessories stores, except shoe"', add
label define ind50ly_lbl 657 `"Shoe stores"', add
label define ind50ly_lbl 658 `"Furniture and house furnishing stores"', add
label define ind50ly_lbl 659 `"Household appliance and radio stores"', add
label define ind50ly_lbl 667 `"Motor vehicles and accessories retailing"', add
label define ind50ly_lbl 668 `"Gasoline service stations"', add
label define ind50ly_lbl 669 `"Drug stores"', add
label define ind50ly_lbl 679 `"Eating and drinking places"', add
label define ind50ly_lbl 686 `"Hardware and farm implement stores"', add
label define ind50ly_lbl 687 `"Lumber and building material retailing"', add
label define ind50ly_lbl 688 `"Liquor stores"', add
label define ind50ly_lbl 689 `"Retail florists"', add
label define ind50ly_lbl 696 `"Jewelry stores"', add
label define ind50ly_lbl 697 `"Fuel and ice retailing"', add
label define ind50ly_lbl 698 `"Miscellaneous retail stores"', add
label define ind50ly_lbl 699 `"Not specified retail trade"', add
label define ind50ly_lbl 716 `"Banking and credit agencies"', add
label define ind50ly_lbl 726 `"Security and commodity brokerage and investment companies"', add
label define ind50ly_lbl 736 `"Insurance"', add
label define ind50ly_lbl 746 `"Real estate"', add
label define ind50ly_lbl 806 `"Advertising"', add
label define ind50ly_lbl 807 `"Accounting, auditing, and bookkeeping services"', add
label define ind50ly_lbl 808 `"Miscellaneous business services"', add
label define ind50ly_lbl 816 `"Auto repair services and garages"', add
label define ind50ly_lbl 817 `"Miscellaneous repair services"', add
label define ind50ly_lbl 826 `"Private households"', add
label define ind50ly_lbl 836 `"Hotels and lodging places"', add
label define ind50ly_lbl 846 `"Laundering, cleaning, and dyeing services"', add
label define ind50ly_lbl 847 `"Dressmaking shops"', add
label define ind50ly_lbl 848 `"Shoe repair shops"', add
label define ind50ly_lbl 849 `"Miscellaneous personal services"', add
label define ind50ly_lbl 856 `"Radio broadcasting and television"', add
label define ind50ly_lbl 857 `"Theaters and motion pictures"', add
label define ind50ly_lbl 858 `"Bowling alleys, and billiard and pool parlors"', add
label define ind50ly_lbl 859 `"Miscellaneous entertainment and recreation services"', add
label define ind50ly_lbl 868 `"Medical and other health services, except hospitals"', add
label define ind50ly_lbl 869 `"Hospitals"', add
label define ind50ly_lbl 879 `"Legal services"', add
label define ind50ly_lbl 888 `"Educational services"', add
label define ind50ly_lbl 896 `"Welfare and religious services"', add
label define ind50ly_lbl 897 `"Nonprofit membership organizations"', add
label define ind50ly_lbl 898 `"Engineering and architectural services"', add
label define ind50ly_lbl 899 `"Miscellaneous professional and related services"', add
label define ind50ly_lbl 906 `"Postal service"', add
label define ind50ly_lbl 916 `"Federal public administration"', add
label define ind50ly_lbl 926 `"State public administration"', add
label define ind50ly_lbl 936 `"Local public administration"', add
label define ind50ly_lbl 997 `"Nonclassifiable"', add
label define ind50ly_lbl 998 `"Industry not reported"', add
label values ind50ly ind50ly_lbl

label define classwly_lbl 00 `"NIU"'
label define classwly_lbl 10 `"Self-employed"', add
label define classwly_lbl 13 `"Self-employed, not incorporated"', add
label define classwly_lbl 14 `"Self-employed, incorporated"', add
label define classwly_lbl 20 `"Works for wages or salary"', add
label define classwly_lbl 22 `"Wage/salary, private"', add
label define classwly_lbl 24 `"Wage/salary, government"', add
label define classwly_lbl 25 `"Federal government employee"', add
label define classwly_lbl 27 `"State government employee"', add
label define classwly_lbl 28 `"Local government employee"', add
label define classwly_lbl 29 `"Unpaid family worker"', add
label define classwly_lbl 99 `"Missing/Unknown"', add
label values classwly classwly_lbl

label define wkswork2_lbl 0 `"NIU"'
label define wkswork2_lbl 1 `"1-13 weeks"', add
label define wkswork2_lbl 2 `"14-26 weeks"', add
label define wkswork2_lbl 3 `"27-39 weeks"', add
label define wkswork2_lbl 4 `"40-47 weeks"', add
label define wkswork2_lbl 5 `"48-49 weeks"', add
label define wkswork2_lbl 6 `"50-52 weeks"', add
label define wkswork2_lbl 9 `"Missing data"', add
label values wkswork2 wkswork2_lbl

label define wksunem2_lbl 0 `"None"'
label define wksunem2_lbl 1 `"1-4 weeks"', add
label define wksunem2_lbl 2 `"5-10 weeks"', add
label define wksunem2_lbl 3 `"11-14 weeks"', add
label define wksunem2_lbl 4 `"15-26 weeks"', add
label define wksunem2_lbl 5 `"27-39 weeks"', add
label define wksunem2_lbl 6 `"40+ weeks"', add
label define wksunem2_lbl 7 `"Over 26 weeks (1962-1967)"', add
label define wksunem2_lbl 8 `"Missing/Unknown"', add
label define wksunem2_lbl 9 `"NIU"', add
label values wksunem2 wksunem2_lbl

label define absent_lbl 0 `"NIU"'
label define absent_lbl 1 `"No"', add
label define absent_lbl 2 `"Yes, laid off"', add
label define absent_lbl 3 `"Yes, other reason (vacation, illness, labor dispute)"', add
label values absent absent_lbl

label define durunem2_lbl 00 `"0"'
label define durunem2_lbl 01 `"1"', add
label define durunem2_lbl 02 `"2"', add
label define durunem2_lbl 03 `"3"', add
label define durunem2_lbl 04 `"4"', add
label define durunem2_lbl 05 `"5"', add
label define durunem2_lbl 06 `"6"', add
label define durunem2_lbl 07 `"7-10"', add
label define durunem2_lbl 08 `"11-14"', add
label define durunem2_lbl 09 `"15-18"', add
label define durunem2_lbl 10 `"19-22"', add
label define durunem2_lbl 11 `"23-26"', add
label define durunem2_lbl 12 `"27-34"', add
label define durunem2_lbl 13 `"35-42"', add
label define durunem2_lbl 14 `"43-50"', add
label define durunem2_lbl 15 `"51-52"', add
label define durunem2_lbl 16 `"Over 52 weeks"', add
label define durunem2_lbl 99 `"NIU"', add
label values durunem2 durunem2_lbl

label define fullpart_lbl 0 `"NIU"'
label define fullpart_lbl 1 `"Full-time"', add
label define fullpart_lbl 2 `"Part-time"', add
label define fullpart_lbl 9 `"Unknown"', add
label values fullpart fullpart_lbl

label define nwlookwk_lbl 00 `"Did not look for work/wasn't on layoff"'
label define nwlookwk_lbl 01 `"1 week"', add
label define nwlookwk_lbl 02 `"2 weeks"', add
label define nwlookwk_lbl 03 `"3 weeks"', add
label define nwlookwk_lbl 04 `"4 weeks"', add
label define nwlookwk_lbl 05 `"5 weeks"', add
label define nwlookwk_lbl 06 `"6 weeks"', add
label define nwlookwk_lbl 07 `"7 weeks"', add
label define nwlookwk_lbl 08 `"8 weeks"', add
label define nwlookwk_lbl 09 `"9 weeks"', add
label define nwlookwk_lbl 10 `"10 weeks"', add
label define nwlookwk_lbl 11 `"11 weeks"', add
label define nwlookwk_lbl 12 `"12 weeks"', add
label define nwlookwk_lbl 13 `"13 weeks"', add
label define nwlookwk_lbl 14 `"14 weeks"', add
label define nwlookwk_lbl 15 `"15 weeks"', add
label define nwlookwk_lbl 16 `"16 weeks"', add
label define nwlookwk_lbl 17 `"17 weeks"', add
label define nwlookwk_lbl 18 `"18 weeks"', add
label define nwlookwk_lbl 19 `"19 weeks"', add
label define nwlookwk_lbl 20 `"20 weeks"', add
label define nwlookwk_lbl 21 `"21 weeks"', add
label define nwlookwk_lbl 22 `"22 weeks"', add
label define nwlookwk_lbl 23 `"23 weeks"', add
label define nwlookwk_lbl 24 `"24 weeks"', add
label define nwlookwk_lbl 25 `"25 weeks"', add
label define nwlookwk_lbl 26 `"26 weeks"', add
label define nwlookwk_lbl 27 `"27 weeks"', add
label define nwlookwk_lbl 28 `"28 weeks"', add
label define nwlookwk_lbl 29 `"29 weeks"', add
label define nwlookwk_lbl 30 `"30 weeks"', add
label define nwlookwk_lbl 31 `"31 weeks"', add
label define nwlookwk_lbl 32 `"32 weeks"', add
label define nwlookwk_lbl 33 `"33 weeks"', add
label define nwlookwk_lbl 34 `"34 weeks"', add
label define nwlookwk_lbl 35 `"35 weeks"', add
label define nwlookwk_lbl 36 `"36 weeks"', add
label define nwlookwk_lbl 37 `"37 weeks"', add
label define nwlookwk_lbl 38 `"38 weeks"', add
label define nwlookwk_lbl 39 `"39 weeks"', add
label define nwlookwk_lbl 40 `"40 weeks"', add
label define nwlookwk_lbl 41 `"41 weeks"', add
label define nwlookwk_lbl 42 `"42 weeks"', add
label define nwlookwk_lbl 43 `"43 weeks"', add
label define nwlookwk_lbl 44 `"44 weeks"', add
label define nwlookwk_lbl 45 `"45 weeks"', add
label define nwlookwk_lbl 46 `"46 weeks"', add
label define nwlookwk_lbl 47 `"47 weeks"', add
label define nwlookwk_lbl 48 `"48 weeks"', add
label define nwlookwk_lbl 49 `"49 weeks"', add
label define nwlookwk_lbl 50 `"50 weeks"', add
label define nwlookwk_lbl 51 `"51 weeks"', add
label define nwlookwk_lbl 52 `"52 weeks"', add
label define nwlookwk_lbl 99 `"NIU"', add
label values nwlookwk nwlookwk_lbl

label define paidhour_lbl 0 `"NIU"'
label define paidhour_lbl 1 `"No"', add
label define paidhour_lbl 2 `"Yes"', add
label define paidhour_lbl 6 `"Refused"', add
label define paidhour_lbl 7 `"Don't Know"', add
label values paidhour paidhour_lbl

label define pension_lbl 0 `"NIU"'
label define pension_lbl 1 `"No pension plan at work"', add
label define pension_lbl 2 `"Pension plan at work, but not included"', add
label define pension_lbl 3 `"Included in pension plan at work"', add
label values pension pension_lbl

label define union_lbl 0 `"NIU"'
label define union_lbl 1 `"No union coverage"', add
label define union_lbl 2 `"Member of labor union"', add
label define union_lbl 3 `"Covered by union but not a member"', add
label values union union_lbl

label define whyunemp_lbl 0 `"NIU"'
label define whyunemp_lbl 1 `"Job loser - on layoff"', add
label define whyunemp_lbl 2 `"Other job loser"', add
label define whyunemp_lbl 3 `"Temporary job ended"', add
label define whyunemp_lbl 4 `"Job leaver"', add
label define whyunemp_lbl 5 `"Re-entrant"', add
label define whyunemp_lbl 6 `"New entrant"', add
label values whyunemp whyunemp_lbl

label define firmsize_lbl 0 `"NIU"'
label define firmsize_lbl 1 `"Under 10"', add
label define firmsize_lbl 2 `"10 to 24"', add
label define firmsize_lbl 3 `"Under 25"', add
label define firmsize_lbl 4 `"10 to 49"', add
label define firmsize_lbl 5 `"25 to 99"', add
label define firmsize_lbl 6 `"50 to 99"', add
label define firmsize_lbl 7 `"100 to 499"', add
label define firmsize_lbl 8 `"500 to 999"', add
label define firmsize_lbl 9 `"1000+"', add
label values firmsize firmsize_lbl

label define whyabsnt_lbl 00 `"NIU"'
label define whyabsnt_lbl 01 `"On temporary layoff (under 30 days)"', add
label define whyabsnt_lbl 02 `"On indefinite layoff (30+ days)"', add
label define whyabsnt_lbl 03 `"Slack work/business conditions"', add
label define whyabsnt_lbl 04 `"Waiting for a new job to begin"', add
label define whyabsnt_lbl 05 `"Vacation/personal days"', add
label define whyabsnt_lbl 06 `"Own illness/injury/medical problems"', add
label define whyabsnt_lbl 07 `"Child care problems"', add
label define whyabsnt_lbl 08 `"Other family/personal obligation"', add
label define whyabsnt_lbl 09 `"Maternity/paternity leave"', add
label define whyabsnt_lbl 10 `"Labor dispute"', add
label define whyabsnt_lbl 11 `"Weather affected job"', add
label define whyabsnt_lbl 12 `"School/training"', add
label define whyabsnt_lbl 13 `"Civic/military duty"', add
label define whyabsnt_lbl 14 `"Does not work in the business"', add
label define whyabsnt_lbl 15 `"Other"', add
label values whyabsnt whyabsnt_lbl

label define wantjob_lbl 0 `"NIU"'
label define wantjob_lbl 1 `"No"', add
label define wantjob_lbl 2 `"Yes"', add
label define wantjob_lbl 3 `"Maybe, it depends"', add
label define wantjob_lbl 4 `"Do not know"', add
label define wantjob_lbl 9 `"Unknown"', add
label values wantjob wantjob_lbl

label define whyptly_lbl 0 `"NIU"'
label define whyptly_lbl 1 `"Could not find full time job"', add
label define whyptly_lbl 2 `"Wanted part time"', add
label define whyptly_lbl 3 `"Slack work"', add
label define whyptly_lbl 4 `"Other"', add
label values whyptly whyptly_lbl

label define whyptlwk_lbl 000 `"NIU"'
label define whyptlwk_lbl 001 `"No response"', add
label define whyptlwk_lbl 010 `"Slack work, business conditions"', add
label define whyptlwk_lbl 011 `"Material shortage"', add
label define whyptlwk_lbl 012 `"Plant or machine repairs"', add
label define whyptlwk_lbl 020 `"Seasonal work"', add
label define whyptlwk_lbl 030 `"Weather affected job"', add
label define whyptlwk_lbl 040 `"Labor dispute"', add
label define whyptlwk_lbl 050 `"job started/ended during week"', add
label define whyptlwk_lbl 051 `"New job started"', add
label define whyptlwk_lbl 052 `"Job terminated"', add
label define whyptlwk_lbl 060 `"Could only find part-time"', add
label define whyptlwk_lbl 070 `"Not want full time work"', add
label define whyptlwk_lbl 071 `"Retired/SS limit on earnings"', add
label define whyptlwk_lbl 080 `"Full time work week under 35 hours"', add
label define whyptlwk_lbl 081 `"Full time peak season only"', add
label define whyptlwk_lbl 090 `"Holiday"', add
label define whyptlwk_lbl 100 `"Own illness"', add
label define whyptlwk_lbl 101 `"Health/medical limitation"', add
label define whyptlwk_lbl 110 `"On vacation"', add
label define whyptlwk_lbl 111 `"Vacation/personal day"', add
label define whyptlwk_lbl 120 `"Too busy with house, school, etc"', add
label define whyptlwk_lbl 121 `"Child care problems"', add
label define whyptlwk_lbl 122 `"Other family/personal obligations"', add
label define whyptlwk_lbl 123 `"School/training"', add
label define whyptlwk_lbl 124 `"Civic/military duty"', add
label define whyptlwk_lbl 130 `"Other"', add
label values whyptlwk whyptlwk_lbl

label define usftptlw_lbl 0 `"NIU"'
label define usftptlw_lbl 1 `"No"', add
label define usftptlw_lbl 2 `"Yes"', add
label values usftptlw usftptlw_lbl

label define payifabs_lbl 0 `"NIU"'
label define payifabs_lbl 1 `"Not Paid"', add
label define payifabs_lbl 2 `"Paid"', add
label define payifabs_lbl 3 `"Self-employed"', add
label values payifabs payifabs_lbl

label define wnftlook_lbl 00 `"NIU"'
label define wnftlook_lbl 10 `"Less than 5 years ago"', add
label define wnftlook_lbl 11 `"In the last twelve months"', add
label define wnftlook_lbl 12 `"One to five years ago"', add
label define wnftlook_lbl 20 `"More than I2 months ago, unspecified"', add
label define wnftlook_lbl 30 `"More than 5 years ago"', add
label define wnftlook_lbl 40 `"Never worked"', add
label define wnftlook_lbl 41 `"Never worked full-time 2+ weeks"', add
label define wnftlook_lbl 42 `"Never worked at all"', add
label values wnftlook wnftlook_lbl

label define wnlwnilf_lbl 00 `"NIU"'
label define wnlwnilf_lbl 10 `"Within past 12 months"', add
label define wnlwnilf_lbl 20 `"More than 12 months ago"', add
label define wnlwnilf_lbl 21 `"1 up to 2 years ago"', add
label define wnlwnilf_lbl 22 `"2 up to 3 years ago"', add
label define wnlwnilf_lbl 23 `"3 up to 4 years ago"', add
label define wnlwnilf_lbl 24 `"4 up to 5 years ago"', add
label define wnlwnilf_lbl 25 `"5 or more years ago"', add
label define wnlwnilf_lbl 30 `"Never worked"', add
label values wnlwnilf wnlwnilf_lbl

label define strechlk_lbl 0 `"NIU"'
label define strechlk_lbl 1 `"1 stretch"', add
label define strechlk_lbl 2 `"More than 1 stretch"', add
label define strechlk_lbl 3 `"2 stretches"', add
label define strechlk_lbl 4 `"3+ stretches"', add
label define strechlk_lbl 9 `"Not specified"', add
label values strechlk strechlk_lbl

label define whynwly_lbl 0 `"NIU"'
label define whynwly_lbl 1 `"Could not find work"', add
label define whynwly_lbl 2 `"Ill or disabled"', add
label define whynwly_lbl 3 `"Taking care of home/family"', add
label define whynwly_lbl 4 `"Going to school"', add
label define whynwly_lbl 5 `"Retired"', add
label define whynwly_lbl 6 `"In Armed Forces"', add
label define whynwly_lbl 7 `"Other"', add
label define whynwly_lbl 9 `"Unknown/missing"', add
label values whynwly whynwly_lbl

label define actnlfly_lbl 00 `"NIU"'
label define actnlfly_lbl 10 `"Ill or disabled"', add
label define actnlfly_lbl 20 `"Taking care of home/family"', add
label define actnlfly_lbl 30 `"Going to school"', add
label define actnlfly_lbl 40 `"Retired"', add
label define actnlfly_lbl 50 `"Other"', add
label define actnlfly_lbl 51 `"Looking for work"', add
label define actnlfly_lbl 52 `"No work available"', add
label define actnlfly_lbl 53 `"Doing unpaid work"', add
label define actnlfly_lbl 54 `"In Armed Forces"', add
label values actnlfly actnlfly_lbl

label define ptweeks_lbl 00 `"NIU"'
label define ptweeks_lbl 01 `"1 week"', add
label define ptweeks_lbl 02 `"2 weeks"', add
label define ptweeks_lbl 03 `"3 weeks"', add
label define ptweeks_lbl 04 `"4 weeks"', add
label define ptweeks_lbl 05 `"5 weeks"', add
label define ptweeks_lbl 06 `"6 weeks"', add
label define ptweeks_lbl 07 `"7 weeks"', add
label define ptweeks_lbl 08 `"8 weeks"', add
label define ptweeks_lbl 09 `"9 weeks"', add
label define ptweeks_lbl 10 `"10 weeks"', add
label define ptweeks_lbl 11 `"11 weeks"', add
label define ptweeks_lbl 12 `"12 weeks"', add
label define ptweeks_lbl 13 `"13 weeks"', add
label define ptweeks_lbl 14 `"14 weeks"', add
label define ptweeks_lbl 15 `"15 weeks"', add
label define ptweeks_lbl 16 `"16 weeks"', add
label define ptweeks_lbl 17 `"17 weeks"', add
label define ptweeks_lbl 18 `"18 weeks"', add
label define ptweeks_lbl 19 `"19 weeks"', add
label define ptweeks_lbl 20 `"20 weeks"', add
label define ptweeks_lbl 21 `"21 weeks"', add
label define ptweeks_lbl 22 `"22 weeks"', add
label define ptweeks_lbl 23 `"23 weeks"', add
label define ptweeks_lbl 24 `"24 weeks"', add
label define ptweeks_lbl 25 `"25 weeks"', add
label define ptweeks_lbl 26 `"26 weeks"', add
label define ptweeks_lbl 27 `"27 weeks"', add
label define ptweeks_lbl 28 `"28 weeks"', add
label define ptweeks_lbl 29 `"29 weeks"', add
label define ptweeks_lbl 30 `"30 weeks"', add
label define ptweeks_lbl 31 `"31 weeks"', add
label define ptweeks_lbl 32 `"32 weeks"', add
label define ptweeks_lbl 33 `"33 weeks"', add
label define ptweeks_lbl 34 `"34 weeks"', add
label define ptweeks_lbl 35 `"35 weeks"', add
label define ptweeks_lbl 36 `"36 weeks"', add
label define ptweeks_lbl 37 `"37 weeks"', add
label define ptweeks_lbl 38 `"38 weeks"', add
label define ptweeks_lbl 39 `"39 weeks"', add
label define ptweeks_lbl 40 `"40 weeks"', add
label define ptweeks_lbl 41 `"41 weeks"', add
label define ptweeks_lbl 42 `"42 weeks"', add
label define ptweeks_lbl 43 `"43 weeks"', add
label define ptweeks_lbl 44 `"44 weeks"', add
label define ptweeks_lbl 45 `"45 weeks"', add
label define ptweeks_lbl 46 `"46 weeks"', add
label define ptweeks_lbl 47 `"47 weeks"', add
label define ptweeks_lbl 48 `"48 weeks"', add
label define ptweeks_lbl 49 `"49 weeks"', add
label define ptweeks_lbl 50 `"50 weeks"', add
label define ptweeks_lbl 51 `"51 weeks"', add
label define ptweeks_lbl 52 `"52 weeks"', add
label values ptweeks ptweeks_lbl

label define gotvdisa_lbl 0 `"NIU"'
label define gotvdisa_lbl 1 `"No"', add
label define gotvdisa_lbl 2 `"Yes"', add
label values gotvdisa gotvdisa_lbl

label define gotveduc_lbl 0 `"NIU"'
label define gotveduc_lbl 1 `"No"', add
label define gotveduc_lbl 2 `"Yes"', add
label values gotveduc gotveduc_lbl

label define gotvothe_lbl 0 `"NIU"'
label define gotvothe_lbl 1 `"No"', add
label define gotvothe_lbl 2 `"Yes"', add
label values gotvothe gotvothe_lbl

label define gotvpens_lbl 0 `"NIU"'
label define gotvpens_lbl 1 `"No"', add
label define gotvpens_lbl 2 `"Yes"', add
label values gotvpens gotvpens_lbl

label define gotvsurv_lbl 0 `"NIU"'
label define gotvsurv_lbl 1 `"No"', add
label define gotvsurv_lbl 2 `"Yes"', add
label values gotvsurv gotvsurv_lbl

label define srcdisa1_lbl 00 `"NIU"'
label define srcdisa1_lbl 01 `"Workers compensation"', add
label define srcdisa1_lbl 02 `"Company or union disability"', add
label define srcdisa1_lbl 03 `"Federal govt disability"', add
label define srcdisa1_lbl 04 `"US military retirement disability"', add
label define srcdisa1_lbl 05 `"State or local govt employee disability"', add
label define srcdisa1_lbl 06 `"US railroad retirement disability"', add
label define srcdisa1_lbl 07 `"Accident or disability insurance"', add
label define srcdisa1_lbl 08 `"Black lung miners disability"', add
label define srcdisa1_lbl 09 `"State temporary sickness"', add
label define srcdisa1_lbl 10 `"Other or don't know"', add
label values srcdisa1 srcdisa1_lbl

label define srcdisa2_lbl 00 `"NIU"'
label define srcdisa2_lbl 01 `"Workers compensation"', add
label define srcdisa2_lbl 02 `"Company or union disability"', add
label define srcdisa2_lbl 03 `"Federal govt disability"', add
label define srcdisa2_lbl 04 `"US military retirement disability"', add
label define srcdisa2_lbl 05 `"State or local govt employee disability"', add
label define srcdisa2_lbl 06 `"US railroad retirement disability"', add
label define srcdisa2_lbl 07 `"Accident or disability insurance"', add
label define srcdisa2_lbl 08 `"Black lung miners disability"', add
label define srcdisa2_lbl 09 `"State temporary sickness"', add
label define srcdisa2_lbl 10 `"Other or don't know"', add
label values srcdisa2 srcdisa2_lbl

label define srcearn_lbl 0 `"NIU"'
label define srcearn_lbl 1 `"Wage and salary"', add
label define srcearn_lbl 2 `"Self employment"', add
label define srcearn_lbl 3 `"Farm self employment"', add
label define srcearn_lbl 4 `"Without pay"', add
label values srcearn srcearn_lbl

label define srceduc_lbl 0 `"NIU"'
label define srceduc_lbl 1 `"Government assistance"', add
label define srceduc_lbl 2 `"Scholarships, grants etc from school"', add
label define srceduc_lbl 3 `"Other assistance"', add
label define srceduc_lbl 4 `"Govt assistance and scholarships, grants etc from school"', add
label define srceduc_lbl 5 `"Govt assistance and other assistance"', add
label define srceduc_lbl 6 `"Scholarships, grants etc from school and other assistance"', add
label define srceduc_lbl 7 `"Govt assistance, scholarships, grants etc from school, and other assistance"', add
label values srceduc srceduc_lbl

label define srcreti1_lbl 0 `"NIU"'
label define srcreti1_lbl 1 `"Company or Union  pension"', add
label define srcreti1_lbl 2 `"Federal Government retirement Pension"', add
label define srcreti1_lbl 3 `"US Military retirement pension"', add
label define srcreti1_lbl 4 `"State or local Govt retirement pension"', add
label define srcreti1_lbl 5 `"US Railroad retirement pension"', add
label define srcreti1_lbl 6 `"Regular payments from annuities or paid-up insurance policies"', add
label define srcreti1_lbl 7 `"Regular payments from IRA, KEOGH, or 401K accounts"', add
label define srcreti1_lbl 8 `"Other or don't know"', add
label values srcreti1 srcreti1_lbl

label define srcreti2_lbl 0 `"NIU"'
label define srcreti2_lbl 1 `"Company or Union  pension"', add
label define srcreti2_lbl 2 `"Federal Government retirement Pension"', add
label define srcreti2_lbl 3 `"US Military retirement pension"', add
label define srcreti2_lbl 4 `"State or local Govt retirement pension"', add
label define srcreti2_lbl 5 `"US Railroad retirement pension"', add
label define srcreti2_lbl 6 `"Regular payments from annuities or paid-up insurance policies"', add
label define srcreti2_lbl 7 `"Regular payments from IRA, KEOGH, or 401K accounts"', add
label define srcreti2_lbl 8 `"Other or do not know"', add
label values srcreti2 srcreti2_lbl

label define srcsurv1_lbl 00 `"NIU"'
label define srcsurv1_lbl 01 `"Company or union survivor pension"', add
label define srcsurv1_lbl 02 `"Federal government pension"', add
label define srcsurv1_lbl 03 `"US military retirement survivor pension"', add
label define srcsurv1_lbl 04 `"State or local govt survivor pension"', add
label define srcsurv1_lbl 05 `"US railroad retirement survivor pension"', add
label define srcsurv1_lbl 06 `"Workers compensation pension"', add
label define srcsurv1_lbl 07 `"Black lung survivor pension"', add
label define srcsurv1_lbl 08 `"Regular payments from estates or trusts"', add
label define srcsurv1_lbl 09 `"Regular payments from annuities or paid-up life insurance"', add
label define srcsurv1_lbl 10 `"Other or do not know"', add
label values srcsurv1 srcsurv1_lbl

label define srcsurv2_lbl 00 `"NIU"'
label define srcsurv2_lbl 01 `"Company or union survivor pension"', add
label define srcsurv2_lbl 02 `"Federal government pension"', add
label define srcsurv2_lbl 03 `"US military retirement survivor pension"', add
label define srcsurv2_lbl 04 `"State or local govt survivor pension"', add
label define srcsurv2_lbl 05 `"US railroad retirement survivor pension"', add
label define srcsurv2_lbl 06 `"Workers compensation pension"', add
label define srcsurv2_lbl 07 `"Black lung survivor pension"', add
label define srcsurv2_lbl 08 `"Regular payments from estates or trusts"', add
label define srcsurv2_lbl 09 `"Regular payments from annuities or paid-up life insurance"', add
label define srcsurv2_lbl 10 `"Other or do not know"', add
label values srcsurv2 srcsurv2_lbl

label define srcunemp_lbl 0 `"No supplemental or strike benefits"'
label define srcunemp_lbl 1 `"Supplemental unemployment benefits"', add
label define srcunemp_lbl 2 `"Union unemployment or strike benefits"', add
label define srcunemp_lbl 3 `"Both"', add
label define srcunemp_lbl 9 `"NIU"', add
label values srcunemp srcunemp_lbl

label define srcwelfr_lbl 0 `"NIU"'
label define srcwelfr_lbl 1 `"AFDC/TANF"', add
label define srcwelfr_lbl 2 `"Other"', add
label define srcwelfr_lbl 3 `"Both AFDC/TANF and other"', add
label values srcwelfr srcwelfr_lbl

label define srcwkcom_lbl 0 `"NIU"'
label define srcwkcom_lbl 1 `"State Workers Compensation"', add
label define srcwkcom_lbl 2 `"Employer or employers insurance"', add
label define srcwkcom_lbl 3 `"Own insurance"', add
label define srcwkcom_lbl 4 `"Other"', add
label values srcwkcom srcwkcom_lbl

label define ssikid_lbl 0 `"NIU"'
label define ssikid_lbl 1 `"No"', add
label define ssikid_lbl 2 `"Yes"', add
label values ssikid ssikid_lbl

label define sskid_lbl 0 `"NIU"'
label define sskid_lbl 1 `"No"', add
label define sskid_lbl 2 `"Yes"', add
label values sskid sskid_lbl

label define vetqa_lbl 0 `"NIU"'
label define vetqa_lbl 1 `"No"', add
label define vetqa_lbl 2 `"Yes"', add
label values vetqa vetqa_lbl

label define whyss1_lbl 0 `"NIU"'
label define whyss1_lbl 1 `"Retired"', add
label define whyss1_lbl 2 `"Disabled (adult or child)"', add
label define whyss1_lbl 3 `"Widowed"', add
label define whyss1_lbl 4 `"Spouse"', add
label define whyss1_lbl 5 `"Surviving child"', add
label define whyss1_lbl 6 `"Dependent child"', add
label define whyss1_lbl 7 `"On behalf of surviving, dependent, or disabled child(ren)"', add
label define whyss1_lbl 8 `"Other (adult or child)"', add
label values whyss1 whyss1_lbl

label define whyss2_lbl 0 `"NIU"'
label define whyss2_lbl 1 `"Retired"', add
label define whyss2_lbl 2 `"Disabled (adult or child)"', add
label define whyss2_lbl 3 `"Widowed"', add
label define whyss2_lbl 4 `"Spouse"', add
label define whyss2_lbl 5 `"Surviving child"', add
label define whyss2_lbl 6 `"Dependent child"', add
label define whyss2_lbl 7 `"On behalf of surviving, dependent, or disabled child(ren)"', add
label define whyss2_lbl 8 `"Other (adult or child)"', add
label values whyss2 whyss2_lbl

label define whyssi1_lbl 0 `"NIU"'
label define whyssi1_lbl 1 `"Disabled (adult or child)"', add
label define whyssi1_lbl 2 `"Blind (adult or child)"', add
label define whyssi1_lbl 3 `"On behalf of a disabled child"', add
label define whyssi1_lbl 4 `"On behalf of a blind child"', add
label define whyssi1_lbl 5 `"Other (adult or child)"', add
label values whyssi1 whyssi1_lbl

label define whyssi2_lbl 0 `"NIU"'
label define whyssi2_lbl 1 `"Disabled (adult or child)"', add
label define whyssi2_lbl 2 `"Blind (adult or child)"', add
label define whyssi2_lbl 3 `"On behalf of a disabled child"', add
label define whyssi2_lbl 4 `"On behalf of a blind child"', add
label define whyssi2_lbl 5 `"Other (adult or child)"', add
label values whyssi2 whyssi2_lbl

label define offpov_lbl 01 `"Below Poverty Line"'
label define offpov_lbl 02 `"Above Poverty Line"', add
label define offpov_lbl 99 `"NIU"', add
label values offpov offpov_lbl

label define offpovuniv_lbl 00 `"Out of Poverty Universe"'
label define offpovuniv_lbl 01 `"In Poverty Universe"', add
label values offpovuniv offpovuniv_lbl

label define offreason_lbl 01 `"No Family Income Issue - Coding Error/Out of Poverty Universe"'
label define offreason_lbl 02 `"Family Income Missing - Out of Poverty Universe"', add
label define offreason_lbl 03 `"Family Income Recode Issue"', add
label define offreason_lbl 99 `"NIU"', add
label values offreason offreason_lbl

label define poverty_lbl 00 `"NIU"'
label define poverty_lbl 10 `"Below poverty"', add
label define poverty_lbl 20 `"Above poverty"', add
label define poverty_lbl 21 `"100-124 percent of the low-income level"', add
label define poverty_lbl 22 `"125-149 percent of the low-income level"', add
label define poverty_lbl 23 `"150 percent and above the low-income level"', add
label values poverty poverty_lbl

label define filestat_lbl 0 `"No data"'
label define filestat_lbl 1 `"Joint, both less than 65"', add
label define filestat_lbl 2 `"Joint, one less than 65, one 65+"', add
label define filestat_lbl 3 `"Joint, both 65+"', add
label define filestat_lbl 4 `"Head of household"', add
label define filestat_lbl 5 `"Single"', add
label define filestat_lbl 6 `"Nonfiler"', add
label values filestat filestat_lbl

label define vetstat_lbl 0 `"NIU"'
label define vetstat_lbl 1 `"No service"', add
label define vetstat_lbl 2 `"Yes"', add
label define vetstat_lbl 9 `"Unknown"', add
label values vetstat vetstat_lbl

label define vetlast_lbl 00 `"NIU"'
label define vetlast_lbl 01 `"No service"', add
label define vetlast_lbl 02 `"World War I"', add
label define vetlast_lbl 03 `"November 1941 or earlier"', add
label define vetlast_lbl 04 `"World War II"', add
label define vetlast_lbl 05 `"Peacetime period B"', add
label define vetlast_lbl 06 `"Korean War"', add
label define vetlast_lbl 07 `"Peacetime period A"', add
label define vetlast_lbl 08 `"Vietnam Era"', add
label define vetlast_lbl 09 `"Other service"', add
label define vetlast_lbl 10 `"May 1975 to July 1990"', add
label define vetlast_lbl 11 `"August 1990 to August 2001"', add
label define vetlast_lbl 12 `"September 2001 or later"', add
label define vetlast_lbl 99 `"Unknown"', add
label values vetlast vetlast_lbl

label define vet1_lbl 00 `"NIU"'
label define vet1_lbl 01 `"September 2001 or later"', add
label define vet1_lbl 02 `"August 1990 to August 2001"', add
label define vet1_lbl 03 `"May 1975 to July 1990"', add
label define vet1_lbl 04 `"Vietnam Era (August 1964 to April 1975)"', add
label define vet1_lbl 05 `"February 1955 to July 1964"', add
label define vet1_lbl 06 `"Korean War (July 1950 to January 1955)"', add
label define vet1_lbl 07 `"January 1947 to June 1950"', add
label define vet1_lbl 08 `"World War II (December 1941 to December 1946)"', add
label define vet1_lbl 09 `"November 1941 or earlier"', add
label values vet1 vet1_lbl

label define vet2_lbl 00 `"NIU"'
label define vet2_lbl 01 `"September 2001 or later"', add
label define vet2_lbl 02 `"August 1990 to August 2001"', add
label define vet2_lbl 03 `"May 1975 to July 1990"', add
label define vet2_lbl 04 `"Vietnam Era (August 1964 to April 1975)"', add
label define vet2_lbl 05 `"February 1955 to July 1964"', add
label define vet2_lbl 06 `"Korean War (July 1950 to January 1955)"', add
label define vet2_lbl 07 `"January 1947 to June 1950"', add
label define vet2_lbl 08 `"World War II (December 1941 to December 1946)"', add
label define vet2_lbl 09 `"November 1941 or earlier"', add
label values vet2 vet2_lbl

label define vet3_lbl 00 `"NIU"'
label define vet3_lbl 01 `"September 2001 or later"', add
label define vet3_lbl 02 `"August 1990 to August 2001"', add
label define vet3_lbl 03 `"May 1975 to July 1990"', add
label define vet3_lbl 04 `"Vietnam Era (August 1964 to April 1975)"', add
label define vet3_lbl 05 `"February 1955 to July 1964"', add
label define vet3_lbl 06 `"Korean War (July 1950 to January 1955)"', add
label define vet3_lbl 07 `"January 1947 to June 1950"', add
label define vet3_lbl 08 `"World War II (December 1941 to December 1946)"', add
label define vet3_lbl 09 `"November 1941 or earlier"', add
label values vet3 vet3_lbl

label define vet4_lbl 00 `"NIU"'
label define vet4_lbl 01 `"September 2001 or later"', add
label define vet4_lbl 02 `"August 1990 to August 2001"', add
label define vet4_lbl 03 `"May 1975 to July 1990"', add
label define vet4_lbl 04 `"Vietnam Era (August 1964 to April 1975)"', add
label define vet4_lbl 05 `"February 1955 to July 1964"', add
label define vet4_lbl 06 `"Korean War (July 1950 to January 1955)"', add
label define vet4_lbl 07 `"January 1947 to June 1950"', add
label define vet4_lbl 08 `"World War II (December 1941 to December 1946)"', add
label define vet4_lbl 09 `"November 1941 or earlier"', add
label values vet4 vet4_lbl

label define disabwrk_lbl 0 `"NIU"'
label define disabwrk_lbl 1 `"No disability that affects work"', add
label define disabwrk_lbl 2 `"Disability limits or prevents work"', add
label values disabwrk disabwrk_lbl

label define health_lbl 1 `"Excellent"'
label define health_lbl 2 `"Very good"', add
label define health_lbl 3 `"Good"', add
label define health_lbl 4 `"Fair"', add
label define health_lbl 5 `"Poor"', add
label values health health_lbl

label define quitsick_lbl 0 `"NIU"'
label define quitsick_lbl 1 `"No, did not quit job or retire"', add
label define quitsick_lbl 2 `"Yes, quit job or retired"', add
label values quitsick quitsick_lbl

label define diffhear_lbl 0 `"NIU"'
label define diffhear_lbl 1 `"No hearing difficulty"', add
label define diffhear_lbl 2 `"Has hearing difficulty"', add
label values diffhear diffhear_lbl

label define diffeye_lbl 0 `"NIU"'
label define diffeye_lbl 1 `"No vision difficulty"', add
label define diffeye_lbl 2 `"Has vision difficulty"', add
label values diffeye diffeye_lbl

label define diffrem_lbl 0 `"NIU"'
label define diffrem_lbl 1 `"No difficulty remembering"', add
label define diffrem_lbl 2 `"Has difficulty remembering"', add
label values diffrem diffrem_lbl

label define diffphys_lbl 0 `"NIU"'
label define diffphys_lbl 1 `"No physical difficulty"', add
label define diffphys_lbl 2 `"Has physical difficulty"', add
label values diffphys diffphys_lbl

label define diffmob_lbl 0 `"NIU"'
label define diffmob_lbl 1 `"No mobility limitation"', add
label define diffmob_lbl 2 `"Has mobility limitation"', add
label values diffmob diffmob_lbl

label define diffcare_lbl 0 `"NIU"'
label define diffcare_lbl 1 `"No personal care difficulty"', add
label define diffcare_lbl 2 `"Has personal care difficulty"', add
label values diffcare diffcare_lbl

label define diffany_lbl 0 `"NIU"'
label define diffany_lbl 1 `"No difficulty"', add
label define diffany_lbl 2 `"Has difficulty"', add
label values diffany diffany_lbl

label define migsta1_lbl 00 `"NIU"'
label define migsta1_lbl 01 `"Alabama"', add
label define migsta1_lbl 02 `"Alaska"', add
label define migsta1_lbl 04 `"Arizona"', add
label define migsta1_lbl 05 `"Arkansas"', add
label define migsta1_lbl 06 `"California"', add
label define migsta1_lbl 08 `"Colorado"', add
label define migsta1_lbl 09 `"Connecticut"', add
label define migsta1_lbl 10 `"Delaware"', add
label define migsta1_lbl 11 `"District of Columbia"', add
label define migsta1_lbl 12 `"Florida"', add
label define migsta1_lbl 13 `"Georgia"', add
label define migsta1_lbl 15 `"Hawaii"', add
label define migsta1_lbl 16 `"Idaho"', add
label define migsta1_lbl 17 `"Illinois"', add
label define migsta1_lbl 18 `"Indiana"', add
label define migsta1_lbl 19 `"Iowa"', add
label define migsta1_lbl 20 `"Kansas"', add
label define migsta1_lbl 21 `"Kentucky"', add
label define migsta1_lbl 22 `"Louisiana"', add
label define migsta1_lbl 23 `"Maine"', add
label define migsta1_lbl 24 `"Maryland"', add
label define migsta1_lbl 25 `"Massachusetts"', add
label define migsta1_lbl 26 `"Michigan"', add
label define migsta1_lbl 27 `"Minnesota"', add
label define migsta1_lbl 28 `"Mississippi"', add
label define migsta1_lbl 29 `"Missouri"', add
label define migsta1_lbl 30 `"Montana"', add
label define migsta1_lbl 31 `"Nebraska"', add
label define migsta1_lbl 32 `"Nevada"', add
label define migsta1_lbl 33 `"New Hampshire"', add
label define migsta1_lbl 34 `"New Jersey"', add
label define migsta1_lbl 35 `"New Mexico"', add
label define migsta1_lbl 36 `"New York"', add
label define migsta1_lbl 37 `"North Carolina"', add
label define migsta1_lbl 38 `"North Dakota"', add
label define migsta1_lbl 39 `"Ohio"', add
label define migsta1_lbl 40 `"Oklahoma"', add
label define migsta1_lbl 41 `"Oregon"', add
label define migsta1_lbl 42 `"Pennsylvania"', add
label define migsta1_lbl 44 `"Rhode Island"', add
label define migsta1_lbl 45 `"South Carolina"', add
label define migsta1_lbl 46 `"South Dakota"', add
label define migsta1_lbl 47 `"Tennessee"', add
label define migsta1_lbl 48 `"Texas"', add
label define migsta1_lbl 49 `"Utah"', add
label define migsta1_lbl 50 `"Vermont"', add
label define migsta1_lbl 51 `"Virginia"', add
label define migsta1_lbl 53 `"Washington"', add
label define migsta1_lbl 54 `"West Virginia"', add
label define migsta1_lbl 55 `"Wisconsin"', add
label define migsta1_lbl 56 `"Wyoming"', add
label define migsta1_lbl 91 `"Abroad"', add
label define migsta1_lbl 99 `"Same house"', add
label values migsta1 migsta1_lbl

label define migsta5_lbl 00 `"NIU"'
label define migsta5_lbl 01 `"Alabama"', add
label define migsta5_lbl 02 `"Alaska"', add
label define migsta5_lbl 04 `"Arizona"', add
label define migsta5_lbl 05 `"Arkansas"', add
label define migsta5_lbl 06 `"California"', add
label define migsta5_lbl 08 `"Colorado"', add
label define migsta5_lbl 09 `"Connecticut"', add
label define migsta5_lbl 10 `"Delaware"', add
label define migsta5_lbl 11 `"District of Columbia"', add
label define migsta5_lbl 12 `"Florida"', add
label define migsta5_lbl 13 `"Georgia"', add
label define migsta5_lbl 15 `"Hawaii"', add
label define migsta5_lbl 16 `"Idaho"', add
label define migsta5_lbl 17 `"Illinois"', add
label define migsta5_lbl 18 `"Indiana"', add
label define migsta5_lbl 19 `"Iowa"', add
label define migsta5_lbl 20 `"Kansas"', add
label define migsta5_lbl 21 `"Kentucky"', add
label define migsta5_lbl 22 `"Louisiana"', add
label define migsta5_lbl 23 `"Maine"', add
label define migsta5_lbl 24 `"Maryland"', add
label define migsta5_lbl 25 `"Massachusetts"', add
label define migsta5_lbl 26 `"Michigan"', add
label define migsta5_lbl 27 `"Minnesota"', add
label define migsta5_lbl 28 `"Mississippi"', add
label define migsta5_lbl 29 `"Missouri"', add
label define migsta5_lbl 30 `"Montana"', add
label define migsta5_lbl 31 `"Nebraska"', add
label define migsta5_lbl 32 `"Nevada"', add
label define migsta5_lbl 33 `"New Hampshire"', add
label define migsta5_lbl 34 `"New Jersey"', add
label define migsta5_lbl 35 `"New Mexico"', add
label define migsta5_lbl 36 `"New York"', add
label define migsta5_lbl 37 `"North Carolina"', add
label define migsta5_lbl 38 `"North Dakota"', add
label define migsta5_lbl 39 `"Ohio"', add
label define migsta5_lbl 40 `"Oklahoma"', add
label define migsta5_lbl 41 `"Oregon"', add
label define migsta5_lbl 42 `"Pennsylvania"', add
label define migsta5_lbl 44 `"Rhode Island"', add
label define migsta5_lbl 45 `"South Carolina"', add
label define migsta5_lbl 46 `"South Dakota"', add
label define migsta5_lbl 47 `"Tennessee"', add
label define migsta5_lbl 48 `"Texas"', add
label define migsta5_lbl 49 `"Utah"', add
label define migsta5_lbl 50 `"Vermont"', add
label define migsta5_lbl 51 `"Virginia"', add
label define migsta5_lbl 53 `"Washington"', add
label define migsta5_lbl 54 `"West Virginia"', add
label define migsta5_lbl 55 `"Wisconsin"', add
label define migsta5_lbl 56 `"Wyoming"', add
label define migsta5_lbl 91 `"Abroad"', add
label values migsta5 migsta5_lbl

label define whymove_lbl 00 `"NIU"'
label define whymove_lbl 01 `"Change in marital status"', add
label define whymove_lbl 02 `"To establish own household"', add
label define whymove_lbl 03 `"Other family reason"', add
label define whymove_lbl 04 `"New job or job transfer"', add
label define whymove_lbl 05 `"To look for work or lost job"', add
label define whymove_lbl 06 `"For easier commute"', add
label define whymove_lbl 07 `"Retired"', add
label define whymove_lbl 08 `"Other job-related reason"', add
label define whymove_lbl 09 `"Wanted to own home, not rent"', add
label define whymove_lbl 10 `"Wanted new or better housing"', add
label define whymove_lbl 11 `"Wanted better neighborhood"', add
label define whymove_lbl 12 `"For cheaper housing"', add
label define whymove_lbl 13 `"Other housing reason"', add
label define whymove_lbl 14 `"Attend/leave college"', add
label define whymove_lbl 15 `"Change of climate"', add
label define whymove_lbl 16 `"Health reasons"', add
label define whymove_lbl 17 `"Other reasons"', add
label define whymove_lbl 18 `"Natural disaster"', add
label define whymove_lbl 19 `"Foreclosure or eviction"', add
label values whymove whymove_lbl

label define country_lbl 00000 `"NIU"'
label define country_lbl 10000 `"American Samoa"', add
label define country_lbl 10500 `"Guam"', add
label define country_lbl 11000 `"Puerto Rico"', add
label define country_lbl 11500 `"U.S. Virgin Islands"', add
label define country_lbl 12090 `"U.S. outlying areas, n.s."', add
label define country_lbl 15000 `"Canada"', add
label define country_lbl 16010 `"Bermuda"', add
label define country_lbl 19900 `"North America, n.s."', add
label define country_lbl 20000 `"Mexico"', add
label define country_lbl 21010 `"Belize/British Honduras"', add
label define country_lbl 21020 `"Costa Rica"', add
label define country_lbl 21030 `"El Salvador"', add
label define country_lbl 21040 `"Guatemala"', add
label define country_lbl 21050 `"Honduras"', add
label define country_lbl 21060 `"Nicaragua"', add
label define country_lbl 21070 `"Panama"', add
label define country_lbl 21090 `"Central America, n.s."', add
label define country_lbl 25000 `"Cuba"', add
label define country_lbl 26010 `"Dominican Republic"', add
label define country_lbl 26020 `"Haiti"', add
label define country_lbl 26030 `"Jamaica"', add
label define country_lbl 26043 `"Bahamas"', add
label define country_lbl 26044 `"Barbados"', add
label define country_lbl 26054 `"Dominica"', add
label define country_lbl 26055 `"Grenada"', add
label define country_lbl 26060 `"Trinidad and Tobago"', add
label define country_lbl 26091 `"Caribbean, n.s."', add
label define country_lbl 30005 `"Argentina"', add
label define country_lbl 30010 `"Bolivia"', add
label define country_lbl 30015 `"Brazil"', add
label define country_lbl 30020 `"Chile"', add
label define country_lbl 30025 `"Colombia"', add
label define country_lbl 30030 `"Ecuador"', add
label define country_lbl 30040 `"Guyana/British Guiana"', add
label define country_lbl 30050 `"Peru"', add
label define country_lbl 30060 `"Uruguay"', add
label define country_lbl 30065 `"Venezuela"', add
label define country_lbl 30090 `"South America, n.s."', add
label define country_lbl 40000 `"Denmark"', add
label define country_lbl 40100 `"Finland"', add
label define country_lbl 40400 `"Norway"', add
label define country_lbl 40500 `"Sweden"', add
label define country_lbl 41000 `"England"', add
label define country_lbl 41100 `"Scotland"', add
label define country_lbl 41300 `"United Kingdom, n.s."', add
label define country_lbl 41400 `"Ireland"', add
label define country_lbl 41410 `"Northern Ireland"', add
label define country_lbl 42000 `"Belgium"', add
label define country_lbl 42100 `"France"', add
label define country_lbl 42500 `"Netherlands"', add
label define country_lbl 42600 `"Switzerland"', add
label define country_lbl 43300 `"Greece"', add
label define country_lbl 43400 `"Italy"', add
label define country_lbl 43600 `"Portugal"', add
label define country_lbl 43610 `"Azores"', add
label define country_lbl 43800 `"Spain"', add
label define country_lbl 45000 `"Austria"', add
label define country_lbl 45200 `"Czechoslavakia"', add
label define country_lbl 45212 `"Slovakia"', add
label define country_lbl 45213 `"Czech Republic"', add
label define country_lbl 45300 `"Germany"', add
label define country_lbl 45400 `"Hungary"', add
label define country_lbl 45500 `"Poland"', add
label define country_lbl 45540 `"Armenia"', add
label define country_lbl 45600 `"Romania"', add
label define country_lbl 45700 `"Yugoslavia"', add
label define country_lbl 46100 `"Latvia"', add
label define country_lbl 46200 `"Lithuania"', add
label define country_lbl 46500 `"Other USSR/Russia"', add
label define country_lbl 46530 `"Ukraine"', add
label define country_lbl 46590 `"USSR, n.s."', add
label define country_lbl 49900 `"Europe, n.s."', add
label define country_lbl 50000 `"China"', add
label define country_lbl 50010 `"Hong Kong"', add
label define country_lbl 50040 `"Taiwan"', add
label define country_lbl 50100 `"Japan"', add
label define country_lbl 50200 `"Korea"', add
label define country_lbl 50220 `"South Korea"', add
label define country_lbl 51100 `"Cambodia"', add
label define country_lbl 51200 `"Indonesia"', add
label define country_lbl 51300 `"Laos"', add
label define country_lbl 51400 `"Malaysia"', add
label define country_lbl 51500 `"Philippines"', add
label define country_lbl 51600 `"Singapore"', add
label define country_lbl 51700 `"Thailand"', add
label define country_lbl 51800 `"Vietnam"', add
label define country_lbl 52000 `"Afghanistan"', add
label define country_lbl 52100 `"India"', add
label define country_lbl 52110 `"Bangladesh"', add
label define country_lbl 52130 `"Burma"', add
label define country_lbl 52140 `"Pakistan"', add
label define country_lbl 52200 `"Iran"', add
label define country_lbl 53200 `"Iraq"', add
label define country_lbl 53400 `"Israel/Palestine"', add
label define country_lbl 53420 `"Palestine"', add
label define country_lbl 53500 `"Jordan"', add
label define country_lbl 53700 `"Lebanon"', add
label define country_lbl 54000 `"Saudi Arabia"', add
label define country_lbl 54100 `"Syria"', add
label define country_lbl 54200 `"Turkey"', add
label define country_lbl 54700 `"Middle East, n.s."', add
label define country_lbl 59900 `"Asia, n.e.c./n.s."', add
label define country_lbl 60010 `"Northern Africa"', add
label define country_lbl 60012 `"Egypt/United Arab Rep."', add
label define country_lbl 60014 `"Morocco"', add
label define country_lbl 60023 `"Ghana"', add
label define country_lbl 60031 `"Nigeria"', add
label define country_lbl 60044 `"Ethiopia"', add
label define country_lbl 60045 `"Kenya"', add
label define country_lbl 60094 `"South Africa (Union of)"', add
label define country_lbl 60099 `"Africa, n.s./n.e.c."', add
label define country_lbl 70010 `"Australia"', add
label define country_lbl 70020 `"New Zealand"', add
label define country_lbl 71000 `"Pacific Islands"', add
label define country_lbl 71021 `"Fiji"', add
label define country_lbl 96000 `"Other, n.e.c. and unknown"', add
label define country_lbl 99999 `"Unknown/missing"', add
label values country country_lbl

label define migrate1_lbl 0 `"NIU"'
label define migrate1_lbl 1 `"Same house"', add
label define migrate1_lbl 2 `"Different house, place not reported"', add
label define migrate1_lbl 3 `"Moved within county"', add
label define migrate1_lbl 4 `"Moved within state, different county"', add
label define migrate1_lbl 5 `"Moved between states"', add
label define migrate1_lbl 6 `"Abroad"', add
label define migrate1_lbl 9 `"Unknown"', add
label values migrate1 migrate1_lbl

label define migrate5_lbl 00 `"NIU"'
label define migrate5_lbl 10 `"Same house"', add
label define migrate5_lbl 20 `"Place not reported"', add
label define migrate5_lbl 30 `"Moved within county"', add
label define migrate5_lbl 40 `"Moved within state, between counties"', add
label define migrate5_lbl 41 `"Different county, same state, same SMSA"', add
label define migrate5_lbl 42 `"Different county, same state, different SMSA"', add
label define migrate5_lbl 50 `"Moved between states"', add
label define migrate5_lbl 51 `"Moved between contiguous states"', add
label define migrate5_lbl 52 `"Moved between non-contiguous states"', add
label define migrate5_lbl 60 `"Abroad five years ago"', add
label values migrate5 migrate5_lbl

label define katevac_lbl 00 `"No data"'
label define katevac_lbl 01 `"Yes"', add
label define katevac_lbl 02 `"No"', add
label define katevac_lbl 03 `"Refused"', add
label define katevac_lbl 04 `"Don't know"', add
label values katevac katevac_lbl

label define katevac2_lbl 00 `"No data"'
label define katevac2_lbl 01 `"Yes"', add
label define katevac2_lbl 02 `"No"', add
label values katevac2 katevac2_lbl

label define katprior_lbl 00 `"No data"'
label define katprior_lbl 01 `"At this address"', add
label define katprior_lbl 02 `"Louisiana (but not at this address)"', add
label define katprior_lbl 03 `"Mississippi (but not at this address)"', add
label define katprior_lbl 04 `"Alabama (but not at this address)"', add
label define katprior_lbl 05 `"Florida (but not at this address)"', add
label define katprior_lbl 06 `"Elsewhere in the U.S."', add
label define katprior_lbl 07 `"Refused"', add
label define katprior_lbl 08 `"Don't know"', add
label values katprior katprior_lbl

label define hcovany_lbl 1 `"Not covered"'
label define hcovany_lbl 2 `"Covered"', add
label values hcovany hcovany_lbl

label define hcovpriv_lbl 1 `"Not covered"'
label define hcovpriv_lbl 2 `"Covered"', add
label values hcovpriv hcovpriv_lbl

label define hinsemp_lbl 1 `"Not covered"'
label define hinsemp_lbl 2 `"Covered"', add
label values hinsemp hinsemp_lbl

label define hinspur_lbl 1 `"Not covered"'
label define hinspur_lbl 2 `"Covered"', add
label values hinspur hinspur_lbl

label define hcovpub_lbl 1 `"Not covered"'
label define hcovpub_lbl 2 `"Covered"', add
label values hcovpub hcovpub_lbl

label define hinscaid_lbl 1 `"Not covered"'
label define hinscaid_lbl 2 `"Covered"', add
label values hinscaid hinscaid_lbl

label define hinscare_lbl 1 `"Not covered"'
label define hinscare_lbl 2 `"Covered"', add
label values hinscare hinscare_lbl

label define hinsmil_lbl 1 `"Not covered"'
label define hinsmil_lbl 2 `"Covered"', add
label values hinsmil hinsmil_lbl

label define inclugh_lbl 0 `"NIU"'
label define inclugh_lbl 1 `"No"', add
label define inclugh_lbl 2 `"Yes"', add
label values inclugh inclugh_lbl

label define paidgh_lbl 00 `"NIU"'
label define paidgh_lbl 10 `"No"', add
label define paidgh_lbl 20 `"Yes"', add
label define paidgh_lbl 21 `"Yes, paid for part"', add
label define paidgh_lbl 22 `"Yes, paid for all"', add
label values paidgh paidgh_lbl

label define himcaid_lbl 1 `"No"'
label define himcaid_lbl 2 `"Yes"', add
label values himcaid himcaid_lbl

label define himcare_lbl 0 `"NIU"'
label define himcare_lbl 1 `"No"', add
label define himcare_lbl 2 `"Yes"', add
label values himcare himcare_lbl

label define hichamp_lbl 1 `"No"'
label define hichamp_lbl 2 `"Yes"', add
label values hichamp hichamp_lbl

label define phinsur_lbl 0 `"NIU"'
label define phinsur_lbl 1 `"No"', add
label define phinsur_lbl 2 `"Yes"', add
label values phinsur phinsur_lbl

label define phiown_lbl 0 `"NIU"'
label define phiown_lbl 1 `"No"', add
label define phiown_lbl 2 `"Yes"', add
label values phiown phiown_lbl

label define caid_lbl 1 `"No"'
label define caid_lbl 2 `"Yes"', add
label values caid caid_lbl

label define care_lbl 1 `"No"'
label define care_lbl 2 `"Yes"', add
label values care care_lbl

label define champus_lbl 1 `"No"'
label define champus_lbl 2 `"Yes"', add
label values champus champus_lbl

label define champva_lbl 1 `"No"'
label define champva_lbl 2 `"Yes"', add
label values champva champva_lbl

label define militva_lbl 0 `"No"'
label define militva_lbl 1 `"Yes, military health care or VA"', add
label define militva_lbl 2 `"Yes, VA health care only"', add
label define militva_lbl 3 `"Yes, military health care only"', add
label define militva_lbl 4 `"Yes, both military health care and VA"', add
label values militva militva_lbl

label define indianh_lbl 1 `"No"'
label define indianh_lbl 2 `"Yes"', add
label values indianh indianh_lbl

label define groupown_lbl 0 `"NIU"'
label define groupown_lbl 1 `"No"', add
label define groupown_lbl 2 `"Yes"', add
label values groupown groupown_lbl

label define groupdep_lbl 1 `"No"'
label define groupdep_lbl 2 `"Yes"', add
label values groupdep groupdep_lbl

label define privown_lbl 0 `"NIU"'
label define privown_lbl 1 `"No"', add
label define privown_lbl 2 `"Yes"', add
label values privown privown_lbl

label define privdep_lbl 1 `"No"'
label define privdep_lbl 2 `"Yes"', add
label values privdep privdep_lbl

label define grouptyp_lbl 0 `"NIU"'
label define grouptyp_lbl 1 `"Family plan"', add
label define grouptyp_lbl 2 `"Self only covered"', add
label values grouptyp grouptyp_lbl

label define privtyp_lbl 0 `"NIU"'
label define privtyp_lbl 1 `"Family plan"', add
label define privtyp_lbl 2 `"Self only covered"', add
label values privtyp privtyp_lbl

label define grpwho1_lbl 00 `"Not covered by 1st group policy of hh member"'
label define grpwho1_lbl 01 `"1"', add
label define grpwho1_lbl 02 `"2"', add
label define grpwho1_lbl 03 `"3"', add
label define grpwho1_lbl 04 `"4"', add
label define grpwho1_lbl 05 `"5"', add
label define grpwho1_lbl 06 `"6"', add
label define grpwho1_lbl 07 `"7"', add
label define grpwho1_lbl 08 `"8"', add
label define grpwho1_lbl 09 `"9"', add
label define grpwho1_lbl 10 `"10"', add
label define grpwho1_lbl 11 `"11"', add
label define grpwho1_lbl 12 `"12"', add
label define grpwho1_lbl 13 `"13"', add
label define grpwho1_lbl 14 `"14"', add
label define grpwho1_lbl 15 `"15"', add
label define grpwho1_lbl 16 `"16"', add
label values grpwho1 grpwho1_lbl

label define grpwho2_lbl 00 `"Not covered by 2nd group policy of hh member"'
label define grpwho2_lbl 01 `"1"', add
label define grpwho2_lbl 02 `"2"', add
label define grpwho2_lbl 03 `"3"', add
label define grpwho2_lbl 04 `"4"', add
label define grpwho2_lbl 05 `"5"', add
label define grpwho2_lbl 06 `"6"', add
label define grpwho2_lbl 07 `"7"', add
label define grpwho2_lbl 08 `"8"', add
label define grpwho2_lbl 09 `"9"', add
label define grpwho2_lbl 10 `"10"', add
label define grpwho2_lbl 11 `"11"', add
label define grpwho2_lbl 12 `"12"', add
label define grpwho2_lbl 13 `"13"', add
label define grpwho2_lbl 14 `"14"', add
label define grpwho2_lbl 15 `"15"', add
label define grpwho2_lbl 16 `"16"', add
label values grpwho2 grpwho2_lbl

label define privwho1_lbl 00 `"Not covered by 1st private policy of hh member"'
label define privwho1_lbl 01 `"1"', add
label define privwho1_lbl 02 `"2"', add
label define privwho1_lbl 03 `"3"', add
label define privwho1_lbl 04 `"4"', add
label define privwho1_lbl 05 `"5"', add
label define privwho1_lbl 06 `"6"', add
label define privwho1_lbl 07 `"7"', add
label define privwho1_lbl 08 `"8"', add
label define privwho1_lbl 09 `"9"', add
label define privwho1_lbl 10 `"10"', add
label define privwho1_lbl 11 `"11"', add
label define privwho1_lbl 12 `"12"', add
label define privwho1_lbl 13 `"13"', add
label define privwho1_lbl 14 `"14"', add
label define privwho1_lbl 15 `"15"', add
label define privwho1_lbl 16 `"16"', add
label values privwho1 privwho1_lbl

label define privwho2_lbl 00 `"Not covered by 2nd private policy of hh member"'
label define privwho2_lbl 01 `"1"', add
label define privwho2_lbl 02 `"2"', add
label define privwho2_lbl 03 `"3"', add
label define privwho2_lbl 04 `"4"', add
label define privwho2_lbl 05 `"5"', add
label define privwho2_lbl 06 `"6"', add
label define privwho2_lbl 07 `"7"', add
label define privwho2_lbl 08 `"8"', add
label define privwho2_lbl 09 `"9"', add
label define privwho2_lbl 10 `"10"', add
label define privwho2_lbl 11 `"11"', add
label define privwho2_lbl 12 `"12"', add
label define privwho2_lbl 13 `"13"', add
label define privwho2_lbl 14 `"14"', add
label define privwho2_lbl 15 `"15"', add
label define privwho2_lbl 16 `"16"', add
label values privwho2 privwho2_lbl

label define schip_lbl 0 `"NIU"'
label define schip_lbl 1 `"No"', add
label define schip_lbl 2 `"Yes"', add
label values schip schip_lbl

label define verify_lbl 0 `"NIU"'
label define verify_lbl 1 `"No, uninsured"', add
label define verify_lbl 2 `"Yes, insured"', add
label values verify verify_lbl

label define groupout_lbl 0 `"NIU"'
label define groupout_lbl 1 `"No"', add
label define groupout_lbl 2 `"Yes"', add
label values groupout groupout_lbl

label define privout_lbl 0 `"NIU"'
label define privout_lbl 1 `"No"', add
label define privout_lbl 2 `"Yes"', add
label values privout privout_lbl

label define out_lbl 1 `"No"'
label define out_lbl 2 `"Yes"', add
label values out out_lbl

label define covergh_lbl 1 `"No"'
label define covergh_lbl 2 `"Yes"', add
label values covergh covergh_lbl

label define coverpi_lbl 1 `"No"'
label define coverpi_lbl 2 `"Yes"', add
label values coverpi coverpi_lbl

label define kidpriv_lbl 0 `"NIU"'
label define kidpriv_lbl 1 `"Not covered"', add
label define kidpriv_lbl 2 `"Covered by person in hh"', add
label define kidpriv_lbl 3 `"Covered by person outside hh"', add
label values kidpriv kidpriv_lbl

label define kidcaid_lbl 0 `"NIU"'
label define kidcaid_lbl 1 `"No"', add
label define kidcaid_lbl 2 `"Yes"', add
label values kidcaid kidcaid_lbl

label define gedtrain_lbl 0 `"NIU"'
label define gedtrain_lbl 1 `"No"', add
label define gedtrain_lbl 2 `"Yes"', add
label values gedtrain gedtrain_lbl

label define gotwic_lbl 0 `"NIU"'
label define gotwic_lbl 1 `"No"', add
label define gotwic_lbl 2 `"Yes"', add
label values gotwic gotwic_lbl

label define jobclub_lbl 0 `"NIU"'
label define jobclub_lbl 1 `"No"', add
label define jobclub_lbl 2 `"Yes"', add
label values jobclub jobclub_lbl

label define jobready_lbl 0 `"NIU"'
label define jobready_lbl 1 `"No"', add
label define jobready_lbl 2 `"Yes"', add
label values jobready jobready_lbl

label define jobtrain_lbl 0 `" NIU"'
label define jobtrain_lbl 1 `"No"', add
label define jobtrain_lbl 2 `"Yes"', add
label values jobtrain jobtrain_lbl

label define kidcare_lbl 0 `"NIU"'
label define kidcare_lbl 1 `"No"', add
label define kidcare_lbl 2 `"Yes"', add
label values kidcare kidcare_lbl

label define kidcneed_lbl 0 `"NIU"'
label define kidcneed_lbl 1 `"No"', add
label define kidcneed_lbl 2 `"Yes"', add
label values kidcneed kidcneed_lbl

label define transaid_lbl 0 `"NIU"'
label define transaid_lbl 1 `"No"', add
label define transaid_lbl 2 `"Yes"', add
label values transaid transaid_lbl

label define workfare_lbl 0 `"NIU"'
label define workfare_lbl 1 `"No"', add
label define workfare_lbl 2 `"Yes"', add
label values workfare workfare_lbl

label define ftype_lbl 1 `"Primary family"'
label define ftype_lbl 2 `"Nonfamily householder"', add
label define ftype_lbl 3 `"Related subfamily"', add
label define ftype_lbl 4 `"Unrelated subfamily"', add
label define ftype_lbl 5 `"Secondary individual"', add
label define ftype_lbl 9 `"Missing"', add
label values ftype ftype_lbl

label define wkstat_lbl 0 `"Children or Armed Forces"'
label define wkstat_lbl 1 `"Employed full time"', add
label define wkstat_lbl 2 `"Employed part time"', add
label define wkstat_lbl 3 `"Employed part time for economic reason"', add
label define wkstat_lbl 4 `"Employed part time for non-economic reason"', add
label define wkstat_lbl 5 `"Unemployed part time"', add
label define wkstat_lbl 6 `"Unemployed full time"', add
label define wkstat_lbl 9 `"NIU, NILF, or blank"', add
label values wkstat wkstat_lbl

label define csout_lbl 01 `"No"'
label define csout_lbl 02 `"Yes"', add
label define csout_lbl 98 `"Blank"', add
label define csout_lbl 99 `"NIU"', add
label values csout csout_lbl

label define cswhy_lbl 01 `"Other parent has died"'
label define cswhy_lbl 02 `"Child was adopted by a single parent"', add
label define cswhy_lbl 03 `"Other parent terminated their parental rights"', add
label define cswhy_lbl 04 `"Other parent is living in the household"', add
label define cswhy_lbl 05 `"Other parent is no longer recognized as a parent by this household"', add
label define cswhy_lbl 06 `"Parents are separated/divorced"', add
label define cswhy_lbl 07 `"Didn't want contact with the child's other parent"', add
label define cswhy_lbl 08 `"Don't know where child's other parent is"', add
label define cswhy_lbl 09 `"She/he lives elsewhere"', add
label define cswhy_lbl 10 `"Other"', add
label define cswhy_lbl 98 `"Blank"', add
label define cswhy_lbl 99 `"NIU"', add
label values cswhy cswhy_lbl

label define cslegal_lbl 01 `"No legal agreement"'
label define cslegal_lbl 02 `"Legal agreement pending"', add
label define cslegal_lbl 03 `"Arrangement, not sure if legal"', add
label define cslegal_lbl 04 `"Court order"', add
label define cslegal_lbl 05 `"Court award"', add
label define cslegal_lbl 06 `"Legal agreement"', add
label define cslegal_lbl 98 `"Blank"', add
label define cslegal_lbl 99 `"NIU"', add
label values cslegal cslegal_lbl

label define csagree_lbl 01 `"No agreement"'
label define csagree_lbl 02 `"Agreement"', add
label define csagree_lbl 03 `"Understanding"', add
label define csagree_lbl 98 `"Blank"', add
label define csagree_lbl 99 `"NIU"', add
label values csagree csagree_lbl

label define cstype_lbl 01 `"No agreement"'
label define cstype_lbl 02 `"Informal"', add
label define cstype_lbl 03 `"Legal agreement pending"', add
label define cstype_lbl 04 `"Legal agreement"', add
label define cstype_lbl 98 `"Blank"', add
label define cstype_lbl 99 `"NIU or blank"', add
label values cstype cstype_lbl

label define cspay_lbl 01 `"No"'
label define cspay_lbl 02 `"Yes"', add
label define cspay_lbl 98 `"Blank"', add
label define cspay_lbl 99 `"NIU"', add
label values cspay cspay_lbl

label define cssupp_lbl 01 `"No"'
label define cssupp_lbl 02 `"Yes"', add
label define cssupp_lbl 03 `"Yes if she/he has a job"', add
label define cssupp_lbl 04 `"Don't know because Child Support Enforcement Office filed the paperwork"', add
label define cssupp_lbl 98 `"Blank"', add
label define cssupp_lbl 99 `"NIU"', add
label values cssupp cssupp_lbl

label define csreas_lbl 01 `"Child(ren) too old last year"'
label define csreas_lbl 02 `"Other parent died before last year"', add
label define csreas_lbl 03 `"Family lived together in all or part of last year"', add
label define csreas_lbl 04 `"Child(ren) lived with other parent in all or part of last year"', add
label define csreas_lbl 05 `"Payments not awarded until this calendar year"', add
label define csreas_lbl 06 `"Other"', add
label define csreas_lbl 98 `"Blank"', add
label define csreas_lbl 99 `"NIU"', add
label values csreas csreas_lbl

label define csdduct_lbl 01 `"No"'
label define csdduct_lbl 02 `"Yes"', add
label define csdduct_lbl 98 `"Blank"', add
label define csdduct_lbl 99 `"NIU"', add
label values csdduct csdduct_lbl

label define cswaypay_lbl 01 `"Directly"'
label define cswaypay_lbl 02 `"By child support, welfare, or other public agency"', add
label define cswaypay_lbl 03 `"By a court"', add
label define cswaypay_lbl 04 `"By direct deposit"', add
label define cswaypay_lbl 05 `"Directly from other parent's employer"', add
label define cswaypay_lbl 06 `"Other"', add
label define cswaypay_lbl 98 `"Blank"', add
label define cswaypay_lbl 99 `"NIU"', add
label values cswaypay cswaypay_lbl

label define cschg_lbl 01 `"No"'
label define cschg_lbl 02 `"Yes"', add
label define cschg_lbl 98 `"Blank"', add
label define cschg_lbl 99 `"NIU"', add
label values cschg cschg_lbl

label define csfreq1_lbl 01 `"Every week"'
label define csfreq1_lbl 02 `"Every other week"', add
label define csfreq1_lbl 03 `"Twice a month"', add
label define csfreq1_lbl 04 `"Every month or Monthly"', add
label define csfreq1_lbl 05 `"Every quarter"', add
label define csfreq1_lbl 06 `"For the year"', add
label define csfreq1_lbl 07 `"None"', add
label define csfreq1_lbl 08 `"Other"', add
label define csfreq1_lbl 98 `"Blank"', add
label define csfreq1_lbl 99 `"NIU"', add
label values csfreq1 csfreq1_lbl

label define csback1_lbl 01 `"No"'
label define csback1_lbl 02 `"Yes"', add
label define csback1_lbl 98 `"Blank"', add
label define csback1_lbl 99 `"NIU"', add
label values csback1 csback1_lbl

label define csfreq2_lbl 01 `"Every week"'
label define csfreq2_lbl 02 `"Every other week"', add
label define csfreq2_lbl 03 `"Twice a month"', add
label define csfreq2_lbl 04 `"Every month or Monthly"', add
label define csfreq2_lbl 05 `"Every quarter"', add
label define csfreq2_lbl 06 `"For the year"', add
label define csfreq2_lbl 07 `"Other"', add
label define csfreq2_lbl 08 `"None"', add
label define csfreq2_lbl 98 `"Blank"', add
label define csfreq2_lbl 99 `"NIU"', add
label values csfreq2 csfreq2_lbl

label define csback2_lbl 01 `"No"'
label define csback2_lbl 02 `"Yes"', add
label define csback2_lbl 98 `"Blank"', add
label define csback2_lbl 99 `"NIU"', add
label values csback2 csback2_lbl

label define csrecv_lbl 01 `"No"'
label define csrecv_lbl 02 `"Yes"', add
label define csrecv_lbl 98 `"Blank"', add
label define csrecv_lbl 99 `"NIU"', add
label values csrecv csrecv_lbl

label define csall_lbl 01 `"No"'
label define csall_lbl 02 `"Yes"', add
label define csall_lbl 98 `"Blank"', add
label define csall_lbl 99 `"NIU"', add
label values csall csall_lbl

label define cstime_lbl 01 `"None"'
label define cstime_lbl 02 `"Some"', add
label define cstime_lbl 03 `"Most"', add
label define cstime_lbl 04 `"All"', add
label define cstime_lbl 98 `"Blank"', add
label define cstime_lbl 99 `"NIU"', add
label values cstime cstime_lbl

label define csfull_lbl 01 `"None"'
label define csfull_lbl 02 `"Some"', add
label define csfull_lbl 03 `"Most"', add
label define csfull_lbl 04 `"All"', add
label define csfull_lbl 98 `"Blank"', add
label define csfull_lbl 99 `"NIU"', add
label values csfull csfull_lbl

label define cscontact_lbl 01 `"No"'
label define cscontact_lbl 02 `"Yes"', add
label define cscontact_lbl 98 `"Blank"', add
label define cscontact_lbl 99 `"NIU"', add
label values cscontact cscontact_lbl

label define csvis_lbl 01 `"No"'
label define csvis_lbl 02 `"Yes"', add
label define csvis_lbl 98 `"Blank"', add
label define csvis_lbl 99 `"NIU"', add
label values csvis csvis_lbl

label define csvisleg_lbl 01 `"No"'
label define csvisleg_lbl 02 `"Yes"', add
label define csvisleg_lbl 98 `"Blank"', add
label define csvisleg_lbl 99 `"NIU"', add
label values csvisleg csvisleg_lbl

label define csphycus_lbl 01 `"No"'
label define csphycus_lbl 02 `"Yes"', add
label define csphycus_lbl 98 `"Blank"', add
label define csphycus_lbl 99 `"NIU"', add
label values csphycus csphycus_lbl

label define cslegcus_lbl 01 `"No"'
label define cslegcus_lbl 02 `"Yes"', add
label define cslegcus_lbl 98 `"Blank"', add
label define cslegcus_lbl 99 `"NIU"', add
label values cslegcus cslegcus_lbl

label define cswork_lbl 01 `"Did not work"'
label define cswork_lbl 02 `"Worked part time"', add
label define cswork_lbl 03 `"Worked full time"', add
label define cswork_lbl 98 `"Blank"', add
label define cswork_lbl 99 `"NIU"', add
label values cswork cswork_lbl

label define cswork5_lbl 01 `"No"'
label define cswork5_lbl 02 `"Yes"', add
label define cswork5_lbl 98 `"Blank"', add
label define cswork5_lbl 99 `"NIU"', add
label values cswork5 cswork5_lbl

label define cselig_lbl 00 `"Not eligible"'
label define cselig_lbl 01 `"Eligible"', add
label values cselig cselig_lbl

label define famkind_lbl 1 `"Husband/wife family"'
label define famkind_lbl 2 `"Male reference person"', add
label define famkind_lbl 3 `"Female reference person"', add
label values famkind famkind_lbl

label define eligorg_lbl 0 `"Not eligible"'
label define eligorg_lbl 1 `"Eligible"', add
label values eligorg eligorg_lbl


