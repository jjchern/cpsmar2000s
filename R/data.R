#' CPS March Supplement (ASEC) from IPUMS, 2000
#'
#' \code{cpsmar2000} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2000
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2000"

#' CPS March Supplement (ASEC) from IPUMS, 2001
#'
#' \code{cpsmar2001} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2001
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2001"

#' CPS March Supplement (ASEC) from IPUMS, 2002
#'
#' \code{cpsmar2002} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2002
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2002"

#' CPS March Supplement (ASEC) from IPUMS, 2003
#'
#' \code{cpsmar2003} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2003
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2003"

#' CPS March Supplement (ASEC) from IPUMS, 2004
#'
#' \code{cpsmar2004} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2004
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2004"

#' CPS March Supplement (ASEC) from IPUMS, 2005
#'
#' \code{cpsmar2005} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2005
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2005"

#' CPS March Supplement (ASEC) from IPUMS, 2006
#'
#' \code{cpsmar2006} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2006
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2006"

#' CPS March Supplement (ASEC) from IPUMS, 2007
#'
#' \code{cpsmar2007} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2007
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2007"

#' CPS March Supplement (ASEC) from IPUMS, 2008
#'
#' \code{cpsmar2008} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2008
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2008"

#' CPS March Supplement (ASEC) from IPUMS, 2009
#'
#' \code{cpsmar2009} contains all the variables (as of Aug. 30, 2015)
#' from IPUMS-CPS ASEC, 2009
#'
#' @format A data frame
#'
#' @source \url{https://cps.ipums.org}
"cpsmar2009"
