About
=====

This R data package includes data from Annual Social and Economic Supplement of the Current Population Survey (ASEC, i.e., CPS March Supplement), 2000-2009.

Installation
============

Install from bitbucket with

``` r
devtools::install_bitbucket("jjchern/cpsmar2000s")
```
